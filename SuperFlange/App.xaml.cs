﻿using SuperFlange.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SuperFlange
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static new App Current => (App)Application.Current;
        public Theme CurrentTheme { get; private set; }

        public event EventHandler ThemeChanged;

        public void SetTheme(Theme theme)
        {
            CurrentTheme = theme;
            Resources.MergedDictionaries[0].Source = new Uri($"/{theme}.xaml", UriKind.Relative);
            ThemeChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    public enum Theme
    {
        Dark,
        Light
    }
}
