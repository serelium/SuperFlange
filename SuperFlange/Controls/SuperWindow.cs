﻿using SuperFlange.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SuperFlange.Controls
{
    public class SuperWindow : Window
    {
        public static DependencyProperty MinimizeButtonImageProperty = DependencyProperty.Register(nameof(MinimizeButtonImage), typeof(ImageSource), typeof(SuperWindow));

        public ImageSource MinimizeButtonImage
        {
            get { return (ImageSource)GetValue(MinimizeButtonImageProperty); }
            set { SetValue(MinimizeButtonImageProperty, value); }
        }

        public static DependencyProperty MaximizeButtonImageProperty = DependencyProperty.Register(nameof(MaximizeButtonImage), typeof(ImageSource), typeof(SuperWindow));

        public ImageSource MaximizeButtonImage
        {
            get { return (ImageSource)GetValue(MaximizeButtonImageProperty); }
            set { SetValue(MaximizeButtonImageProperty, value); }
        }

        public static DependencyProperty CloseButtonImageProperty = DependencyProperty.Register(nameof(CloseButtonImage), typeof(ImageSource), typeof(SuperWindow));

        public ImageSource CloseButtonImage
        {
            get { return (ImageSource)GetValue(CloseButtonImageProperty); }
            set { SetValue(CloseButtonImageProperty, value); }
        }

        public static DependencyProperty ShowThemeMenuProperty = DependencyProperty.Register(nameof(ShowThemeMenuProperty), typeof(bool), typeof(SuperWindow), new PropertyMetadata(true));

        public bool ShowThemeMenu
        {
            get { return (bool)GetValue(ShowThemeMenuProperty); }
            set { SetValue(ShowThemeMenuProperty, value); }
        }

        public static DependencyProperty MinimizeWindowCommandProperty = DependencyProperty.Register(nameof(MinimizeWindowCommand), typeof(ICommand), typeof(SuperWindow));

        public ICommand MinimizeWindowCommand
        {
            get { return (ICommand)GetValue(MinimizeWindowCommandProperty); }
            set { SetValue(MinimizeWindowCommandProperty, value); }
        }

        public static DependencyProperty MaximizeWindowCommandProperty = DependencyProperty.Register(nameof(MaximizeWindowCommand), typeof(ICommand), typeof(SuperWindow));

        public ICommand MaximizeWindowCommand
        {
            get { return (ICommand)GetValue(MaximizeWindowCommandProperty); }
            set { SetValue(MaximizeWindowCommandProperty, value); }
        }

        public static DependencyProperty CloseWindowCommandProperty = DependencyProperty.Register(nameof(CloseWindowCommand), typeof(ICommand), typeof(SuperWindow));

        public ICommand CloseWindowCommand
        {
            get { return (ICommand)GetValue(CloseWindowCommandProperty); }
            set { SetValue(CloseWindowCommandProperty, value); }
        }

        public static DependencyProperty SetDarkThemeCommandProperty = DependencyProperty.Register(nameof(SetDarkThemeCommand), typeof(ICommand), typeof(SuperWindow));

        public ICommand SetDarkThemeCommand
        {
            get { return (ICommand)GetValue(SetDarkThemeCommandProperty); }
            set { SetValue(SetDarkThemeCommandProperty, value); }
        }

        public static DependencyProperty SetLightThemeCommandProperty = DependencyProperty.Register(nameof(SetLightThemeCommand), typeof(ICommand), typeof(SuperWindow));

        public ICommand SetLightThemeCommand
        {
            get { return (ICommand)GetValue(SetLightThemeCommandProperty); }
            set { SetValue(SetLightThemeCommandProperty, value); }
        }

        public SuperWindow()
            : base()
        {
            SetupForTheme();

            MinimizeWindowCommand = new Command(Minimize);
            MaximizeWindowCommand = new Command(Maximize);
            CloseWindowCommand = new Command(Close);
            SetDarkThemeCommand = new Command(() => SetTheme(Theme.Dark));
            SetLightThemeCommand = new Command(() => SetTheme(Theme.Light));

            Style = (Style)FindResource("SuperWindowStyle");
            WindowStyle = WindowStyle.SingleBorderWindow;

            App.Current.ThemeChanged += OnThemeChanged;
        }

        private void OnThemeChanged(object sender, EventArgs e)
        {
            SetupForTheme();
            Style = (Style)FindResource("SuperWindowStyle");
        }

        public void Maximize()
        {
            if (WindowState == WindowState.Maximized)
                WindowState = WindowState.Normal;
            else if (WindowState == WindowState.Normal)
                WindowState = WindowState.Maximized;
        }

        public void Minimize()
        {
            WindowState = WindowState.Minimized;
        }

        public void SetTheme(Theme theme)
        {
            if (App.Current.CurrentTheme != theme)
                App.Current.SetTheme(theme);
        }

        private void SetupForTheme()
        {
            if(App.Current.CurrentTheme == Theme.Dark)
            {
                MinimizeButtonImage = new BitmapImage(new Uri("pack://application:,,,/SuperFlange;component/Resources/MinimizeWindowWhite.png"));
                MaximizeButtonImage = new BitmapImage(new Uri("pack://application:,,,/SuperFlange;component/Resources/MaximizeWindowWhite.png"));
                CloseButtonImage = new BitmapImage(new Uri("pack://application:,,,/SuperFlange;component/Resources/CloseWindowWhite.png"));
            }
            else
            {
                MinimizeButtonImage = new BitmapImage(new Uri("pack://application:,,,/SuperFlange;component/Resources/MinimizeWindowBlack.png"));
                MaximizeButtonImage = new BitmapImage(new Uri("pack://application:,,,/SuperFlange;component/Resources/MaximizeWindowBlack.png"));
                CloseButtonImage = new BitmapImage(new Uri("pack://application:,,,/SuperFlange;component/Resources/CloseWindowBlack.png"));
            }
        }
    }
}
