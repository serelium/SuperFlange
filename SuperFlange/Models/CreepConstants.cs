﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperFlange.Models
{
    public class CreepConstants : ElementBase
    {
        private CreepStressFunctionType _CreepStressFunctionType;
        [Measure(5, null, null, true)]
        public CreepStressFunctionType CreepStressFunctionType
        {
            get => _CreepStressFunctionType;
            set => SetPropertyBackingField(ref _CreepStressFunctionType, value, nameof(CreepStressFunctionType));
        }

        private float _CreepStressConstantA;
        [Measure(6, null, null, true)]
        public float CreepStressConstantA
        {
            get => _CreepStressConstantA;
            set => SetPropertyBackingField(ref _CreepStressConstantA, value, nameof(CreepStressConstantA));
        }

        private float _CreepStressConstantB;
        [Measure(7, null, null, true)]
        public float CreepStressConstantB
        {
            get => _CreepStressConstantB;
            set => SetPropertyBackingField(ref _CreepStressConstantB, value, nameof(CreepStressConstantB));
        }

        private float _CreepStressConstantC;
        [Measure(8, null, null, true)]
        public float CreepStressConstantC
        {
            get => _CreepStressConstantC;
            set => SetPropertyBackingField(ref _CreepStressConstantC, value, nameof(CreepStressConstantC));
        }

        private float _CreepStressConstantD;
        [Measure(9, null, null, true)]
        public float CreepStressConstantD
        {
            get => _CreepStressConstantD;
            set => SetPropertyBackingField(ref _CreepStressConstantD, value, nameof(CreepStressConstantD));
        }

        private CreepTimeFunctionType _CreepTimeFunctionType;
        [Measure(10, null, null, true)]
        public CreepTimeFunctionType CreepTimeFunctionType
        {
            get => _CreepTimeFunctionType;
            set => SetPropertyBackingField(ref _CreepTimeFunctionType, value, nameof(CreepTimeFunctionType));
        }

        private float _CreepTimeConstantA;
        [Measure(11, null, null, true)]
        public float CreepTimeConstantA
        {
            get => _CreepTimeConstantA;
            set => SetPropertyBackingField(ref _CreepTimeConstantA, value, nameof(CreepTimeConstantA));
        }

        private float _CreepTimeConstantB;
        [Measure(12, null, null, true)]
        public float CreepTimeConstantB
        {
            get => _CreepTimeConstantB;
            set => SetPropertyBackingField(ref _CreepTimeConstantB, value, nameof(CreepTimeConstantB));
        }

        private float _CreepTimeConstantC;
        [Measure(13, null, null, true)]
        public float CreepTimeConstantC
        {
            get => _CreepTimeConstantC;
            set => SetPropertyBackingField(ref _CreepTimeConstantC, value, nameof(CreepTimeConstantC));
        }

        private float _CreepTimeConstantD;
        [Measure(14, null, null, true)]
        public float CreepTimeConstantD
        {
            get => _CreepTimeConstantD;
            set => SetPropertyBackingField(ref _CreepTimeConstantD, value, nameof(CreepTimeConstantD));
        }
    }
}
