﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperFlange.Models
{
    public class Material : ElementBase
    {
        private float _YoungModulusAtAmbient;
        [Measure(1, name: "Young's Modulus At Ambient Temperature", units: "mm", isRequired: true)]
        public float YoungModulusAtAmbient
        {
            get => _YoungModulusAtAmbient;
            set => SetPropertyBackingField(ref _YoungModulusAtAmbient, value, nameof(YoungModulusAtAmbient));
        }

        private float _YoungModulusAtOperatingTemp;
        [Measure(2, name: "Young's Modulus At Operating Temperature", units: "mm", isRequired: true)]
        public float YoungModulusAtOperatingTemp
        {
            get => _YoungModulusAtOperatingTemp;
            set => SetPropertyBackingField(ref _YoungModulusAtOperatingTemp, value, nameof(YoungModulusAtOperatingTemp));
        }

        private float _PoissonRatio;
        [Measure(3, units: "mm", isRequired: true)]
        public float PoissonRatio
        {
            get => _PoissonRatio;
            set => SetPropertyBackingField(ref _PoissonRatio, value, nameof(PoissonRatio));
        }

        private float _AllowableStressAtAmbient;
        [Measure(4, name: "Allowable Stress At Ambient Temperature", units: "mm", isRequired: true)]
        public float AllowableStressAtAmbient
        {
            get => _AllowableStressAtAmbient;
            set => SetPropertyBackingField(ref _AllowableStressAtAmbient, value, nameof(AllowableStressAtAmbient));
        }

        private float _AllowableStressAtOperatingTemp;
        [Measure(5, name: "Allowable Stress At Operating Temperature", units: "mm", isRequired: true)]
        public float AllowableStressAtOperatingTemp
        {
            get => _AllowableStressAtOperatingTemp;
            set => SetPropertyBackingField(ref _AllowableStressAtOperatingTemp, value, nameof(AllowableStressAtOperatingTemp));
        }

        private float _ThermalExpansionCoefficient;
        [Measure(6, units: "mm", isRequired: true)]
        public float ThermalExpansionCoefficient
        {
            get => _ThermalExpansionCoefficient;
            set => SetPropertyBackingField(ref _ThermalExpansionCoefficient, value, nameof(ThermalExpansionCoefficient));
        }

        private float _ThermalConductivity;
        [Measure(7, units: "mm", isRequired: true)]
        public float ThermalConductivity
        {
            get => _ThermalConductivity;
            set => SetPropertyBackingField(ref _ThermalConductivity, value, nameof(ThermalConductivity));
        }

        private float _OutsideHeatFilmTransferCoefficient;
        [Measure(8, units: "mm", isRequired: true)]
        public float OutsideHeatFilmTransferCoefficient
        {
            get => _OutsideHeatFilmTransferCoefficient;
            set => SetPropertyBackingField(ref _OutsideHeatFilmTransferCoefficient, value, nameof(OutsideHeatFilmTransferCoefficient));
        }

        private float _InsideHeatFilmTransferCoefficient;
        [Measure(9, units: "mm", isRequired: true)]
        public float InsideHeatFilmTransferCoefficient
        {
            get => _InsideHeatFilmTransferCoefficient;
            set => SetPropertyBackingField(ref _InsideHeatFilmTransferCoefficient, value, nameof(InsideHeatFilmTransferCoefficient));
        }

        private float _CreepConstantA;
        [Measure(10, units: "mm", isRequired: true)]
        public float CreepConstantA
        {
            get => _CreepConstantA;
            set => SetPropertyBackingField(ref _CreepConstantA, value, nameof(CreepConstantA));
        }

        private float _CreepConstantM;
        [Measure(11, units: "mm", isRequired: true)]
        public float CreepConstantM
        {
            get => _CreepConstantM;
            set => SetPropertyBackingField(ref _CreepConstantM, value, nameof(CreepConstantM));
        }

        private float _CreepConstantN;
        [Measure(12, units: "mm", isRequired: true)]
        public float CreepConstantN
        {
            get => _CreepConstantN;
            set => SetPropertyBackingField(ref _CreepConstantN, value, nameof(CreepConstantN));
        }
    }
}
