﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperFlange.Models
{
    public class Shell : Element
    {
        private ShellHeadType _HeadType;
        [Measure(1, units: "mm", isRequired: true)]
        public ShellHeadType HeadType
        {
            get => _HeadType;
            set => SetPropertyBackingField(ref _HeadType, value, nameof(HeadType));
        }

        private float _InsideDiameter;
        [Measure(2, units: "mm", isRequired: true)]
        public float InsideDiameter
        {
            get => _InsideDiameter;
            set => SetPropertyBackingField(ref _InsideDiameter, value, nameof(InsideDiameter));
        }

        private float _Thickness;
        [Measure(3, units: "mm", isRequired: true)]
        public float Thickness
        {
            get => _Thickness;
            set => SetPropertyBackingField(ref _Thickness, value, nameof(Thickness));
        }

        private float _HubLength;
        [Measure(4, units: "mm", isRequired: true)]
        public float HubLength
        {
            get => _HubLength;
            set => SetPropertyBackingField(ref _HubLength, value, nameof(HubLength));
        }
    }

    public enum ShellHeadType
    {
        Type1 = 1
    }
}
