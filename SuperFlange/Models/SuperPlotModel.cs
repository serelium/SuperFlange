﻿using OxyPlot;
using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SuperFlange.Models
{
    public class SuperPlotModel : PlotModel
    {
        public SuperPlotModel()
            :base()
        {
            App.Current.ThemeChanged += OnThemeChanged;
            SetupForTheme();
        }

        public void OnThemeChanged(object sender, EventArgs e)
        {
            SetupForTheme();
        }

        private void SetupForTheme()
        {
            OxyColor color = OxyColors.Black;

            if (App.Current.CurrentTheme == Theme.Dark)
                color = OxyColors.White;

            TextColor = color;
            TitleColor = color;
            LegendTextColor = color;
            PlotAreaBorderColor = color;
            SubtitleColor = color;
            SelectionColor = color;
        }
    }

    public class SuperLinearAxis : LinearAxis
    {
        public SuperLinearAxis()
            : base()
        {
            App.Current.ThemeChanged += OnThemeChanged;
            SetupForTheme();
        }

        public void OnThemeChanged(object sender, EventArgs e)
        {
            SetupForTheme();
        }

        private void SetupForTheme()
        {
            OxyColor color = OxyColors.Black;

            if (App.Current.CurrentTheme == Theme.Dark)
                color = OxyColors.White;

            TextColor = color;
            TitleColor = color;
            AxislineColor = color;
            ExtraGridlineColor = color;
            MajorGridlineColor = color;
            MinorGridlineColor = color;
            TicklineColor = color;
        }
    }
}
