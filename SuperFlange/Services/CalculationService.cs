﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperFlange.Models;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.Interpolation;
using System.Windows;

namespace SuperFlange.Services
{
    public class CalculationService
    {
        public void SuperFlange(Flange flange1, Flange flange2, Shell shell1, Shell shell2, Bolt bolt, Gasket gasket, OperatingConditions operatingConditions)
        {

        }

        public List<Point> GeneratePoints(IInterpolation interpolation, double startX, double endX, int pointCount)
        {
            List<Point> points = new List<Point>();

            double stepX = (endX - startX) / pointCount;

            for (int i = 0; i <= pointCount; i++)
            {
                double x = startX + stepX * i;
                points.Add(new Point(x, interpolation.Interpolate(x)));
            }

            return points;
        }

        public double ComputeGasketInitialDisplacement_Old(Gasket gasket, OperatingConditions operatingConditions)
        {
            //double epsilon = 0;

            //double derivA = 2;
            //double covA = 3;
            //double angleRA = 30;

            //double derivB = derivA;
            //double covB = covA;
            //double angleRB = angleRA;

            //int ip = gasket.DisplacementMatrix.Length;
            //int jp = gasket.DisplacementMatrix[0].Length;

            //Matrix<double> mSref = Matrix<double>.Build.Dense(ip, 1, MathHelper.LinSpace(0, 1, ip).ToArray());
            //Matrix<double> mTref = Matrix<double>.Build.Dense(ip, 1, MathHelper.LinSpace(0, 1, ip).ToArray());
            //Matrix<double> mKAabs = Matrix<double>.Build.Dense(ip, ip);
            //Matrix<double> mKBabs = Matrix<double>.Build.Dense(jp, jp);

            //for (int i = 0; i < ip - 1; i++)
            //{
            //    for (int j = 1; j < ip; j++)
            //    {
            //        if (covA < 4)
            //        {
            //            mKAabs[i, j] = Math.Pow(Math.Abs(mSref[i, 0] - mSref[j, 0]), covA);
            //            mKAabs[j, i] = Math.Pow(Math.Abs(mSref[i, 0] - mSref[j, 0]), covA);
            //        }
            //        else if (covA == 4)
            //        {
            //            mKAabs[i, j] = Math.Pow(Math.Abs(mSref[i, 0] - mSref[j, 0]), 2) * Math.Log(Math.Abs(mSref[i, 0] - mSref[j, 0]));
            //            mKAabs[j, i] = Math.Pow(Math.Abs(mSref[i, 0] - mSref[j, 0]), 2) * Math.Log(Math.Abs(mSref[i, 0] - mSref[j, 0])); 
            //        }
            //        else if (covA == 5)
            //        {
            //            mKAabs[i, j] = Math.Sin(angleRA * Math.Abs(mSref[i, 0] - mSref[j, 0]) * Math.PI / 180);
            //            mKAabs[j, i] = Math.Sin(angleRA * Math.Abs(mSref[i, 0] - mSref[j, 0]) * Math.PI / 180);
            //        }
            //    }
            //}

            //if(jp >= 2)
            //{
            //    for (int i = 0; i < ip - 1; i++)
            //    {
            //        for (int j = 1; j < ip; j++)
            //        {
            //            if (covA < 4)
            //            {
            //                mKBabs[i, j] = Math.Pow(Math.Abs(mTref[i, 0] - mTref[j, 0]), covA);
            //                mKBabs[j, i] = Math.Pow(Math.Abs(mTref[i, 0] - mTref[j, 0]), covA);
            //            }
            //            else if (covA == 4)
            //            {
            //                mKBabs[i, j] = Math.Pow(Math.Abs(mTref[i, 0] - mTref[j, 0]), 2) * Math.Log(Math.Abs(mTref[i, 0] - mTref[j, 0]));
            //                mKBabs[j, i] = Math.Pow(Math.Abs(mTref[i, 0] - mTref[j, 0]), 2) * Math.Log(Math.Abs(mTref[i, 0] - mTref[j, 0]));
            //            }
            //            else if (covA == 5)
            //            {
            //                mKBabs[i, j] = Math.Sin(angleRA * Math.Abs(mTref[i, 0] - mTref[j, 0]) * Math.PI / 180);
            //                mKBabs[j, i] = Math.Sin(angleRA * Math.Abs(mTref[i, 0] - mTref[j, 0]) * Math.PI / 180);
            //            }
            //        }
            //    }
            //}

            //Matrix<double> mKAs = Matrix<double>.Build.Dense(ip, (int)derivA);
            //mKAs += 1;

            //if (derivA != 5)
            //{
            //    for(int i = 0; i < mKAs.ColumnCount; i++)
            //    {
            //        mKAs.SetColumn(i, mSref.PointwisePower(i).Inverse().Column(0));
            //    }
            //}

            Matrix<double> mDisplacement = Matrix<double>.Build.DenseOfColumnArrays(gasket.DisplacementMatrix);
            Matrix<double> mStress = Matrix<double>.Build.DenseOfColumnArrays(gasket.StressMatrix);

            Vector<double> vDisplacement = mDisplacement.Row(mDisplacement.RowCount - 1);
            Vector<double> vStress = mStress.Row(mStress.RowCount - 1);

            CubicSpline interpolation = CubicSpline
                .InterpolateNatural(vStress.ToArray(), vDisplacement.ToArray());

            double disp = interpolation.Interpolate(22791.5380048f);

            return disp;
        }
    }
}
