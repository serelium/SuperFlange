﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperFlange.Services
{
    public class GlobalServices
    {
        private static GlobalServices _Instance;
        public static GlobalServices Instance
        { 
            get
            {
                if (_Instance == null)
                    _Instance = new GlobalServices();

                return _Instance;
            }
        }

        public CalculationService CalculationService { get; }

        private GlobalServices()
        {
            CalculationService = new CalculationService();
        }
    }
}
