﻿using MathNet.Numerics.Interpolation;
using MathNet.Numerics.LinearAlgebra;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using SuperFlange.Models;
using SuperFlange.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SuperFlange.Solvers
{
    public class GasketSolver
    {
        private CalculationService _CalculationService;

        public GasketSolver()
        {
            _CalculationService = GlobalServices.Instance.CalculationService;
        }

        public double ComputeGasketInitialDisplacement(Gasket gasket, double initialGasketStress)
        {
            Matrix<double> mDisplacement = Matrix<double>.Build.DenseOfRowArrays(gasket.DisplacementMatrix);
            Matrix<double> mStress = Matrix<double>.Build.DenseOfRowArrays(gasket.StressMatrix);

            Vector<double> vDisplacement = mDisplacement.Column(mDisplacement.ColumnCount - 1);
            Vector<double> vStress = mStress.Column(mStress.ColumnCount - 1);

            CubicSpline interpolation = CubicSpline.InterpolateNatural(vStress, vDisplacement);

            return interpolation.Interpolate(initialGasketStress);
        }

        public List<Point> InterpolateGasketStressSpline(Gasket gasket, double initialDisplacement)
        {
            Matrix<double> mInitialDisplacement = Matrix<double>.Build.DenseOfRowArrays(gasket.DisplacementMatrix);
            Matrix<double> mInitialStress = Matrix<double>.Build.DenseOfRowArrays(gasket.StressMatrix);

            int index = 0;

            for (int i = 0; i < mInitialDisplacement.RowCount; i++)
            {
                var row = mInitialDisplacement.Row(i);
                if (row.Last() >= initialDisplacement)
                {
                    index = i;
                    break;
                }
            }

            Vector<double> displacementsA = mInitialDisplacement.Row(index - 1);
            Vector<double> stressesA = mInitialStress.Row(index - 1);

            Vector<double> displacementsC = mInitialDisplacement.Row(index);
            Vector<double> stressesC = mInitialStress.Row(index);

            LogLinear interpolationA = LogLinear.Interpolate(stressesA, displacementsA);
            LogLinear interpolationC = LogLinear.Interpolate(stressesC, displacementsC);
            LogLinear interpolation = LogLinear.Interpolate(mInitialDisplacement.EnumerateColumns().Last(), mInitialStress.EnumerateColumns().Last());

            double stress = interpolation.Interpolate(initialDisplacement);
            double ratio = (initialDisplacement - interpolationA.Interpolate(stress)) / (interpolationC.Interpolate(stress) - interpolationA.Interpolate(stress));

            List<Point> finalStressPoints = new List<Point>();

            for (int i = 0; i < displacementsA.Count; i++)
            {
                double distance = (interpolationC.Interpolate(stressesA[i]) - displacementsA[i]) * ratio;
                finalStressPoints.Add(new Point(displacementsA[i] + distance, stressesA[i]));
            }

            finalStressPoints.Add(new Point(initialDisplacement, stress));

            return finalStressPoints;
        }

        public double ComputeFinalGasketStress(Gasket gasket, double initialDisplacement, double finalDisplacement)
        {
            var points = InterpolateGasketStressSpline(gasket, initialDisplacement);
            LogLinear interpolation = LogLinear.Interpolate(points.Select(p => p.X), points.Select(p => p.Y));

            return interpolation.Interpolate(finalDisplacement);
        }

        public double ComputeAverageGasketSlope(IInterpolation interpolation, double startX, double endX, int numberOfSteps)
        {
            double stepX = (endX - startX) / numberOfSteps;
            double sum = 0;

            for (int i = 0; i < numberOfSteps; i++)
            {
                double x = startX + stepX * i;
                sum += interpolation.Differentiate(x);
            }

            return sum / numberOfSteps;
        }

        public SuperPlotModel BuildGasketStressDiplacementPlotModel(Gasket gasket)
        {
            Matrix<double> mInitialDisplacement = Matrix<double>.Build.DenseOfRowArrays(gasket.DisplacementMatrix);
            Matrix<double> mInitialStress = Matrix<double>.Build.DenseOfRowArrays(gasket.StressMatrix);

            var plotModel = new SuperPlotModel()
            {
                LegendPlacement = LegendPlacement.Outside,
            };

            SuperLinearAxis xAxis = new SuperLinearAxis()
            {
                Position = AxisPosition.Bottom,
                Title = "Diplacement",
            };

            SuperLinearAxis yAxis = new SuperLinearAxis
            {
                Position = AxisPosition.Left,
                Title = "Stress",
            };

            plotModel.Axes.Add(xAxis);
            plotModel.Axes.Add(yAxis);

            for (int i = mInitialDisplacement.ColumnCount - 1; i < mInitialDisplacement.ColumnCount; i++)
            {
                var xs = mInitialDisplacement.Column(i);
                var ys = mInitialStress.Column(i);

                IInterpolation interp = MathNet.Numerics.Interpolate.LogLinear(xs, ys);
                List<Point> generatedPoints = _CalculationService.GeneratePoints(interp, xs.First(), xs.Last(), 100);

                LineSeries lineSeries = new LineSeries()
                {
                    Title = "Loading",
                    Color = OxyColors.Green,
                    MarkerType = MarkerType.Circle,
                    MarkerSize = 2,
                    MarkerStroke = OxyColors.Transparent,
                    MarkerFill = OxyColors.Green,
                    MarkerStrokeThickness = 1.5,
                };

                lineSeries.Points.AddRange(generatedPoints.Select(p => new DataPoint(p.X, p.Y)));
                plotModel.Series.Add(lineSeries);
            }

            for (int i = 0; i < mInitialDisplacement.RowCount; i++)
            {
                var xs = mInitialDisplacement.Row(i);
                var ys = mInitialStress.Row(i);

                IInterpolation interp = MathNet.Numerics.Interpolate.LogLinear(xs, ys);
                List<Point> generatedPoints = _CalculationService.GeneratePoints(interp, xs.First(), xs.Last(), 100);

                LineSeries lineSeries = new LineSeries()
                {
                    Title = $"Unloading {i + 1}",
                    Color = OxyColors.SkyBlue,
                    MarkerType = MarkerType.Circle,
                    MarkerSize = 2,
                    MarkerStroke = OxyColors.Transparent,
                    MarkerFill = OxyColors.SkyBlue,
                    MarkerStrokeThickness = 1.5,
                };

                lineSeries.Points.AddRange(generatedPoints.Select(p => new DataPoint(p.X, p.Y)));
                plotModel.Series.Add(lineSeries);
            }

            LineSeries finalLoadingSeries = new LineSeries()
            {
                Title = "Final Loading",
                Color = OxyColors.Red,
                MarkerType = MarkerType.Circle,
                MarkerSize = 2,
                MarkerStroke = OxyColors.Transparent,
                MarkerFill = OxyColors.Red,
                MarkerStrokeThickness = 1.5,
            };

            List<Point> points = InterpolateGasketStressSpline(gasket, 0.019f);
            finalLoadingSeries.Points.AddRange(points.Select(p => new DataPoint(p.X, p.Y)));
            plotModel.Series.Add(finalLoadingSeries);

            LineSeries finalLoadingSeries1 = new LineSeries()
            {
                Title = "Final Loading 1",
                Color = OxyColors.Red,
                MarkerType = MarkerType.Circle,
                MarkerSize = 2,
                MarkerStroke = OxyColors.Transparent,
                MarkerFill = OxyColors.Red,
                MarkerStrokeThickness = 1.5,
            };

            List<Point> points1 = InterpolateGasketStressSpline(gasket, 0.01400000000000000000f);
            finalLoadingSeries1.Points.AddRange(points1.Select(p => new DataPoint(p.X, p.Y)));
            plotModel.Series.Add(finalLoadingSeries1);

            for (int i = 0; i < 10000; i++)
                ComputeFinalGasketStress(gasket, 0.019f, 0.014f);

            return plotModel;
        }
    }
}
