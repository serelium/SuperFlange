﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SuperFlange.ViewModels
{
    public class GraphViewModel : ViewModel
    {
        private string _Title;
        public string Title
        {
            get => _Title;
            set => SetPropertyBackingField(ref _Title, value, nameof(Title));
        }

        private PlotModel _PlotModel;
        public PlotModel PlotModel
        {
            get => _PlotModel;
            set => SetPropertyBackingField(ref _PlotModel, value, nameof(PlotModel));
        }

        private PlotController _ChartController;
        public PlotController ChartController
        {
            get => _ChartController;
            set => SetPropertyBackingField(ref _ChartController, value, nameof(ChartController));
        }

        public GraphViewModel()
        {
            ChartController = new PlotController();
            ChartController.Bind(new OxyMouseEnterGesture(), PlotCommands.HoverSnapTrack);
        }
    }
}
