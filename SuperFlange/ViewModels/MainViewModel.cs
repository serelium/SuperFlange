﻿using OxyPlot;
using OxyPlot.Axes;
using SuperFlange.Models;
using SuperFlange.Services;
using SuperFlange.Solvers;
using SuperFlange.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SuperFlange.ViewModels
{
    public class MainViewModel : ViewModel
    {
        public Flange FirstFlange { get; set; }
        public Flange SecondFlange { get; set; }
        public Bolt Bolt { get; set; }
        public Shell FirstShell { get; set; }
        public Shell SecondShell { get; set; }
        public Gasket Gasket { get; set; }
        public OperatingConditions OperatingConditions { get; set; }

        public ICommand MinimizeWindowCommand { get; set; }
        public ICommand MaximizeRestoreCommand { get; set; }
        public ICommand CloseWindowCommand { get; set; }
        public ICommand SetDarkThemeCommand { get; set; }
        public ICommand SetLightThemeCommand { get; set; }

        public ICommand SelectCommand { get; set; }
        public ICommand SolveCommand { get; set; }

        public MainViewModel()
        {
            FirstFlange = new Flange();
            SecondFlange = new Flange();
            Bolt = new Bolt();
            FirstShell = new Shell();
            SecondShell = new Shell();
            Gasket = new Gasket();
            OperatingConditions = new OperatingConditions();

            // Only When Testing
            InitForTest();

            MinimizeWindowCommand = new Command(Minimize);
            MaximizeRestoreCommand = new Command(MaximizeRestore);
            CloseWindowCommand = new Command(CloseWindow);
            SetDarkThemeCommand = new Command(() => SetTheme(Theme.Dark));
            SetLightThemeCommand = new Command(() => SetTheme(Theme.Light));
            SolveCommand = new Command(() => Solve());
        }

        public void CloseWindow()
        {
            Application.Current.MainWindow.Close();
        }

        public void MaximizeRestore()
        {
            Window mainWindow = Application.Current.MainWindow;

            if (mainWindow.WindowState == WindowState.Maximized)
                mainWindow.WindowState = WindowState.Normal;
            else if (mainWindow.WindowState == WindowState.Normal)
                mainWindow.WindowState = WindowState.Maximized;
        }

        public void Minimize()
        {
            Application.Current.MainWindow.WindowState = WindowState.Minimized;
        }

        public void SetTheme(Theme theme)
        {
            ((App)Application.Current).SetTheme(theme);
        }

        public void InitForTest()
        {
            FirstFlange.FlangeType = FlangeType.WeldNeck;
            FirstFlange.OutsideDiameter = 58.375f;
            FirstFlange.RaisedFaceHeight = -1; // Unused
            FirstFlange.Thickness = 5.625f;
            FirstFlange.RaisedFaceDiameter = -1; // Unused
            FirstFlange.NumberOfBolts = 76;
            FirstFlange.InsideDiameter = 51;
            FirstFlange.HubThickness = 0.823f;
            FirstFlange.Material.YoungModulusAtAmbient = 30000000;
            FirstFlange.Material.YoungModulusAtOperatingTemp = 25000000;
            FirstFlange.Material.PoissonRatio = 0.3f;
            FirstFlange.Material.AllowableStressAtAmbient = -1; // Unused
            FirstFlange.Material.AllowableStressAtOperatingTemp = -1; // Unused
            FirstFlange.Material.ThermalExpansionCoefficient = 0.00000694f;
            FirstFlange.Material.ThermalConductivity = 5.87f;
            FirstFlange.Material.OutsideHeatFilmTransferCoefficient = 0.0951f;
            FirstFlange.Material.InsideHeatFilmTransferCoefficient = 0.476f;
            FirstFlange.Material.CreepConstantA = 0.000000000000000000000000000093567f;
            FirstFlange.Material.CreepConstantM = 5.5f;
            FirstFlange.Material.CreepConstantN = 1f;

            FirstShell.Thickness = 0.625f;
            FirstShell.HubLength = 1.25f;
            FirstShell.HeadType = ShellHeadType.Type1;
            FirstShell.Material.PoissonRatio = 0.3f;
            FirstShell.Material.YoungModulusAtAmbient = 30000000;
            FirstShell.Material.YoungModulusAtOperatingTemp = 25000000;
            FirstShell.Material.ThermalExpansionCoefficient = 0.00000694f;
            FirstShell.Material.ThermalConductivity = 5.87f;
            FirstShell.Material.CreepConstantA = 0.000000000000000000000000000093567f;
            FirstShell.Material.CreepConstantM = 5.5f;
            FirstShell.Material.CreepConstantN = 1;

            SecondFlange.FlangeType = FlangeType.WeldNeck;
            SecondFlange.OutsideDiameter = 58.375f;
            SecondFlange.RaisedFaceHeight = -1; // Unused
            SecondFlange.Thickness = 5.625f;
            SecondFlange.RaisedFaceDiameter = -1; // Unused
            SecondFlange.NumberOfBolts = 76;
            SecondFlange.InsideDiameter = 51;
            SecondFlange.HubThickness = 0.823f;
            SecondFlange.Material.YoungModulusAtAmbient = 30000000;
            SecondFlange.Material.YoungModulusAtOperatingTemp = 25000000;
            SecondFlange.Material.PoissonRatio = 0.3f;
            SecondFlange.Material.AllowableStressAtAmbient = -1; // Unused
            SecondFlange.Material.AllowableStressAtOperatingTemp = -1; // Unused
            SecondFlange.Material.ThermalExpansionCoefficient = 0.00000694f;
            SecondFlange.Material.ThermalConductivity = 5.87f;
            SecondFlange.Material.OutsideHeatFilmTransferCoefficient = 0.0951f;
            SecondFlange.Material.InsideHeatFilmTransferCoefficient = 0.476f;
            SecondFlange.Material.CreepConstantA = 0.000000000000000000000000000093567f;
            SecondFlange.Material.CreepConstantM = 5.5f;
            SecondFlange.Material.CreepConstantN = 1f;

            SecondShell.Thickness = 0.625f;
            SecondShell.HubLength = 1.25f;
            SecondShell.HeadType = ShellHeadType.Type1;
            SecondShell.Material.PoissonRatio = 0.3f;
            SecondShell.Material.YoungModulusAtAmbient = 30000000;
            SecondShell.Material.YoungModulusAtOperatingTemp = 25000000;
            SecondShell.Material.ThermalExpansionCoefficient = 0.00000694f;
            SecondShell.Material.ThermalConductivity = 5.87f;
            SecondShell.Material.CreepConstantA = 0.000000000000000000000000000093567f;
            SecondShell.Material.CreepConstantM = 5.5f;
            SecondShell.Material.CreepConstantN = 1;

            Bolt.NumberOfThreadsPerUnit = 8;
            Bolt.WachersRigidityPerBolt = 0;
            Bolt.NutFactor = -1; // Unused
            Bolt.Material.YoungModulusAtAmbient = 30000000;
            Bolt.Material.YoungModulusAtOperatingTemp = 25000000;
            Bolt.Material.PoissonRatio = -1; // Unused
            Bolt.Material.AllowableStressAtAmbient = -1; // Unused
            Bolt.Material.AllowableStressAtOperatingTemp = -1; // Unused
            Bolt.Material.ThermalExpansionCoefficient = 0.000007777f; 
            Bolt.Material.OutsideHeatFilmTransferCoefficient = 31.96f;
            Bolt.Material.InsideHeatFilmTransferCoefficient = 9.51f;
            Bolt.Material.CreepConstantA = -1; // Unused
            Bolt.Material.CreepConstantM = -1; // Unused
            Bolt.Material.CreepConstantN = -1; // Unused
            Bolt.CreepConstants.CreepStressFunctionType = CreepStressFunctionType.Type6;
            Bolt.CreepConstants.CreepStressConstantA = 0.00000000000000000000000000000000000002f;
            Bolt.CreepConstants.CreepStressConstantB = 6.9f;
            Bolt.CreepConstants.CreepStressConstantC = 0;
            Bolt.CreepConstants.CreepStressConstantD = 0;
            Bolt.CreepConstants.CreepTimeFunctionType = CreepTimeFunctionType.Type6;
            Bolt.CreepConstants.CreepTimeConstantA = 1;
            Bolt.CreepConstants.CreepTimeConstantB = 1;
            Bolt.CreepConstants.CreepTimeConstantC = 0;
            Bolt.CreepConstants.CreepTimeConstantD = 0;
            Bolt.NominalDiameter = 1;

            Gasket.InsideDiameter = 52.125f;
            Gasket.OutsideDiameter = 53.125f;
            Gasket.RingOutsideDiameter = -1; // Unused
            Gasket.CenteringRingDiameter = -1; // Unused
            Gasket.Thickness = 0.063f;
            Gasket.EffectiveSeatingWidth = 0.5f;
            Gasket.Material.ThermalExpansionCoefficient = 0.00000477f;
            //-1, ... # intercept_part_A
            //-1, ... # slope_gb_line
            //-1, ... # intercept_part_B
            //-1, ... # gasket_factor
            //-1, ... # seating_stress
            //-1, ... # max_tightness
            //-1, ... # min_operating_stress
            //-1, ... # stress_creep_function
            //-1, ... # time_creep_function
            Gasket.DisplacementMatrix = new double[][]
            {
                new double[] { 0,        0,          0,        0,        0,        0,        0       },
                new double[] { 0.0019f,  0.0026f,    0.0033f,  0.0040f,  0.0045f,  0.0050f,  0.0054f },
                new double[] { 0.0055f,  0.0075f,    0.0082f,  0.0088f,  0.0093f,  0.0094f,  0.0095f },
                new double[] { 0.007f,   0.0084634f, 0.00953f, 0.01039f, 0.01108f, 0.01144f, 0.0119f },
                new double[] { 0.0090f,  0.0114f,    0.0121f,  0.0125f,  0.0128f,  0.0134f,  0.0137f },
                new double[] { 0.0115f,  0.0127f,    0.0134f,  0.0141f,  0.01481f, 0.01526f, 0.0156f },
                new double[] { 0.0131f,  0.0147f,    0.0152f,  0.0157f,  0.0164f,  0.0171f,  0.0180f },
                new double[] { 0.01462f, 0.0165f,    0.01687f, 0.01724f, 0.01788f, 0.01866f, 0.0199f }
            };
            Gasket.StressMatrix = new double[][]
            {
                new double[] { 0,    0,    0,    0,     0,     0,     0 },
                new double[] { 0,  180d,  290d,  500,   700,   900,  1047 },
                new double[] { 0,  200,  300,  500,  1000,  1500,  2500 },
                new double[] { 0,  200,  500, 1000,  2000,  3000,  4550 },
                new double[] { 0,  500, 1000, 2000,  3000,  5000,  8061 },
                new double[] { 0,  950, 1502, 3200,  6000, 10000, 13360 },
                new double[] { 0, 1074, 2000, 4000,  8000, 15000, 24732 },
                new double[] { 0, 1300, 3000, 5360, 10000, 20600, 40000 }
            };
            Gasket.CreepConstants.CreepStressFunctionType = CreepStressFunctionType.Type2;
            Gasket.CreepConstants.CreepStressConstantA = 0;
            Gasket.CreepConstants.CreepStressConstantB = 0;
            Gasket.CreepConstants.CreepStressConstantC = 1;
            Gasket.CreepConstants.CreepStressConstantD = 0;
            Gasket.CreepConstants.CreepTimeFunctionType = CreepTimeFunctionType.Type2;
            Gasket.CreepConstants.CreepTimeConstantA = 0;
            Gasket.CreepConstants.CreepTimeConstantB = 0;
            Gasket.CreepConstants.CreepTimeConstantC = 0.001f;
            Gasket.CreepConstants.CreepTimeConstantD = 0;

            OperatingConditions.DesignPressure = 270;
            OperatingConditions.DesignTemperature = 797;
            OperatingConditions.TestPressureRatio = -1; // Unused
            OperatingConditions.InitialBoltLoad = -1; // Unused
            OperatingConditions.ExternalForce = 0;
            OperatingConditions.ExternalMoment = 0;
            OperatingConditions.AmbiantTemperature = 77;
            OperatingConditions.CreepTime = 20000;
            OperatingConditions.InitialBoltStress = 45000;
            //-1, ... # initial_gasket_stress
            //-1, ... # applied_bolt_torque
            //-1, ... # tightness_class
            //-1, ... # assembly_efficiency
            //-1, ... # ratio_tpa_tpmin
        }

        public void Solve()
        {
            GasketSolver gasketSolver = new GasketSolver();

            GraphWindow graphWindow = new GraphWindow();
            GraphViewModel graphViewModel = (GraphViewModel)graphWindow.DataContext;

            graphViewModel.PlotModel = gasketSolver.BuildGasketStressDiplacementPlotModel(Gasket);

            graphWindow.Show();
        }
        
        public void SolveMatlab()
        {
            //using (SuperFlangeSolverNative.SuperFlangeSolver solver = new SuperFlangeSolverNative.SuperFlangeSolver())
            //{
            //    solver.Solve(
            //        0,

            //        // First Flange

            //        (int)FirstFlange.FlangeType,
            //        FirstFlange.OutsideDiameter,
            //        FirstFlange.RaisedFaceHeight,
            //        FirstFlange.Thickness,
            //        FirstFlange.RaisedFaceDiameter,
            //        FirstFlange.BoltCircleDiameter,
            //        (float)FirstFlange.NumberOfBolts,
            //        FirstFlange.InsideDiameter,
            //        FirstFlange.HubThickness,
            //        FirstShell.Thickness,
            //        FirstShell.HubLength,
            //        FirstFlange.Material.YoungModulusAtAmbient,
            //        FirstFlange.Material.YoungModulusAtOperatingTemp,
            //        FirstFlange.Material.PoissonRatio,
            //        FirstFlange.Material.AllowableStressAtAmbient,
            //        FirstFlange.Material.AllowableStressAtOperatingTemp,
            //        FirstFlange.Material.ThermalExpansionCoefficient,
            //        FirstFlange.Material.ThermalConductivity,
            //        FirstFlange.Material.OutsideHeatFilmTransferCoefficient,
            //        FirstFlange.Material.InsideHeatFilmTransferCoefficient,
            //        FirstFlange.Material.CreepConstantA,
            //        FirstFlange.Material.CreepConstantM,
            //        FirstFlange.Material.CreepConstantN,
            //        (int)FirstShell.HeadType,
            //        FirstShell.Material.PoissonRatio,
            //        FirstShell.Material.YoungModulusAtAmbient,
            //        FirstShell.Material.YoungModulusAtOperatingTemp,
            //        FirstShell.Material.CreepConstantA,
            //        FirstShell.Material.CreepConstantM,
            //        FirstShell.Material.CreepConstantN,
            //        FirstShell.Material.ThermalExpansionCoefficient,
            //        FirstShell.Material.ThermalConductivity,

            //        // Second Flange

            //        (int)SecondFlange.FlangeType,
            //        SecondFlange.OutsideDiameter,
            //        SecondFlange.RaisedFaceHeight,
            //        SecondFlange.Thickness,
            //        SecondFlange.RaisedFaceDiameter,
            //        SecondFlange.BoltCircleDiameter,
            //        (float)SecondFlange.NumberOfBolts,
            //        SecondFlange.InsideDiameter,
            //        SecondFlange.HubThickness,
            //        SecondShell.Thickness,
            //        SecondShell.HubLength,
            //        SecondFlange.Material.YoungModulusAtAmbient,
            //        SecondFlange.Material.YoungModulusAtOperatingTemp,
            //        SecondFlange.Material.PoissonRatio,
            //        SecondFlange.Material.AllowableStressAtAmbient,
            //        SecondFlange.Material.AllowableStressAtOperatingTemp,
            //        SecondFlange.Material.ThermalExpansionCoefficient,
            //        SecondFlange.Material.ThermalConductivity,
            //        SecondFlange.Material.OutsideHeatFilmTransferCoefficient,
            //        SecondFlange.Material.InsideHeatFilmTransferCoefficient,
            //        SecondFlange.Material.CreepConstantA,
            //        SecondFlange.Material.CreepConstantM,
            //        SecondFlange.Material.CreepConstantN,
            //        (int)SecondShell.HeadType,
            //        SecondShell.Material.PoissonRatio,
            //        SecondShell.Material.YoungModulusAtAmbient,
            //        SecondShell.Material.YoungModulusAtOperatingTemp,
            //        SecondShell.Material.CreepConstantA,
            //        SecondShell.Material.CreepConstantM,
            //        SecondShell.Material.CreepConstantN,
            //        SecondShell.Material.ThermalExpansionCoefficient,
            //        SecondShell.Material.ThermalConductivity,

            //        // Bolt

            //        (float)Bolt.NumberOfThreadsPerUnit,
            //        Bolt.WachersRigidityPerBolt,
            //        Bolt.NutFactor,
            //        Bolt.Material.YoungModulusAtAmbient,
            //        Bolt.Material.YoungModulusAtOperatingTemp,
            //        Bolt.Material.PoissonRatio,
            //        Bolt.Material.AllowableStressAtAmbient,
            //        Bolt.Material.AllowableStressAtOperatingTemp,
            //        Bolt.Material.ThermalExpansionCoefficient,
            //        Bolt.Material.OutsideHeatFilmTransferCoefficient,
            //        Bolt.Material.InsideHeatFilmTransferCoefficient,
            //        Bolt.Material.CreepConstantA,
            //        Bolt.Material.CreepConstantM,
            //        Bolt.Material.CreepConstantN,
            //        (int)Bolt.CreepConstants.CreepStressFunctionType,
            //        Bolt.CreepConstants.CreepStressConstantA,
            //        Bolt.CreepConstants.CreepStressConstantB,
            //        Bolt.CreepConstants.CreepStressConstantC,
            //        Bolt.CreepConstants.CreepStressConstantD,
            //        (int)Bolt.CreepConstants.CreepTimeFunctionType,
            //        Bolt.CreepConstants.CreepTimeConstantA,
            //        Bolt.CreepConstants.CreepTimeConstantB,
            //        Bolt.CreepConstants.CreepTimeConstantC,
            //        Bolt.CreepConstants.CreepTimeConstantD,

            //        // Gasket

            //        Gasket.InsideDiameter,
            //        Gasket.OutsideDiameter,
            //        Gasket.RingOutsideDiameter,
            //        Gasket.CenteringRingDiameter,
            //        Gasket.Thickness,
            //        Gasket.EffectiveSeatingWidth,
            //        Gasket.Material.ThermalExpansionCoefficient,
            //        -1, // intercept_part_A
            //        -1, // slope_gb_line
            //        -1, // intercept_part_B
            //        -1, // gasket_factor
            //        -1, // seating_stress
            //        -1, // max_tightness
            //        -1, // min_operating_stress
            //        -1, // stress_creep_function
            //        -1, // time_creep_function
            //        Gasket.DisplacementMatrix,
            //        Gasket.StressMatrix,
            //        Bolt.NominalDiameter,
            //        (int)Gasket.CreepConstants.CreepStressFunctionType,
            //        Gasket.CreepConstants.CreepStressConstantA,
            //        Gasket.CreepConstants.CreepStressConstantB,
            //        Gasket.CreepConstants.CreepStressConstantC,
            //        Gasket.CreepConstants.CreepStressConstantD,
            //        (int)Gasket.CreepConstants.CreepTimeFunctionType,
            //        Gasket.CreepConstants.CreepTimeConstantA,
            //        Gasket.CreepConstants.CreepTimeConstantB,
            //        Gasket.CreepConstants.CreepTimeConstantC,
            //        Gasket.CreepConstants.CreepTimeConstantD,

            //        // Operating Conditions

            //        OperatingConditions.DesignPressure,
            //        OperatingConditions.DesignTemperature, 
            //        OperatingConditions.TestPressureRatio,
            //        OperatingConditions.InitialBoltLoad, 
            //        OperatingConditions.ExternalForce,
            //        OperatingConditions.ExternalMoment,
            //        OperatingConditions.AmbiantTemperature,
            //        OperatingConditions.CreepTime,
            //        OperatingConditions.InitialBoltStress,
            //        -1, // initial_gasket_stress
            //        -1, // applied_bolt_torque
            //        -1, // tightness_class
            //        -1, // assembly_efficiency
            //        -1, // ratio_tpa_tpmin

            //        // Calculations To Perfom
            //        1,
            //        1,
            //        0,
            //        0,
            //        0,
            //        0);
            //}
        }
    }
}
