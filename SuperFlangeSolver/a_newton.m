
function [racine]=a_newton(xi,te,ho,tol1,m)

yi=tan(xi*te)-2*xi*ho/(xi^2-ho^2);
gi=(1+tan(xi*te)^2)*te-2*ho/(xi^2-ho^2)+4*xi^2*ho/(xi^2-ho^2)^2;
xip1=xi-yi/gi;
yip1=tan(xip1*te)-2*xip1*ho/(xip1^2-ho^2);
%fprintf('\n\ni       phini            phini+1          yi            yip1\n')
%fprintf('%2i    %9.7f     %9.7f     %9.5f     %9.5f\n',i,xi,xip1,yi,yip1)
  
  while abs(xip1-xi) >1e-6*abs(xip1)
%   i=i+1;
   xi=xip1;
        

yi=tan(xi*te)-2*xi*ho/(xi^2-ho^2);
gi=(1+tan(xi*te)^2)*te-2*ho/(xi^2-ho^2)+4*xi^2*ho/(xi^2-ho^2)^2;
xip1=xi-yi/gi;
yip1=tan(xip1*te)-2*xip1*ho/(xip1^2-ho^2);
%   fprintf('%2i    %9.7f     %9.7f     %9.5f    %9.5f       %9.5f\n',i,xi,xip1,yi,yip1)

end
racine=xi;