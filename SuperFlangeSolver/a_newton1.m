
function [racine]=a_newton1(xi,te,ho,tol1,m)

  yi=(2*tan(te*xi)^-1)-xi/ho+ho/xi;
  gi=-2/tan(te*xi)^2*(1+tan(te*xi)^2)*te-1/ho-ho/xi^2;
 xip1=xi-yi/gi;
yip1=(2*tan(te*xip1)^-1)-xip1/ho+ho/xip1;
  
  while abs(xip1-xi) > 1e-6*abs(xip1)
     %i=i+1;
   xi=xip1;
        

  yi=(2*tan(te*xi)^-1)-xi/ho+ho/xi;
  gi=-2/tan(te*xi)^2*(1+tan(te*xi)^2)*te-1/ho-ho/xi^2;
xip1=xi-yi/gi;
yip1=(2*tan(te*xip1)^-1)-xip1/ho+ho/xip1;

end
racine=xi;