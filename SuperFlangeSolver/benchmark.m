%Variables qui doivent �tre transf�r�es  DATA
%************************************************************************************************************
%************************************************************************************************************

%Variable g�n�rales
FlangeType1=1;                   %Type de flange: 1=WN, 2=SlipOn, 3=Lapped, 4=Threaded, 5= Socket, 6=Blind, 0=none
FlangeType2=1; 
ShellHeadType1=1;                %Type de shell-head: 1=Cylinder, 2=plate, 3=Sphere, 4=Conical, 5=eliptical, 0=none
ShellHeadType2=1;


%*********************************************************************************************
%  flange 52 in HE
%*********************************************************************************************

%Variable pour calculer l'interface cylindre/hub 
vc(1)=0.3;                                %Poison of cylindre
Ec(1)=30000000;                           %Young of cylindre at ambiante
Ect(1)=25000000;                          %Young of cylindre at temperature
B(1)=51;                                  %ID of flange
A(1)=58.375;                              %OD of flange
g1(1)=0.823;                              %thickness of hub big end
g0(1)=0.625;                              %thickness of cylinder
h(1)=1.25;                                %hub lenght

%Variable pour calculer l'interface hub/flange
vf(1)=0.3;                                %Poison of flange
Ef(1)=30000000;                           %Young of flange
Eft(1)=25000000;                          %Young of flange at
tf(1)=5.625;                              %thickness of flange

%  flange 2

%Variable pour calculer l'interface cylindre/hub 
vc(2)=0.3;                                %poison of cylindre
Ec(2)=30000000;                           %Young of cylindre at ambiante
Ect(2)=25000000;                          %Young of cylindre at temperature
B(2)=51;                                  %ID of flange
A(2)=58.375;                              %OD of flange
g1(2)=0.823;                              %thickness of hub big end
g0(2)=0.625;                              %thickness of cylinder
h(2)=1.25;                                %hub lenght

%Variable pour calculer l'interface hub/flange
vf(2)=0.3;                                %poison of flange
Ef(2)=30000000;                           %Young of flange at ambiante
Eft(2)=25000000;                          %Young of flange at temperature
tf(2)=5.625;                              %thickness of flange

%  VARIABLE IND�PENDANTES

% Variable pour le gasket
vg=0.4;                                   %Poisson of Gasket
tg=0.063;                                 %Epaisseur du gasket
N=0.5;                                    %Contact Width of gasket
OD=53.125;                                %Gasket OD
ID=52.125;                                %Gasket ID

% Bolt properties       
d=1;                                      %Nominal Diameter of bolt
C=56.25;                                  %Bolt Circular Diameter
n=76;                                     %number of bolt
nt=8;                                     %number of threads per inch
Eb=30000000;                              %Young Modulud of bolts
Ebt=25000000;                             %Young Modulud of bolts at temperature
kw=0;                                     %Belleville_rigidity

%**************************************************************************


  Dg=[ 0	    0	      0	         0	       0	     0	       0
    0.0019	 0.0026	    0.0033	  0.0040	0.0045	  0.0050	0.0054
    0.0055	 0.0075	    0.0082	  0.0088	0.0093	  0.0094	0.0095
    0.007	 0.0084634	0.00953	  0.01039	0.01108	  0.01144	0.0119
    0.0090	 0.0114	    0.0121	  0.0125	0.0128	  0.0134	0.0137
    0.0115	 0.0127	    0.0134	  0.0141	0.01481	  0.01526	0.0156
    0.0131	 0.0147	    0.0152	  0.0157	0.0164	  0.0171	0.0180
    0.01462  0.0165	    0.01687	  0.01724	0.01788	  0.01866	0.0199];
						
  Sg=[0	        0	      0	         0	       0	     0	       0
      0	       180	     290	    500	      700	    900	     1047
      0	       200	     300	    500	     1000	   1500	     2500
      0	       200	     500	   1000	     2000	   3000	     4550
      0	       500	    1000	   2000	     3000	   5000	     8061
      0	       950	    1502	   3200	     6000	  10000	    13360
      0	      1074	    2000	   4000	     8000	  15000	    24732
      0	      1300	    3000	   5360	    10000	  20600	    40000];



%Dg=Dg.*10;

%donn�es Thermique
hos=0.0951;                                 %heat_film_transfer_coefficient_for_flange_outside_surface_
hi=0.476;                                   %heat_film_transfer_coefficient_for_flange_inside_surface
hfb=31.96;                                  %heat_film_transfer_coefficient_from_flange_to_bolt
hcb=9.51;                                   %heat_film_transfer_coefficient_from_flange_to_bolt_head
kf=5.87;                                    %flange_thermal_conductivity
ks=5.87;                                    %shell_thermal_conductivity
alfab=7.777e-6;                             %bolt_thermal_expansion_coefficient
alfag=4.77e-6;                               %gasket_thermal_expansion_coefficient
alfaf(1)=6.94e-6;                           %fange_thermal_expansion_coefficient
alfaf(2)=6.94e-6;                           %fange_thermal_expansion_coefficient
alfac(1)=6.94e-6;                           %shell_thermal_expansion_coefficient
alfac(2)=6.94e-6;                           %shell_thermal_expansion_coefficient

%Gasket creep data
N_stress(1)=2;                              %creep_stress_function_type
a_stress(1)=0;%5.617e-4;                       %creep_stress_constant_1
b_stress(1)=0;%-3.764e-3;                      %creep_stress_constant_2
c_stress(1)=1;                              %creep_stress_constant_3
d_stress(1)=0;                              %creep_stress_constant_4

N_time(1)=2;                                %creep_time_function_type
a_time(1)=0;                                %creep_time_constant_1
b_time(1)=0;                                %creep_time_constant_2
c_time(1)=0.001;%-2.3741;                          %creep_time_constant_3
d_time(1)=0;                                %creep_time_constant_4
creep_member(1)=1;


%Bolt creep data
N_stress(2)=6;                              %creep_time_function_type
a_stress(2)=4.8e-37/24;                     %creep_stress_constant_1
b_stress(2)=6.9;                            %creep_stress_constant_2
c_stress(2)=0;                              %creep_stress_constant_3
d_stress(2)=0;                              %creep_stress_constant_4

N_time(2)=6;                                %creep_time_function_type
a_time(2)=1;                                %creep_stress_constant_1
b_time(2)=1;                                %creep_stress_constant_2
c_time(2)=0;                                %creep_stress_constant_3
d_time(2)=0;                                %creep_stress_constant_4
creep_member(2)=0;

%Flange creep data
n_f(1)=1;                                      %flange_creep_constant_n
A_f(1)=7.5e-17/146^5.5;                        %flange_creep_constant_A
m_f(1)=5.5;                                    %flange_creep_constant_m
n_f(2)=1;                                      %flange_creep_constant_n
A_f(2)=7.5e-17/146^5.5;                        %flange_creep_constant_A
m_f(2)=5.5;                                    %flange_creep_constant_m
creep_member(3)=0;

%cylinder creep data
n_c(1)=n_f(1);                                    %shell_creep_constant_n
A_c(1)=A_f(1);                                    %shell_creep_constant_A
m_c(1)=m_f(1);                                    %shell_creep_constant_m
n_c(2)=n_f(2);                                    %shell_creep_constant_n
A_c(2)=A_f(2);                                    %shell_creep_constant_A
m_c(2)=m_f(2);                                    %shell_creep_constant_m
creep_member(4)=0;

%Variable des conditions initiales
Pressure=0;%270;                          %nominal_pressure
Ext_Moment=0;                               %external_bending_moment
Ext_Force=0;                                %external_axial_force
BoltStress=45000;                           %initial_bolt_stress
ti=797;                                     %internal_operating_temperature
to=77;                                      %external_ambient_temperature


unit=0;                                     %unit�
Seating_load=1;
Operating_load=1;
External_load=0;
Thermal_load=0;
Relaxation_load=0;





