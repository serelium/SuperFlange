function [Ecreep_or_Time]=creep_function(N,Ecreep_Gas1,Ecreep_Bol1,Ecreep_Gas2,Ecreep_Bol2,t_eqGas,t_eqBol,StrGas,StrBol,N_F,N_G,a_Str,b_Str,c_Str,d_Str,a_time,b_time,c_time,d_time);

%syms s t real;
Ecreep_dum(1)=Ecreep_Gas1;
Ecreep_dum(2)=Ecreep_Bol1;
s(1)=StrGas;
s(2)=StrBol;
Ecreep(1)=Ecreep_Gas2;
Ecreep(2)=Ecreep_Bol2;
t(1)=t_eqGas;
t(2)=t_eqBol;


for j =1:2
  switch N_F(j)
    case 1
        Fs(j)=a_Str(j)*log(s(j))+b_Str(j);
    case 2
        Fs(j)=a_Str(j)*s(j)^b_Str(j)+c_Str(j);
    case 3
        Fs(j)=a_Str(j)*s(j)^b_Str(j)*exp(c_Str(j)*s(j))+d_Str(j);
    case 4
        Fs(j)=a_Str(j)*exp(b_Str(j)*s(j))+c_Str(j);
    case 5
        Fs(j)=a_Str(j)*((exp(b_Str(j)*s(j))-exp(-b_Str(j)*s(j)))/2)^c_Str(j)+d_Str(j);
    case 6
        Fs(j)=1;
  end;
  

  if N==1 
     switch N_G(j)
       case 1
           Gt(j)=a_time(j)*log(b_time(j)*t(j)+c_time(j));
       case 2
           Gt(j)=a_time(j)*t(j)^b_time(j)+c_time(j)*t(j)+d_time(j);
       case 3
           Gt(j)=(1+a_time(j)*t(j)^b_time(j))*exp(c_time(j)*t(j));
       case 4
           Gt(j)=a_time(j)*log(t(j))+b_time(j)*t(j)+c_time(j);
       case 5
           Gt(j)=a_time(j)*exp(b_time(j)*t(j))+c_time(j)*t(j)+d_time(j);
       case 6
           Gt(j)=1;
     end;
  
     Ecreep_or_Time(j)=Fs(j)*Gt(j);
     if Ecreep_or_Time(j)==1 
        Ecreep_or_Time(j)=0;
     end
  else
     [Time]=Newton(N_G(j),j,(Ecreep_dum(j)+Ecreep(j))/Fs(j),t(j),a_Str,b_Str,c_Str,d_Str,a_time,b_time,c_time,d_time);
     Ecreep_or_Time(j)=Time;
  end;
  
end;


%{---------------------------------------------------------------------------}
function [f,df]=funcd(n,i,y,t,a_Str,b_Str,c_Str,d_Str,a_time,b_time,c_time,d_time);
switch n
       case 1
           f=a_time(i)*log(b_time(i)*t+c_time(i))-y;
           df=a_time(i)*b_time(i)/(b_time(i)*t+c_time(i));
       case 2
           f=a_time(i)*t^b_time(i)+c_time(i)*t+d_time(i)-y;
           df=a_time(i)*b_time(i)*t^(b_time(i)-1)+c_time(i);
       case 3
           f=(1+a_time(i)*t^b_time(i))*exp(c_time(i)*t)-y;
           df=c_time(i)*(1+a_time(i)*t^b_time(i));
           df=(df+a_time(i)*b_time(i)*t^(b_time(i)-1))*exp(c_time(i)*t);
       case 4
           f=a_time(i)*log(t)+b_time(i)*t+c_time(i)-y;
           df=a_time(i)/t+b_time(i);
       case 5
           f=a_time(i)*exp(b_time(i)*t)+c_time(i)*t+d_time(i)-y;
           df=a_time(i)*b_time(i)*exp(b_time(i)*t)+c_time(i);
       case 6
           f=0;
           df=1;
 end;

%{----------------------------------------------------------------------------}

function [Time]=Newton(N,i,y,t_guess,a_Str,b_Str,c_Str,d_Str,a_time,b_time,c_time,d_time);
JMAX=20;

rtnewt=t_guess;
for j=1:JMAX
    [f,df]=funcd(N,i,y,rtnewt,a_Str,b_Str,c_Str,d_Str,a_time,b_time,c_time,d_time);
    dx=f/df;
    rtnewt=rtnewt-dx;
    if (abs(dx) < t_guess/1000 ) break, end
end;
Time=rtnewt;

