
function [G_new,kg_new]=fg_position(ContWidthGas,Ag,Bg,Cgi,Cgo,C,d_hole,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
    n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old,G_Fgi)
% Calculate non-uniform gasket stress distribution and new location of reaction}



N= 250;   %   {Le nombre pair d'intervals pour le calcul de la position de Fg}
Precision=1;
init_total_Rotation=init_TetaF1-init_TetaF2;
final_total_Rotation=final_TetaF1-final_TetaF2;
ug_Fgi=ugmi+init_total_Rotation*(G_Fgi-G_Sgmi)/2;
if Seat_flag==0
    ug_Fg=ug_Fgi-(LoaInBol-GasketForce)/kg;
else
    ug_Fg=ug_Fgi;
end
G_old_temp=G_old;
Precision_min=0.0001; %ContWidthGas/Ag/100;
while Precision > Precision_min
  Mg_in=0;
  Mg_out=0;
  Fg_in=0;
  Fg_out=0;
  Mg_hin=0;
  Mg_hout=0;
  Fg_hin=0;
  Fg_hout=0;
  dG_in=(G_old_temp-Bg)/N;
  for I=0:N
      G_r(I+1)=Bg+I*dG_in;
  end    
  dG_out=(Ag-G_old_temp)/N;
  for I=N+1:2*N
      G_r(I+1)=G_old_temp+(I-N)*dG_out;
  end
  if Seat_flag==0;
     ug_ri=ugmi+init_total_Rotation*(G_r-G_Sgmi)/2;
     ug_r=ug_Fg+final_total_Rotation*(G_r-G_old_temp)/2;
     [kf,Sg_r]=ui_uf_sf_k(ug_ri,ug_r);
  else
     ug_ri=ugmi+init_total_Rotation*(G_r-G_Sgmi)/2;
     ug_r=ug_ri;
     [kf,Sg_r]=ui_uf_sf_k(ug_ri,ug_r);
  end

 
  for I=1:2:N-1
      Fg_in=Fg_in+pi*(Sg_r(I)*G_r(I)+4*Sg_r(I+1)*G_r(I+1)+Sg_r(I+2)*G_r(I+2))*dG_in/6;
      Mg_in=Mg_in+pi*(Sg_r(I)*(G_r(I))^2+4*Sg_r(I+1)*(G_r(I+1))^2+Sg_r(I+2)*(G_r(I+2))^2)*dG_in/6;
      if (G_r(I)>Cgi) & (G_r(I)<Cgo) & (G_r(I+1)<Cgo) & (G_r(I+2)<Cgo)
         Fg_hin=Fg_hin+(Sg_r(I)*(d_hole^2-(G_r(I)-C)^2)^.5+4*Sg_r(I+1)*(d_hole^2-(G_r(I+1)-C)^2)^.5+Sg_r(I+2)*(d_hole^2-(G_r(I+2)-C)^2)^.5)*dG_in/6;
         Mg_hin=Mg_hin+(G_r(I)*Sg_r(I)*(d_hole^2-(G_r(I)-C)^2)^.5+4*G_r(I+1)*Sg_r(I+1)*(d_hole^2-(G_r(I+1)-C)^2)^.5+G_r(I+2)*Sg_r(I+2)*(d_hole^2-(G_r(I+2)-C)^2)^.5)*dG_in/6;
      end   
  end
  Fg_hin=n*Fg_hin;
  Mg_hin=n*Mg_hin;
%  Fg_in=trapz(pi*G_r.^2/4,Sg_r);
%  Mg_in=trapz(G_r.^3/4,Sg_r);

  for I=N+1:2:2*N-1
      Fg_out=Fg_out+pi*(Sg_r(I)*G_r(I)+4*Sg_r(I+1)*G_r(I+1)+Sg_r(I+2)*G_r(I+2))*dG_out/6;
      Mg_out=Mg_out+pi*(Sg_r(I)*(G_r(I))^2+4*Sg_r(I+1)*(G_r(I+1))^2+Sg_r(I+2)*(G_r(I+2))^2)*dG_out/6;
      if (G_r(I)>Cgi) & (G_r(I)<Cgo) & (G_r(I+1)<Cgo) & (G_r(I+2)<Cgo)
         Fg_hout=Fg_hout+(Sg_r(I)*(d_hole^2-(G_r(I)-C)^2)^.5+4*Sg_r(I+1)*(d_hole^2-(G_r(I+1)-C)^2)^.5+Sg_r(I+2)*(d_hole^2-(G_r(I+2)-C)^2)^.5)*dG_out/6;
         Mg_hout=Mg_hout+(G_r(I)*Sg_r(I)*(d_hole^2-(G_r(I)-C)^2)^.5+4*G_r(I+1)*Sg_r(I+1)*(d_hole^2-(G_r(I+1)-C)^2)^.5+G_r(I+2)*Sg_r(I+2)*(d_hole^2-(G_r(I+2)-C)^2)^.5)*dG_out/6;
      end
  end
  Fg_hout=n*Fg_hout;
  Mg_hout=n*Mg_hout;
%  Fg_out=trapz(pi*G_r.^2/4,Sg_r);
%  Mg_out=trapz(pi*G_r.^3/4,Sg_r);

Fg=Fg_in+Fg_out-Fg_hin-Fg_hout;
Mg=Mg_in+Mg_out-Mg_hin-Mg_hout;
G_new=Mg/Fg;
Precision=abs((G_new-G_old_temp)/((Ag+Bg)/2));
G_old_temp=abs(G_new);
N=N*2;
end


N=N/2;
k_in=0;
k_out=0;
for I=0:N-1
     k_in=k_in+(1+init_total_Rotation*(G_r(I+1)-G_old_temp)/ug_Fg)*pi*G_r(I+1)*dG_in*kf(I+1)/2;
%     k_in=k_in+pi*G_r(I+1)*dG_in*kf(I+1)/2;
end    
for I=0:N
     k_out=k_out+(1+init_total_Rotation*(G_r(N+I+1)-G_old_temp)/ug_Fg)*pi*G_r(N+I+1)*dG_out*kf(N+I+1)/2;
%     k_out=k_out+pi*G_r(N+I+1)*dG_out*kf(N+I+1)/2;
end    
kg_new=k_in+k_out;  
%kg=kg_new;%(kg_old+kg_new)/2;  
