function [m,Y]=matrices(vc,vf,Ec,Ef,B,A,g1,g0,h,d,C,n,tf,OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
FlangeType,ShellHeadType,NbFlange,Pressure,P_Eq,N,LoaInBol,init_TetaF,rd_Temp,rot_Temp,rd_creep,rot_creep);


global StifPl;

%Variable pour le gasket
%AreaGas= pi*(OD^2-(OD-2*N)^2)/4;
%kg= Eg*AreaGas/tg;
EtaG=OD^2*(OD-2*N)^2/(OD^2-(OD-2*N)^2);

Flange_ring=0;

%***************************************************************************************************
%   MATRICE m et vecteur Y
%***************************************************************************************************

m=zeros(13,13);     %initialise a zero la matrice de m et Y 
Y=zeros(13,1);      


switch FlangeType   %Type de flange: 1=WN, 2=SlipOn, 3=Lapped, 4=Threaded, 5= Socket, 6=Blind
case 1

    Flange_ring=1;

    %Variable pour calculer l'interface cylindre/hub 
    alpha=abs(atan((g1-g0)/h));             %hub tapper angle
    ah=(g0+g1)/4+B/2;                       %hub mean radius
    x=[g0/(tan(alpha)),      g0/(tan(alpha))+h];     %x[x1,x2]
    Rho=(12*(1-vf^2)/(alpha*ah)^2)^0.25;
    epsilon=2*Rho*x.^.5;

    [ber,dber,bei,dbei,ker,dker,kei,dkei]=kelvin_function(epsilon);

    asz=size(epsilon);
    
    for j=1:asz(1,2);
        S(j,1)=-epsilon(j)*bei(j)-2*dber(j);
        S(j,2)=epsilon(j)*ber(j)-2*dbei(j);
        S(j,3)=-epsilon(j)*kei(j)-2*dker(j);
        S(j,4)=epsilon(j)*ker(j)-2*dkei(j);

        S(j,5)=4*epsilon(j)*bei(j)      +8*dber(j)  -epsilon(j)^2*dbei(j);
        S(j,6)=-4*epsilon(j)*ber(j)     +8*dbei(j)  +epsilon(j)^2*dber(j);
        S(j,7)=4*epsilon(j)*kei(j)      +8*dker(j)  -epsilon(j)^2*dkei(j);
        S(j,8)=-4*epsilon(j)*ker(j)     +8*dkei(j)  +epsilon(j)^2*dker(j);
    
        S(j,9)=     -epsilon(j)^3*ber(j)    -24*epsilon(j)*bei(j)   -48*dber(j)    +8*epsilon(j)^2*dbei(j);
        S(j,10)=    -epsilon(j)^3*bei(j)    +24*epsilon(j)*ber(j)   -48*dbei(j)    -8*epsilon(j)^2*dber(j);
        S(j,11)=    -epsilon(j)^3*ker(j)    -24*epsilon(j)*kei(j)   -48*dker(j)    +8*epsilon(j)^2*dkei(j);
        S(j,12)=    -epsilon(j)^3*kei(j)    +24*epsilon(j)*ker(j)   -48*dkei(j)    -8*epsilon(j)^2*dker(j);
    end

    m(1,1)=     x(1)^-0.5      *dber(1);        %dber(no de flange, epsilon(1))
    m(1,2)=     x(1)^-0.5      *dbei(1);        %dbei(no de flange, epsilon(1))
    m(1,3)=     x(1)^-0.5      *dker(1);        %kber(no de flange, epsilon(1))
    m(1,4)=     x(1)^-0.5      *dkei(1);        %kbei(no de flange, epsilon(1))
    m(1,9)=-1;


    m(2,1)=     (x(1)^-1.5)/2     *S(1,1);      %S(no de flange,  epsilon(1),  S1)
    m(2,2)=     (x(1)^-1.5)/2     *S(1,2);      %S(no de flange,  epsilon(1),  S2)
    m(2,3)=     (x(1)^-1.5)/2     *S(1,3);      %S(no de flange,  epsilon(1),  S3)
    m(2,4)=     (x(1)^-1.5)/2     *S(1,4);      %S(no de flange,  epsilon(1),  S4)
    m(2,10)=1;

    m(3,1)=     (Ef*alpha^3*x(1)^0.5*Rho^2)      /(24*(1-vf^2))    *S(1,2);       %c'est quoi P poisson est de la flange ou du hub???
    m(3,2)=     -(Ef*alpha^3*x(1)^0.5*Rho^2)     /(24*(1-vf^2))    *S(1,1);       %c'est quoi P poisson est de la flange ou du hub???
    m(3,3)=     (Ef*alpha^3*x(1)^0.5*Rho^2)      /(24*(1-vf^2))    *S(1,4);       %c'est quoi P poisson est de la flange ou du hub???
    m(3,4)=     -(Ef*alpha^3*x(1)^0.5*Rho^2)     /(24*(1-vf^2))    *S(1,3);       %c'est quoi P poisson est de la flange ou du hub???
    m(3,5)=-1;

    m(4,1)=     (Ef*alpha^3*x(1)^0.5)      /(48*(1-vf^2))    *S(1,5);        %S(no de flange,  epsilon(1),  S5)
    m(4,2)=     (Ef*alpha^3*x(1)^0.5)      /(48*(1-vf^2))    *S(1,6);        %S(no de flange,  epsilon(1),  S6)
    m(4,3)=     (Ef*alpha^3*x(1)^0.5)      /(48*(1-vf^2))    *S(1,7);        %S(no de flange,  epsilon(1),  S7)
    m(4,4)=     (Ef*alpha^3*x(1)^0.5)      /(48*(1-vf^2))    *S(1,8);        %S(no de flange,  epsilon(1),  S8)
    m(4,6)=-1;

    m(5,1)=     x(2)^-0.5     *dber(2);                                         %dber(no de flange, epsilon(2))
    m(5,2)=     x(2)^-0.5     *dbei(2);                                         %dbei(no de flange, epsilon(2))
    m(5,3)=     x(2)^-0.5     *dker(2);                                         %kber(no de flange, epsilon(2))
    m(5,4)=     x(2)^-0.5     *dkei(2);                                         %kbei(no de flange, epsilon(2))
    m(5,11)=-1;


    m(6,1)=     (x(2)^-1.5)/2     *S(2,1);                                      %S(no de flange,  epsilon(2),  S1)
    m(6,2)=     (x(2)^-1.5)/2     *S(2,2);                                      %S(no de flange,  epsilon(2),  S2)
    m(6,3)=     (x(2)^-1.5)/2     *S(2,3);                                      %S(no de flange,  epsilon(2),  S3)
    m(6,4)=     (x(2)^-1.5)/2     *S(2,4);                                      %S(no de flange,  epsilon(2),  S4)
    m(6,12)=1;

    m(7,1)=     (Ef*alpha^3*x(2)^0.5*Rho^2)      /(24*(1-vf^2))    *S(2,2);       %c'est quoi P poisson est de la flange ou du hub???
    m(7,2)=     -(Ef*alpha^3*x(2)^0.5*Rho^2)     /(24*(1-vf^2))    *S(2,1);       %c'est quoi P poisson est de la flange ou du hub???
    m(7,3)=     (Ef*alpha^3*x(2)^0.5*Rho^2)      /(24*(1-vf^2))    *S(2,4);       %c'est quoi P poisson est de la flange ou du hub???
    m(7,4)=     -(Ef*alpha^3*x(2)^0.5*Rho^2)     /(24*(1-vf^2))    *S(2,3);       %c'est quoi P poisson est de la flange ou du hub???
    m(7,7)=-1;

    m(8,1)=     (Ef*alpha^3*x(2)^0.5)      /(48*(1-vf^2))    *S(2,5);        %S(no de flange,  epsilon(1),  S5)
    m(8,2)=     (Ef*alpha^3*x(2)^0.5)      /(48*(1-vf^2))    *S(2,6);        %S(no de flange,  epsilon(1),  S6)
    m(8,3)=     (Ef*alpha^3*x(2)^0.5)      /(48*(1-vf^2))    *S(2,7);        %S(no de flange,  epsilon(1),  S7)
    m(8,4)=     (Ef*alpha^3*x(2)^0.5)      /(48*(1-vf^2))    *S(2,8);        %S(no de flange,  epsilon(1),  S8)
    m(8,8)=-1;

    Y(1)=-(2-vf)    *(Pressure*ah^2)            /(2*Ef*alpha*x(1))-rd_Temp(NbFlange,2)-rd_creep(NbFlange,2);    
    Y(2)=(2-vf)     *(Pressure*ah^2)            /(2*Ef*alpha*(x(1))^2)-rot_Temp(NbFlange,2)+rot_creep(NbFlange,2);
    Y(3)=-alpha*ah  *Pressure/4;
    Y(4)=-(2-vf)    *(Pressure*ah^2*alpha^2)    /(12*(1-vf^2));         
    Y(5)=-(2-vf)    *(Pressure*ah^2)            /(2*Ef*alpha*x(2))-rd_Temp(NbFlange,3)-rd_creep(NbFlange,3);
    Y(6)=(2-vf)     *(Pressure*ah^2)            /(2*Ef*alpha*(x(2))^2)-rot_Temp(NbFlange,3)+rot_creep(NbFlange,3);
    Y(7)=-alpha*ah  *Pressure/4;
    Y(8)=-(2-vf)    *(Pressure*ah^2*alpha^2)    /(12*(1-vf^2));
    
    
case {2,3,4,5}
    
    Flange_ring=1;
    for i=1:6
        m(i,i)=1;
    end;
    m(5,7)=-1;
    m(6,8)=-1;
    m(7,9)=1;
    m(7,11)=-1;
    m(8,10)=1;
    m(8,12)=-1;
 
case 6              % Blind cover properties       

    for i=1:12
        m(i,i)=1;
    end;
    EtaP=((1-vf)*G_old/A)^2;
    EtaP=EtaP+1-vf^2;
    RigPl=Ef*tf^3;
    RigPl=RigPl/12/(1-vf^2);
    BetaPl=C/A;
    for i=2:-1:1                       % 2=stifness at the bolt 1=stiffness at the gasket
        C0 = 1-BetaPl^2;
        C1 = 1-BetaPl^4;
        K1 = (1+vf)*log(BetaPl);
        K2 = (1-vf)*C0-2*K1;
        K3 = (4-(1-vf)*BetaPl^2-4*K1)*BetaPl^2;
        K1 = (3+vf)*C0+2*BetaPl^2*K1;
        StifPl(NbFlange,1,i)= 64*pi*RigPl*(1+vf)/(A^2*(K1-K2+K2*C0));
        StifPl(NbFlange,2,i)= 64*pi*RigPl*(1+vf)/(A^2*(K2*(4*BetaPl/A)));
        BetaPl =G_old/A;
    end;
    A0 = (4-5*BetaPl^2+4*(2+BetaPl^2)*log(BetaPl))*BetaPl^2;
    A0 = 1+A0+2*K3*C0/(1+vf)-C1;
    A0 = A0*A^4/1024/RigPl;
    StifPl(NbFlange,1,3)=1/A0;
    B0 = -2*K3*(4*BetaPl/A)/(1+vf)+8*BetaPl^3/A;
    B0 = -B0*A^4/1024/RigPl;
    StifPl(NbFlange,2,3)=1/B0;
    m(13,13)=1/StifPl(NbFlange,1,1)-1/StifPl(NbFlange,1,2);
    Y(13)=m(13,13)*LoaInBol+(AreaPres/StifPl(NbFlange,1,1)-1/StifPl(NbFlange,1,3))*Pressure;
     
 otherwise
    
end;

% Cylindre
if ShellHeadType==1
    Betac =((12*(1-vc^2))/(B^2*g0^2))^0.25;
    m(9,5)=6*(1-vc^2)/(Ec*g0^3*Betac^3);   %dans le code pour ton logiciel il ya un signe - devant le 6
    m(9,6)=6*(1-vc^2)/(Ec*g0^3*Betac^2);    
    m(9,9)=-1;

    m(10,5)=-6*(1-vc^2)/(Ec*g0^3*Betac^2);
    m(10,6)=-12*(1-vc^2)/(Ec*g0^3*Betac);   %dans le code pour ton logiciel il ya un signe + devant le 6
    m(10,10)=-1;

    Y(9)=-(2-vc)*(Pressure*B^2)/(8*Ec*g0)-rd_Temp(NbFlange,1)-rd_creep(NbFlange,1);             
    Y(10)=-rot_Temp(NbFlange,1)+rot_creep(NbFlange,1);
end;


% Flange
if Flange_ring==1
    K=A/B;
    gama=(A^2+B^2)/(A^2-B^2)+vf;
    bolt_hole=d;                                                %bolt hole should be 0.125 in greater
    D0=(A-B)/log(K);
    D0=D0-(bolt_hole/(pi*C/n)*(D0-2*d/log((C+d)/(C-d))));
    EtaF=((A^2/G_old^2)*(1+vf)+(1-vf))/(K^2-1);
    if K>1.162
       YA=(3*(1-vf)+6*(1+vf)*K^2*log(K)/(K^2-1))/pi;      %Plate Theory
       YA=YA/(D0*(1/B-1/A));
    else
       YA=6/pi/log(K);                                          %Ring Theory
    end
    if K>1.162                     %{---Effect of plate subjected to bending---}%  
       m(13,12)=3*B*(((1-vf)*(G_old^2-C^2))/B^2+2*(1+vf)*K^2*log(G_old/C))/(2*pi*YA*(1-K^2));
%       m(13,12)=m(13,12)*D0/B;
    else                           %{----Theory of rings----}
       m(13,12)= (C-G_old)/2;
    end

    m(11,7)=-B*gama/(2*Ef*tf);
    m(11,11)=-1;
    m(11,12)=tf/2;

    m(12,7)=-B*tf/(2*D0);
    m(12,8)=(B+g1)/D0;
    m(12,12)=-Ef*tf^3/(B*pi*YA);
    m(12,13)=(C-G_old)/(2*pi*D0);

%    m(13,13)=1/kg+1/kb;

    Y(11)=-gama*B   *(Pressure)                 /(2*Ef)-rd_Temp(NbFlange,4)-rd_creep(NbFlange,4);
    Y(12)=-(G_old-B)*(G_old^2+B^2)*Pressure     /(16*D0);   
    Y(12)=Y(12)-(D0-G_old)*G_old^2*P_Eq/8/D0;
    Y(13)=m(13,12)*init_TetaF;    
end;

% Changer le signe pour les rotations pour la bride inférieure
if NbFlange==2
   m(2,10)=-m(2,10);
   m(6,12)=-m(6,12);
   m(10,10)=-m(10,10);
   m(11,12)=-m(11,12);
   m(13,12)=-m(13,12);
   m(12,12)=-m(12,12);

end;
  
