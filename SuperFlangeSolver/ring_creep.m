function [d_rot_creep_f,d_rd_creep_f]=ring_creep(a,m,n,final_TetaF1,final_TetaF2,E,vf,A,B,tf,Pressure,t,t_inc,P1,P2,fid);

global epsi_r_cr_f1 epsi_r_cr_f2 epsi_t_cr_f1 epsi_t_cr_f2;
    
    u=-(n(1)-1)/n(1);
    k=1;                %Creep of flange 1
    dt=tf(k)/20;
    dr=(A(k)-B(k))/40;
    
    
    for i=1:21
        for j=1:21
           
            z=(11-j)*dt;
            r=B(k)/2+(i-1)*dr;
            %uf=(Pressure-P1/tf(k))*((1-vf(k))*r+(1+vf(k))*(A(k)/2)^2/r)/E(k)/((A(k)/B(k))^2-1)+final_TetaF1*z;
            %d_uf=(Pressure-P1/tf(k))*(1-vf(k)-1/4*(1+vf(k))*A(k)^2/r^2)/E(k)/(A(k)^2/B(k)^2-1);
            %sigma_t=E(k)/(1-vf(k)^2)*(uf/r+vf(k)*d_uf);
            %sigma_r=E(k)/(1-vf(k)^2)*(d_uf+vf(k)*uf/r); 
            
            sigma_t=(Pressure-P1/tf(k))*(B(k)/2)^2*(1+(A(k)/2)^2/r^2)/((A(k)/2)^2-(B(k)/2)^2)+E(k)*final_TetaF1*(11-j)*dt/r; 
            sigma_r=(Pressure-P1/tf(k))*(B(k)/2)^2*(1-(A(k)/2)^2/r^2)/((A(k)/2)^2-(B(k)/2)^2); 
            sigma_t=sigma_t-E(k)/(1-vf(k)^2)*(epsi_t_cr_f1(i,j)+vf(k)*epsi_r_cr_f1(i,j));
%            sigma_r=sigma_r-E(k)/(1-vf(k)^2)*(epsi_r_cr_f1(i,j)+vf(k)*epsi_t_cr_f1(i,j));
            Sr=1/3*(2*sigma_r-sigma_t);
            St=1/3*(2*sigma_t-sigma_r);
            Sz=1/3*(-sigma_r-sigma_t);
            sigma_e=1/sqrt(2)*sqrt(sigma_t^2+sigma_r^2+(sigma_t-sigma_r)^2);
        
              
            epsi_cre=a(1)*sigma_e^m(1)*t^n(1);
            epsi_pr_cr(i,j)=3/2*Sr*n(1)*a(1)^(1/n(1))*sigma_e^(m(1)/n(1)-1)*epsi_cre^-u;
            epsi_pt_cr(i,j)=3/2*St*n(1)*a(1)^(1/n(1))*sigma_e^(m(1)/n(1)-1)*epsi_cre^-u;
%            epsi_pl_cr(i,j)=3/2*Sl*n(1)*a(1)^(1/n(1))*sigma_e^(m(1)/n(1)-1)*epsi_cre^-u;
            epsi_r_cr_f1(i,j)=epsi_r_cr_f1(i,j)+epsi_pr_cr(i,j)*t_inc;
            epsi_t_cr_f1(i,j)=epsi_t_cr_f1(i,j)+epsi_pt_cr(i,j)*t_inc;
%            epsi_l_cr_f1(i,j)=epsi_l_cr_f1(i,j)+epsi_pl_cr(i,j)*t_inc;
    
    s(j)=sigma_t;
    ep(j)=(11-j)*dt;
            epsi_p_cr(i,j)=(11-j)*dt*(epsi_pt_cr(i,j)+vf(k)*epsi_pr_cr(i,j));   
        end;
    sr(i)=sigma_r;
    epr(i)=(i-1)*dr;
    end;
    d_rd_creep_f(k)=(B(k)/2)*epsi_pt_cr(1,11)*t_inc;
    epsi_p_cr=abs(epsi_p_cr);
    int_ec=dt*dr*trapz(trapz(epsi_p_cr));                            
    d_rot_creep_f(k)=12*t_inc*int_ec/(tf(k))^3/log(A(k)/B(k));             
%hold on
%plot(ep,s,'r.-')

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

    u=-(n(2)-1)/n(2);
    k=2;                %Creep of flange 2
    dt=tf(k)/20;
    dr=(A(k)-B(k))/40;
    
    for i=1:21
        for j=1:21
                
            z=(11-j)*dt;
            r=B(k)/2+(i-1)*dr;
            %uf=(Pressure-P2/tf(k))*((1-vf(k))*r+(1+vf(k))*(A(k)/2)^2/r)/E(k)/((A(k)/B(k))^2-1)-final_TetaF2*z;
            %d_uf=(Pressure-P2/tf(k))*(1-vf(k)-1/4*(1+vf(k))*A(k)^2/r^2)/E(k)/(A(k)^2/B(k)^2-1);
            %sigma_t=E(k)/(1-vf(k)^2)*(uf/r+vf(k)*d_uf);
            %sigma_r=E(k)/(1-vf(k)^2)*(d_uf+vf(k)*uf/r);   
            
            sigma_t=(Pressure-P2/tf(k))*(B(k)/2)^2*(1+(A(k)/2)^2/r^2)/((A(k)/2)^2-(B(k)/2)^2)-E(k)*final_TetaF2*(11-j)*dt/r; 
            sigma_r=(Pressure-P2/tf(k))*(B(k)/2)^2*(1-(A(k)/2)^2/r^2)/((A(k)/2)^2-(B(k)/2)^2); 
            sigma_t=sigma_t-E(k)/(1-vf(k)^2)*(epsi_t_cr_f2(i,j)+vf(k)*epsi_r_cr_f2(i,j));
 %           sigma_r=sigma_r-E(k)/(1-vf(k)^2)*(epsi_r_cr_f2(i,j)+vf(k)*epsi_t_cr_f2(i,j));
            Sr=1/3*(2*sigma_r- sigma_t);
            St=1/3*(2*sigma_t-sigma_r);
            Sz=1/3*(-sigma_r-sigma_t);
            sigma_e=1/sqrt(2)*sqrt(((sigma_t)^2+(sigma_r)^2+(sigma_t-sigma_r)^2));
        
         
            epsi_cre=a(2)*sigma_e^m(2)*t^n(2);
            epsi_pr_cr(i,j)=3/2*Sr*n(2)*a(2)^(1/n(2))*sigma_e^(m(2)/n(2)-1)*epsi_cre^-u;
            epsi_pt_cr(i,j)=3/2*St*n(2)*a(2)^(1/n(2))*sigma_e^(m(2)/n(2)-1)*epsi_cre^-u;
            epsi_r_cr_f2(i,j)=epsi_r_cr_f2(i,j)+epsi_pr_cr(i,j)*t_inc;
            epsi_t_cr_f2(i,j)=epsi_t_cr_f2(i,j)+epsi_pt_cr(i,j)*t_inc;
    
            epsi_p_cr(i,j)=(11-j)*dt*(epsi_pt_cr(i,j)+vf(k)*epsi_pr_cr(i,j));   
        end;
    end;
    d_rd_creep_f(k)=(B(k)/2)*epsi_pt_cr(1,11)*t_inc;
    epsi_p_cr=abs(epsi_p_cr);
    int_ec=dt*dr*trapz(trapz(epsi_p_cr));                            
    d_rot_creep_f(k)=12*t_inc*int_ec/(tf(k))^3/log(A(k)/B(k));                
    
%fprintf(fid,'%1i\n',t,s);    