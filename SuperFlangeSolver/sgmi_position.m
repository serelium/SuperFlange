
function G_Sgmi=sgmi_position(ContWidthGas,Ag,Bg,Cgi,Cgo,C,d_hole,LoaInBol,kg,AreaGas,GasketForce,...
    n,init_TetaF1,init_TetaF2,ugmi,G_Sgmi,G_Fgi,G_old);

% Calculate the location of the gasket mean stress}

K=0;
L=0;
M=0;
Fg1=0;
Fg2=0;
N=250;                                 %Le nombre d'intervals pour le calcul de la position de Sgm}
%G_Sgmi=2*(Ag^2+Bg^2+Ag*Bg)/3/(Ag+Bg);   %Position de la reaction avec distribution uniform}
Fg=0;
h=ContWidthGas/10;
if G_old>C
    h=-h;
end
init_total_Rotation=init_TetaF1-init_TetaF2;
while abs(Fg-GasketForce) > abs(0.01*GasketForce)
  Fg=0;
  Fg_hin=0;
  Fg_hout=0;
  dG_in=(G_Sgmi-Bg)/N;
  for I=0:N
      G_r(I+1)=Bg+I*dG_in;
  end    
  dG_out=(Ag-G_Sgmi)/N;
  for I=N+1:2*N
      G_r(I+1)=G_Sgmi+(I-N)*dG_out;
  end
%  ug_r=ugmi+init_total_Rotation*(G_r-G_Sgmi)/2;
%  Sg_r=Flag2(ug_r);

  ug_ri=ugmi-init_total_Rotation*(G_Sgmi-G_r)/2;
  ug_r=ug_ri;
  [kf,Sg_r]=ui_uf_sf_k(ug_ri,ug_r);

  for I=1:2:N-1
      Fg=Fg+pi*(Sg_r(I)*G_r(I)+4*Sg_r(I+1)*G_r(I+1)+Sg_r(I+2)*G_r(I+2))*dG_in/6;
      if (G_r(I)>Cgi) & (G_r(I)<Cgo) & (G_r(I+1)<Cgo) & (G_r(I+2)<Cgo)
         Fg_hin=Fg_hin+(Sg_r(I)*(d_hole^2-(G_r(I)-C)^2)^.5+4*Sg_r(I+1)*(d_hole^2-(G_r(I+1)-C)^2)^.5+Sg_r(I+2)*(d_hole^2-(G_r(I+2)-C)^2)^.5)*dG_in/6;
      end   
  end
  
  %  Fg=trapz(pi*G_r.^2/4,Sg_r);
  
  for I=N+1:2:2*N-1
      Fg=Fg+pi*(Sg_r(I)*G_r(I)+4*Sg_r(I+1)*G_r(I+1)+Sg_r(I+2)*G_r(I+2))*dG_out/6;
      if (G_r(I)>Cgi) & (G_r(I)<Cgo) & (G_r(I+1)<Cgo) & (G_r(I+2)<Cgo)
         Fg_hout=Fg_hout+(Sg_r(I)*(d_hole^2-(G_r(I)-C)^2)^.5+4*Sg_r(I+1)*(d_hole^2-(G_r(I+1)-C)^2)^.5+Sg_r(I+2)*(d_hole^2-(G_r(I+2)-C)^2)^.5)*dG_out/6;
      end
  end
    
  %  Fg=Fg+trapz(pi*G_r.^2/4,Sg_r);

  Fg=Fg-n*(Fg_hin+Fg_hout);
  
  if GasketForce > Fg
      G_Sgmi=G_Sgmi-2*h;
%     Fg1=Fg;
      L=1;
      K=1;
  else
      G_Sgmi=G_Sgmi+2*h;
%     Fg2=Fg;
      M=1;
      K=-1;
  end

  if (L==1)&(M==1)
      h=h/2;
      G_Sgmi=G_Sgmi+K*2*h;
  end

%  if (L==1)&(M==1)
%      if K==1
%        h=h*(GasketForce-Fg1)/(Fg2-Fg1);
%      else    
%        h=h-h*(GasketForce-Fg1)/(Fg2-Fg1);
%      end
%      G_Sgmi=G_Sgmi+K*2*h;
%      Fg1=0;
%      Fg2=0;
%      L=0;
%      M=0;
%      K=0;
%  end
end
G_Sgmi=G_Sgmi+K*2*h;                                
