function ugmi=si_ui_initial(input)
% fonction qui prend comme entr�e la contrainte initiale et retourne le 
% d�placement initial
global Dg Sg;

   X=Dg;
   
   Y=Sg;
     
      
 % D�terminer le type des d�rives et de covariances pour chaque profil
epsilon=0;

derivA = 2;
covA = 3;
angleRA = 30;   

derivB = 2;
covB = 3;
angleRB = 30;

IP=length(X(:,1));
JP=length(X(1,:));
     
sref=linspace(0,1,IP); % Vecteur r�f�rence des positions param�triques des points le long du profil A
tref=linspace(0,1,JP); % Vecteur r�f�rence des positions param�triques des points le long du profil B

%*********************

% �quation param�trique : XX(skrig,tkrig,rkrig) = SP * KA^-1 * coefX * KB^-1 * TP
% O� 	SP : matrice des coefficients param�triques li� � la covariances et � la d�rive du profil A
%		KA : matrice de krigeage du profil A
%	coefX : matrice form�e � partir des coordonn�es en X des points krig�s
%		KB : matrice de krigeage du profil B
%		TP : matrice des coefficients param�triques li� � la covariances et � la d�rive du profil B
%
%  	Pour obtenir les coordonn�es en Y et en Z il suffit de remplacer XX et
% coefX par la lettre corresponndante.

	% Calcul des matrices de krigeage KA et KB

% Calcul du quadran sup�rieur gauche de la matrice KA
% Ce quadran est une matrice carr� [IP,IP] diagonale, c'est-�-dire que M(i,j)=M(j,i)
% Cette matrice est remplie de la valeur absolue de la diff�rence des coordonn�es
% param�trique le long du profil en question �lev�e � la puissance de la covariance

for i=1:IP-1
	for j=2:IP
        
        if covA < 4 % Covariances polynomiales
            KAabs(i,j)=abs(sref(i)-sref(j))^covA; % Partie sup�rieure de la diagonale de z�ro
            KAabs(j,i)=abs(sref(i)-sref(j))^covA; % Partie inf�rieure de la diagonale de z�ro   
        elseif covA == 4 % Covariance logarithmique
            KAabs(i,j)=abs(sref(i)-sref(j))^2*log(abs(sref(i)-sref(j))); % Partie sup�rieure de la diagonale de z�ro
            KAabs(j,i)=abs(sref(i)-sref(j))^2*log(abs(sref(i)-sref(j))); % Partie inf�rieure de la diagonale de z�ro
        elseif covA == 5 % Covariance sinuso�dale
            KAabs(i,j)=sin(angleRA*abs(sref(i)-sref(j))*pi/180); % Partie sup�rieure de la diagonale de z�ro
            KAabs(j,i)=sin(angleRA*abs(sref(i)-sref(j))*pi/180); % Partie inf�rieure de la diagonale de z�ro   
        end
        
    end
end

% Calcul du quadran sup�rieur gauche de la matrice KB
% Ce quadran est une matrice carr� (JP X JP) diagonale, c'est-�-dire que M(i,j)=M(j,i)
% Cette matrice est remplie de la valeur absolue de la diff�rence des coordonn�es
% param�trique le long du profil en question �lev�e � la puissance de la covariance

if JP >= 2

	for i=1:JP-1
		for j=2:JP
      
      	  if covB < 4 % Covariances polynomiales
         	   KBabs(i,j)=abs(tref(i)-tref(j))^covB; % Partie sup�rieure de la diagonale de z�ro
            	KBabs(j,i)=abs(tref(i)-tref(j))^covB; % Partie inf�rieure de la diagonale de z�ro   
	        elseif covB == 4 % Covariance logarithmique
   	         KBabs(i,j)=abs(tref(i)-tref(j))^2*log(abs(tref(i)-tref(j))); % Partie sup�rieure de la diagonale de z�ro
      	      KBabs(j,i)=abs(tref(i)-tref(j))^2*log(abs(tref(i)-tref(j))); % Partie inf�rieure de la diagonale de z�ro
        	  elseif covB == 5 % Covariance sinuso�dale
            	KBabs(i,j)=sin(angleRB*abs(tref(i)-tref(j))*pi/180); % Partie sup�rieure de la diagonale de z�ro
            	KBabs(j,i)=sin(angleRB*abs(tref(i)-tref(j))*pi/180); % Partie inf�rieure de la diagonale de z�ro   
        	  end
       
   	end
	end
   
end

% Calcul du quadran sup�rieur droit de la matrice KA
% Ce quadran est une matrice [IP,derivA+1], cette matrice est remplie du vecteur des
% coordonn�es param�triques le long du profil, positionn� en colonne, o� chaque �l�ment
% est �lev� de z�ro jusqu'� la puissance de la d�rive du profil.  C'est pourquoi la
% premi�re colonne de la matrice est compos�e de un.

KAs=[ones(IP,1)]; % Premi�re colonne de la matrice, seulement des uns
derivA = 3;

if derivA ~= 5
        
    for i=1:derivA-1 % Boucle pour remplir de la 2i�me � la derni�re colonne
    	KAs=[KAs,sref.^i']; % en variant la puissance d'�l�vation des �l�ment du vecteur colonne
    end
    
else % La d�rive est sinusoidale
   
   KAs=[KAs,sin(angleRA*pi/180*sref'),cos(angleRA*pi/180*sref')];
   
end

% Calcul du quadran sup�rieur droit de la matrice KB
% Ce quadran est une matrice [JP,derivB+1], cette matrice est remplie du vecteur des
% coordonn�es param�triques le long du profil, positionn� en colonne, o� chaque �l�ment
% est �lev� de z�ro jusqu'� la puissance de la d�rive du profil.  C'est pourquoi la
% premi�re colonne de la matrice est compos�e de un.

KBt=[ones(JP,1)]; % Premi�re colonne de la matrice, seulement des uns

if JP >= 2

	if derivB ~= 5

		for i=1:derivB-1 % Boucle pour remplir de la 2i�me � la derni�re colonne
	   	KBt=[KBt,tref.^i']; % en variant la puissance d'�l�vation des �l�ment du vecteur colonne
	   end
    
	else % La d�rive est sinusoidale
   
		KBt=[KBt,sin(angleRB*pi/180*tref'),cos(angleRB*pi/180*tref')];
   
	end
   
end

% Rassemblement de la matrice de krigeage
% La partie sup�rieure est l'union des deux matrices calcul�es pr�c�demment.
% La partie inf�rieure de la matrice de krigeage est compos�e de deux autres matrices.
% La premi�re se trouve � �tre la transpos�e de la matrice composant la partie sup�rieure
% droite et la seconde matrice est une matrice carr� pleine de z�ro de la dimension de la
% d�rive plus 1. (Exemple : Si la d�rive est lin�aire, la matrice est 2X2.)

if derivA ~= 5
   
   KA=[KAabs,KAs;KAs',zeros(derivA)]; % Rassemblement de la matrice de krigeage KA
   
else % La d�rive est sinusoidale
   
   KA=[KAabs,KAs;KAs',zeros(3)]; % Rassemblement de la matrice de krigeage KA

end

if JP >= 2
   
   if derivB ~= 5
   
  		KB=[KBabs,KBt;KBt',zeros(derivB)]; % Rassemblement de la matrice de krigeage KB
   
	else % La d�rive est sinusoidale
   
		KB=[KBabs,KBt;KBt',zeros(3)]; % Rassemblement de la matrice de krigeage KB
      
   end

else
   
   KB = 1;
   
end


% Calcul des matrices form�es � partir des coordonn�es des points � kriger

% Matrices form�es � partir des coordonn�es des points � kriger pour la partie sup�rieure gauche [IP,JP,nr]
% Les trois autres parties sont des matrices z�ros : la matrice de la partie sup�rieure droite
% de dimension [IP,detB,nr], les deux parties inf�rieures ont respectivement [detA,JP,nr]
% et [detA,detB,nr]
% detA est �gale � derivA si la d�rive est polymomiale et � 3 si la d�rive est sinusoidale
% detB est �gale � derivB si la d�rive est polymomiale et � 3 si la d�rive est sinusoidale

% D�terminer les d�terminants de chaque profil selon le type de la d�rive

if derivA ~= 5
    detA=derivA;
else % La d�rive est sinusoidale
    detA=3;
end

if derivB ~= 5
    detB=derivB;
else % La d�rive est sinusoidale
    detB=3;
end
% Assemblage des matrices de coefficients


coefX=[X,zeros(IP,detB);zeros(detA,JP),zeros(detA,detB)]; % Pour les coordonn�es en x
coefY=[Y,zeros(IP,detB);zeros(detA,JP),zeros(detA,detB)]; % Pour les coordonn�es en y

%-------------------------------------------------------------------------

%-------------------------------------------------------------------------

%-------------------------------------------------------------------------

cont_ini=input;

for inc=1:length(cont_ini)
   
    
    if cont_ini(inc)<=0
        def_ini(inc)=0;
        
    else
    
    if cont_ini(inc)>max(max(Y))
        cont_ini(inc)=max(max(Y));
        
    end
   sigma=cont_ini(inc)/2; % valeur de d�part pour entrer dans la boucle
   DS=0.6;			         % valeur de l'incr�mentation de d�part
  
skrig=0.5; % param�tre de d�part
tkrig=1; % le point cherch� se trouve sur la courbe sup�rieure

eps=1e-6; % critere de pr�cision

while abs((abs(sigma)-abs(cont_ini(inc)))/abs(sigma))>eps
% cette boucle permet de d�terminer la valeur de S   
   
SP=[];
for i=1:IP

    if covA < 4 % Covariances polynomiales
        SP=[SP,abs(skrig-sref(i)).^covA];
    elseif covA == 4 % Covariance logarithmique
        SP=[SP,abs(skrig-sref(i)).^2.*log(abs(skrig-sref(i)))];
    elseif covA == 5 % Covariance sinuso�dale
        SP=[SP,sin(angleRA*abs(skrig-sref(i))*pi/180)];
    end

end

% Ajout et calcul de la partie de la d�rive de la matrice des coefficient param�triques du profil A
% Cette partie de la matrice � les dimensions suivantes : [ns,derivA+1]
% Cette matrice est remplie du vecteur des coordonn�es param�triques le long du profil, 
% positionn� en colonne, o� chaque �l�ment est �lev� de z�ro jusqu'� la puissance de la 
% d�rive du profil.  C'est pourquoi la premi�re colonne de la matrice est compos�e de un.

SP=[SP,ones(1,1)]; % Premi�re colonne de la matrice, seulement des uns

if derivA~=5
    
   for i=1:derivA-1 % Boucle pour remplir de la 2i�me � la derni�re colonne
    	SP=[SP,skrig.^i]; % en variant la puissance d'�l�vation des �l�ment du vecteur colonne
	end
    
else % La d�rive est sinuso�dale
   
   SP=[SP,sin(angleRA*pi/180*skrig),cos(angleRA*pi/180*skrig)];
   
end

% Cr�ation de la matrice param�trique du profil B, TP.
% Calcul de la partie de la covariance de la matrice des coefficients param�triques du profil B
% Cette partie de la matrice � les dimensions suivantes : [JP,nt]

if JP >= 2
   
	TP=[];
	for i=1:JP
   	if covB < 4 % Covariances polynomiales
      	TP=[TP;abs(tkrig-tref(i)).^covB];
   	elseif covB == 4 % Covariance logarithmique
      	TP=[TP;abs(tkrig-tref(i)).^2.*log(abs(tkrig-tref(i)))];
    	elseif covB == 5 % Covariance sinuso�dale
      	TP=[TP;sin(angleRB*abs(tkrig-tref(i))*pi/180)]; 
    	end
   end
    
end

% Ajout et calcul de la partie de la d�rive de la matrice des coefficient param�triques du profil B
% Cette partie de la matrice � les dimensions suivantes : [derivB+1,nt]
% Cette matrice est remplie du vecteur des coordonn�es param�triques le long du profil, 
% positionn� en ligne, o� chaque �l�ment est �lev� de z�ro jusqu'� la puissance de la 
% d�rive du profil.  C'est pourquoi la premi�re ligne de la matrice est compos�e de un.

if JP >= 2

	TP=[TP;ones(1,1)]; % Premi�re ligne de la matrice, seulement des uns

	if derivB ~= 5
    
   	for i=1:derivB-1 % Boucle pour remplir de la 2i�me � la derni�re ligne
    		TP=[TP;tkrig.^i]; % en variant la puissance d'�l�vation des �l�ment du vecteur ligne
    	end
	else % La d�rive est sinuso�dale
   
   	TP=[TP;sin(angleRA*pi/180*tkrig);cos(angleRA*pi/180*tkrig)];
   end
else
   TP = 1;
end


%********************

% Calcul des matrices des �quations param�triques
% En rempla�ant par les matrices calcul�es pr�c�demment
% Cette �quation a �t� expliqu�e plus haut
   

	epsilon=[SP*KA^-1*coefX*KB^-1*TP];
	sigma=[SP*KA^-1*coefY*KB^-1*TP];

if sigma<cont_ini(inc)
   skrig=skrig+DS/2;
else
   skrig=skrig-DS/2;
end

if skrig<0
   skrig=0;
elseif skrig>1
   skrig=1;
end
DS=DS/2; 
end
def_ini(inc)=epsilon;
end
end

% �criture des r�sultats dans un fichier texte
%fid = fopen('D:\Hakim\sflange\flag1.txt','wt');
%fprintf(fid,'%1i\n',def_ini);
%fclose(fid);
ugmi=def_ini;