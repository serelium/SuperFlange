function stress(vc,vf,Ec,Ef,B,A,g1,g0,h,C,tf,Eb,lb,d,n,nt,G_old,FlangeType1,ShellHeadType1,FlangeType2,...
    ShellHeadType2,Pressure,TetaF1,TetaF2,Coef,GasketForce,StrGas,x_N,y_Sg_r,unit,fid);

FlangeType(1)=FlangeType1;
FlangeType(2)=FlangeType2;
ShellHeadType(1)=ShellHeadType1;
ShellHeadType(2)=ShellHeadType2;

BoltForce=Coef(13);

C1=Coef(1);
C2=Coef(2);
C3=Coef(3);
C4=Coef(4);
P0=Coef(5);
M0=Coef(6);
P1=Coef(7);
M1=Coef(8);

RootArea=n*pi*(d-1.3/nt)^2/4;
BoltStress=BoltForce/RootArea;
MaxBoltStress=BoltStress+Eb*d*(TetaF1-TetaF2)/2/lb;




u_ratio='[ % ]';
u_angle='[ � ]';
if unit==1
   u_distance='[ N ]';
   u_force='[ N ]';
   u_stress='[ MPa ]';
   u_area='[ mm^2 ]';
else
   u_distance='[ in ]';
   u_force='[ lb ]';
   u_stress='[ psi ]';
   u_area='[ in^2 ]';
end

fprintf(fid,'\n                            * MAIN PARAMETRS *\n\n');
s=' = ';
s1='                Bolt load ';
s2='             Bolt stress ';
s3='        Max. bolt stress ';
s4='              Gasket load ';
s5='   Average gasket stress ';
s6='      Rotation of flange 1 ';
s7='      Rotation of flange 2 ';

fprintf(fid,'%s%s%s%12.2f\n',s1,u_force,s,BoltForce);
fprintf(fid,'%s%s%s%12.2f\n',s2,u_stress,s,BoltStress);
fprintf(fid,'%s%s%s%12.2f\n',s3,u_stress,s,MaxBoltStress);
fprintf(fid,'%s%s%s%12.2f\n',s4,u_force,s,GasketForce);
fprintf(fid,'%s%s%s%12.2f\n',s5,u_stress,s,StrGas);
fprintf(fid,'%s%s%s%12.3f\n',s6,u_angle,s,TetaF1*180/pi);
fprintf(fid,'%s%s%s%12.3f\n',s7,u_angle,s,TetaF2*180/pi);
fprintf(fid,'\n                               * STRESSES IN GASKET *\n\n');
fprintf(fid,'\n   Distribution of gasket contact stress \n');
fprintf(fid,'   Position on gasket      Gasket stress \n');
fprintf(fid,'            %s               %s \n\n',u_ratio,u_stress);
for i=1:19
    fprintf(fid,'     %12.3f          %12.2f\n',x_N(i),y_Sg_r(i));
end;

flange_number{1}='* FLANGE ONE *';
flange_number{2}='* FLANGE TWO *';

for j=1:2

    fprintf(fid,'\n\n                                   %s\n\n',flange_number{j});

    if ShellHeadType(j)==1
%    {-------------Cylinder Stress--------------}
      Radius(1)=B(j)/2;
      Radius(2)=Radius(1)+g0(j);
      Radius(3)=Radius(1)+g0(j)/2;
      T_Dist=2.45*(Radius(1)*g0(j))^0.5;
      increment=T_Dist/13;                  %{ 1/13th of code influenced distance }
      Betac =((12*(1-vc(j)^2))/(B(j)^2*g0(j)^2))^0.25;
      Distance=0;
      fprintf(fid,'\n                              * STRESSES IN CYLINDER *\n\n');
      fprintf(fid,'    Distance            Tangential stress                    Longitudinal stress\n');
      fprintf(fid,'  from junction     inside            outside             inside           outside\n');
      fprintf(fid,'      %s       %s            %s            %s            %s \n\n',u_distance,u_stress,u_stress,u_stress,u_stress);
%     fprintf(fid,'           1                   1                  1                  1                 1');
      while Distance<T_Dist+increment
          %{----Tangential----}
          [WxCyl,MxCyl]=WMCylinder(Distance,Betac,Ec(j),vc(j),g0(j),g1(j),P0,M0);
          Sigmat_in=Pressure*Radius(3)/g0(j)+Ec(j)*WxCyl/Radius(3)+6*vc(j)*MxCyl/g0(j)^2;
          Sigmat_out=Pressure*Radius(3)/g0(j)+Ec(j)*WxCyl/Radius(3)-6*vc(j)*MxCyl/g0(j)^2;
          %{----Longitudinal----}
          Sigmal_in=Pressure*Radius(3)/2/g0(j)+6*MxCyl/g0(j)^2;
          Sigmal_out=Pressure*Radius(3)/2/g0(j)-6*MxCyl/g0(j)^2;
          fprintf(fid,'%12.3f  %12.2f       %12.2f       %12.2f       %12.2f\n',Distance,Sigmat_in,Sigmat_out,Sigmal_in,Sigmal_out);
          Distance=Distance+increment;
      end;
    end;

%    {---------------Hub Stress-----------------}

    if (FlangeType(j)==1) | (FlangeType(j)==5)
%      Draw_Loadcase;**********************************************************************}
       fprintf(fid,'\n                                 * STRESSES IN HUB *\n\n');
       fprintf(fid,'  Distance from         Tangential stress                    Longitudinal stress\n');
       fprintf(fid,'  hub small end     inside            outside             inside           outside\n');
       fprintf(fid,'      %s       %s            %s            %s            %s \n\n',u_distance,u_stress,u_stress,u_stress,u_stress);
       Distance=0;
       increment=h(j)/13;        %{ 1/13th of hub length }
       alpha=abs(atan((g1(j)-g0(j))/h(j)));             %hub tapper angle
       X1=g0(j)/(tan(alpha));
       while Distance<h(j)+increment
%           {-----Tangential----}
            X=X1+Distance;
            HuThic=X*tan(alpha);
%            Radius(3)=(B(j)+HuThic)/2;
            [WxHub,MxHub]=WMHub(X,Pressure,alpha,B(j),Ef(j),vf(j),g0(j),g1(j),C1,C2,C3,C4);
            Sigmat_in=Pressure*Radius(3)/HuThic+Ef(j)*WxHub/Radius(3)+6*vf(j)*MxHub/HuThic^2;
            Sigmat_out=Pressure*Radius(3)/HuThic+Ef(j)*WxHub/Radius(3)-6*vf(j)*MxHub/HuThic^2;
%           {----Longitudinal----}
            Sigmal_in=Pressure*Radius(3)/2/HuThic+6*MxHub/HuThic^2;
            Sigmal_out=Pressure*Radius(3)/2/HuThic-6*MxHub/HuThic^2;
            fprintf(fid,'%12.3f  %12.2f       %12.2f       %12.2f       %12.2f\n',X-X1,Sigmat_in,Sigmat_out,Sigmal_in,Sigmal_out);
            Distance=Distance+increment;
       end;
    end;

    
%    {---------------Flange stresses----------------}

    if FlangeType(j)==6
%    {---------------Blind flange stresses----------------}
       strg{1}='  ouside diameter ';
       strg{2}='  bolt circle     ';
       strg{3}='  gasket reaction ';
       strg{4}='  plate center    ';
       fprintf(fid,'\n                           * STRESSES IN BLIND COVER PLATE*\n');
       fprintf(fid,'                    Radial stress   Tangential stress    shear stress\n');
       fprintf(fid,'                      %s             %s             %s\n\n',u_stress,u_stress,u_stress);
       rhoPl=[1, C/A(j), G_old/A(j), 0];
       BetaPl=[C/A(j), G_old/A(j)];
       for I=1:4
           if I<2
              C3=log(rhoPl(I));
              C4=1/rhoPl(I)^2-1;
              Mr=-BoltForce/(8*pi())*(BetaPl(1)^2*C4-2*C3);
              Mt=-BoltForce/(8*pi())*(-BetaPl(1)^2*C4-2*C3+2*(1-BetaPl(1)^2));
              qr=-BoltForce/(C*pi())*BetaPl(1)/rhoPl(I);
           else
              Mr=-BoltForce/(8*pi())*(1-BetaPl(1)^2-2*log(BetaPl(1)));
              Mt=Mr;
              qr=0;
           end
           if I<4
              C3=log(rhoPl(I));
              C4=1/rhoPl(I)^2-1;
              Mr=Mr+Pressure*A(j)^2/64*(BetaPl(2)^4*C4-4*BetaPl(2)^2*C3);
              Mr=Mr+GasketForce/(8*pi())*(BetaPl(2)^2*C4-2*C3);
              Mt=Mt+Pressure*A(j)^2/64*(-BetaPl(2)^4*C4-4*BetaPl(2)^2*C3+2*BetaPl(2)^2*(2-BetaPl(2)^2));
              Mt=Mt+GasketForce/(8*pi())*(-BetaPl(2)^2*C4-2*C3+2*(1-BetaPl(2)^2));
              qr=qr+(GasketForce/(G_old*pi())+Pressure*G_old/4)*BetaPl(2)/rhoPl(I);
           else
              C1=1-rhoPl(I)^2;
              K1=(4-BetaPl(2)^2-4*log(BetaPl(2)))*BetaPl(2)^2;
              Mr=Mr+Pressure*A(j)^2/64*(K1-3+3*C1);
              Mr=Mr+GasketForce/(8*pi())*(1-BetaPl(2)^2-2*log(BetaPl(2)));
              Mt=Mt+Pressure*A(j)^2/64*(K1-1+C1);
              Mt=Mt+GasketForce/(8*pi())*(1-BetaPl(2)^2-2*log(BetaPl(2)));
              qr=0;
           end
           Stress_t_plate=6*Mt/tf(j)^2;
           Stress_r_plate=6*Mr/tf(j)^2;
           Stress_shear_plate=qr/tf(j);
           fprintf(fid,'%s  %12.2f       %12.2f       %12.2f\n',strg{I},Stress_r_plate,Stress_t_plate,Stress_shear_plate);
       end
    else
%    {---------------Flange ring stresses----------------}
       fprintf(fid,'\n                              * STRESSES IN FLANGE RING*\n');
       K=A(j)/B(j);
       bolt_hole=d;                                                %bolt hole should be 0.125 in greater
       D0=(A(j)-B(j))/log(K);
       D0=D0-(bolt_hole/(pi*C/n)*(D0-2*d/log((C+d)/(C-d))));
       if K>1.162 
          fprintf(fid,'                                   (PLATE THEORY) \n');
          YA=(3*(1-vf(j))+6*(1+vf(j))*K^2*log(K)/(K^2-1))/pi;      %Plate Theory
          YA=YA/(D0*(1/B(j)-1/A(j)));
       else
          fprintf(fid,'                                   (RING THEORY) \n');
          YA=6/pi/log(K);                                          %Ring Theory
       end;

       fprintf(fid,'\n                             STRESS TYPE AT INNER DIAMETER\n');
       strg{1}='  junction ';
       strg{2}='  gasket   ';
       yy=tf(j)/2;
       fprintf(fid,'   near        Radial stress   Tangential stress    Longitudinal stress  shear stress\n');
       fprintf(fid,'                 %s             %s             %s             %s\n\n',u_stress,u_stress,u_stress,u_stress);
       SI1=0; SI2=0; SI3=0; SI4=0;
       if K>1.162       %{----Plate theory----}
          for I=1:2
              Temp1=-(K^2-1)/((1+vf(j))*K^2/(1-vf(j))+1);
              Temp2=+M1-P1*tf(j)/2;
              Temp3=(C-G_old)*BoltForce/2/pi+(G_old-B(j))*(G_old^2+B(j)^2)*Pressure/16;
              Temp3=pi*Temp3*YA/6/B(j);
              Temp3=-((1+K^2)/(K^2-1))*Temp2-Temp3;
              SigmaR(1)=-(Pressure+P1/tf(j));
              SigmaR(2)=0;   SigmaR(3)=0;
              SigmaR(4)=-12*yy*Temp2/tf(j)^3;
              SigmaT(1)=(1+K^2)*Pressure/pi/G_old/tf(j)/(K^2-1);
              SigmaT(2)=0;   SigmaT(3)=0;
              SigmaT(4)=-12*yy*Temp3/tf(j)^3;
              SigmaT(4)=SigmaT(4)-(1+K^2)/(K^2-1)*P1/tf(j);
              SigmaL(1)=-Pressure;
              SigmaL(2)=0; SigmaL(3)=0; SigmaL(4)=0;
              SigmaR(5)=SigmaR(1)+SigmaR(2)+SigmaR(3)+SigmaR(4);
              SigmaT(5)=SigmaT(1)+SigmaT(2)+SigmaT(3)+SigmaT(4);
              SigmaL(5)=SigmaL(1);
              Thauqr=BoltForce/(G_old*pi()*tf(j));
%              Thauqr=BoltForce/(C*pi())*BetaPl(j)/rhoPl(j)/tf(j);
              if I==2
                 SigmaL(5)=0;
              end;
              fprintf(fid,'%s  %12.2f       %12.2f       %12.2f       %12.2f\n',strg{I},SigmaR(5),SigmaT(5),SigmaL(5),Thauqr);
              yy=-yy;
          end;
       else              %{----Ring theory----}
          Temp1=1-K^2;
          Temp2=1+K^2;
          Temp3=K^2-1;
          for I=1:2
              SigmaR(1)=-(Pressure+P1/tf(j));
              SigmaR(2)=0;
              SigmaR(3)=0;%-P1/tf(j)*Temp1/Temp3;
              SigmaR(5)=SigmaR(1)+SigmaR(2)+SigmaR(3);
              SigmaT(1)=Pressure*Temp2/Temp3;
              SigmaT(2)=(C-G_old)*BoltForce/2/pi+(G_old-B(j))*(G_old^2+B(j)^2)*Pressure/16;
              SigmaT(2)=SigmaT(2)*2*pi*YA*yy/tf(j)^3/B(j);
              SigmaT(3)=-P1/tf(j)*Temp2/Temp3;
              SigmaT(4)=M1*(B(j)+g0(j))-P1*tf(j)*B(j)/2;
              SigmaT(4)=SigmaT(4)*2*pi*YA*yy/tf(j)^3/B(j);

              SigmaT(5)=SigmaT(1)+SigmaT(2)+SigmaT(3)+SigmaT(4);
              SigmaL(1)=Pressure*B(j)^2/((B(j)+2*g1(j))^2-B(j)^2);
              SigmaL(2)=0; SigmaL(3)=0;
              SigmaL(5)=SigmaL(1)+SigmaL(2)+SigmaL(3);
              Thauqr=BoltForce/(G_old*pi()*tf(j));
              yy=-yy;
              if I==2
                 SigmaL(5)=0;
              end;
              fprintf(fid,'%s  %12.2f       %12.2f       %12.2f       %12.2f\n',strg{I},SigmaR(5),SigmaT(5),SigmaL(5),Thauqr);
          end;
       end;

    
    end;
    C1=Coef(25);
    C2=Coef(24);
    C3=Coef(23);
    C4=Coef(22);
    P0=Coef(21);
    M0=Coef(20);
    P1=Coef(19);
    M1=Coef(18);
end;
    
%  {---STRESSES---}



%{----------------------------------------------------------------------------}
function [WxCyl,MxCyl]=WMCylinder(Dist,Beta,E,v,g0,g1,P0,M0);
    Temp1=6*(1-v^2)*exp(-Beta*Dist)/(E*g0^3*Beta^3);
    WxCyl=Temp1*P0*cos(Beta*Dist)+Temp1*M0*Beta*(cos(Beta*Dist)-sin(Beta*Dist));
    Temp1=exp(-Beta*Dist);
    MxCyl=P0*Temp1*sin(Beta*Dist)/Beta+M0*Temp1*(cos(Beta*Dist)+sin(Beta*Dist));

%{----------------------------------------------------------------------------}
function [WxHub,MxHub]=WMHub(x,Pressure,alpha,B,E,v,g0,g1,C1,C2,C3,C4);
    ah=(g0+g1)/4+B/2;                       %hub mean radius
    Rho=(12*(1-v^2)/(alpha*ah)^2)^0.25;
    epsilon=2*Rho*x^.5;
    [ber,dber,bei,dbei,ker,dker,kei,dkei]=kelvin_function(epsilon);
    S5=4*epsilon*bei      +8*dber  -epsilon^2*dbei;
    S6=-4*epsilon*ber     +8*dbei  +epsilon^2*dber;
    S7=4*epsilon*kei      +8*dker  -epsilon^2*dkei;
    S8=-4*epsilon*ker     +8*dkei  +epsilon^2*dker;
    WxHub=(C1*dber+C2*dbei+C3*dker+C4*dkei)*x^-0.5;
    Temp1=E*alpha^3*x^0.5/48/(1-v^2);
    Temp2=Pressure*ah^2*alpha^2*(2-v)/(12*(1-v^2));
    MxHub=Temp1*(C1*S5+C2*S6+C3*S7+C4*S8);
    MxHub=MxHub+Temp2;
%{------------------------}

