function [x_N,Sg_r,ug_r]=stress_distribution(ContWidthGas,Ag,Bg,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,...
         GasketForce,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_Fg);

init_total_Rotation=init_TetaF1-init_TetaF2;
final_total_Rotation=final_TetaF1-final_TetaF2;
%if Seat_flag==0;
%   delta_ug_Fg=(LoaInBol-GasketForce)/kg;
%end
N=9;

dG_in=(G_Fg-Bg)/N;
for I=0:N-1
     G_r(I+1)=Bg+I*dG_in;
     x_N(I+1)=(G_r(I+1)-Bg)/(2*ContWidthGas);    
end    

dG_out=(Ag-G_Fg)/N;
for I=0:N
    G_r(N+I+1)=G_Fg+I*dG_out;
    x_N(N+I+1)=(G_r(N+I+1)-Bg)/(2*ContWidthGas);    
end

if Seat_flag==0;
   ug_ri=ugmi-init_total_Rotation*(G_Sgmi-G_r)/2;
   ug_r=ug_Fg+final_total_Rotation*(G_r-G_Fg)/2;
   [kf,Sg_r]=ui_uf_sf_k(ug_ri,ug_r);
else
%   ug_r=ugmi-init_total_Rotation*(G_Sgmi-G_r)/2;
%   Sg_r=Flag2(ug_r);
   ug_ri=ugmi-init_total_Rotation*(G_Sgmi-G_r)/2;
   ug_r=ug_ri;
   [kf,Sg_r]=ui_uf_sf_k(ug_ri,ug_r);
end
%k_in=0;
%k_out=0;
%for I=0:N-1
%     k_in=k_in+(1+init_total_Rotation*(G_r(I+1)-G_Fg)/ug_Fg)*pi*G_r(I+1)*dG_in*kf(I+1)/2;
%     k_in=k_in+pi*G_r(I+1)*dG_in*kf(I+1)/2;
%end    
%for I=0:N
%     k_out=k_out+(1+init_total_Rotation*(G_r(N+I+1)-G_Fg)/ug_Fg)*pi*G_r(N+I+1)*dG_out*kf(N+I+1)/2;
%     k_out=k_out+pi*G_r(N+I+1)*dG_out*kf(N+I+1)/2;
%end    
%kg=k_in+k_out;