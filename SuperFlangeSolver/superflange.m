function superflange
%clear all;

%Variables intitialisation
format long;
global Dg Sg P1an M1an P2an M2an epsi_r_cr_f1 epsi_r_cr_f2 epsi_t_cr_f1 epsi_t_cr_f2 StifPl;

StifPl=zeros(2,2,3);


%Variables qui doivent �tre transf�r�es  DATA
%************************************************************************************************************
%************************************************************************************************************

%Variable g�n�rales
FlangeType1=1;                   %Type de flange: 1=WN, 2=SlipOn, 3=Lapped, 4=Threaded, 5= Socket, 6=Blind, 0=none
FlangeType2=1; 
ShellHeadType1=1;                %Type de shell-head: 1=Cylinder, 2=plate, 3=Sphere, 4=Conical, 5=eliptical, 0=none
ShellHeadType2=1;


%*********************************************************************************************
%  flange 52 in HE
%*********************************************************************************************

%Variable pour calculer l'interface cylindre/hub 
vc(1)=0.3;                                %Poison of cylindre
Ec(1)=30000000;                           %Young of cylindre at ambiante
Ect(1)=25000000;                          %Young of cylindre at temperature
B(1)=51;                                  %ID of flange
A(1)=58.375;                              %OD of flange
g1(1)=0.823;                              %thickness of hub big end
g0(1)=0.625;                              %thickness of cylinder
h(1)=1.25;                                %hub lenght

%Variable pour calculer l'interface hub/flange
vf(1)=0.3;                                %Poison of flange
Ef(1)=30000000;                           %Young of flange
Eft(1)=25000000;                          %Young of flange at
tf(1)=5.625;                              %thickness of flange

%  flange 2

%Variable pour calculer l'interface cylindre/hub 
vc(2)=0.3;                                %poison of cylindre
Ec(2)=30000000;                           %Young of cylindre at ambiante
Ect(2)=25000000;                          %Young of cylindre at temperature
B(2)=51;                                  %ID of flange
A(2)=58.375;                              %OD of flange
g1(2)=0.823;                              %thickness of hub big end
g0(2)=0.625;                              %thickness of cylinder
h(2)=1.25;                                %hub lenght

%Variable pour calculer l'interface hub/flange
vf(2)=0.3;                                %poison of flange
Ef(2)=30000000;                           %Young of flange at ambiante
Eft(2)=25000000;                          %Young of flange at temperature
tf(2)=5.625;                              %thickness of flange

%  VARIABLE IND�PENDANTES

% Variable pour le gasket
vg=0.4;                                   %Poisson of Gasket
tg=0.063;                                 %Epaisseur du gasket
N=0.5;                                    %Contact Width of gasket
OD=53.125;                                %Gasket OD
ID=52.125;                                %Gasket ID

% Bolt properties       
d=1;                                      %Nominal Diameter of bolt
C=56.25;                                  %Bolt Circular Diameter
n=76;                                     %number of bolt
nt=8;                                     %number of threads per inch
Eb=30000000;                              %Young Modulud of bolts
Ebt=25000000;                             %Young Modulud of bolts at temperature
kw=0;                                     %Belleville_rigidity

%**************************************************************************


  Dg=[ 0	    0	      0	         0	       0	     0	       0
    0.0019	 0.0026	    0.0033	  0.0040	0.0045	  0.0050	0.0054
    0.0055	 0.0075	    0.0082	  0.0088	0.0093	  0.0094	0.0095
    0.007	 0.0084634	0.00953	  0.01039	0.01108	  0.01144	0.0119
    0.0090	 0.0114	    0.0121	  0.0125	0.0128	  0.0134	0.0137
    0.0115	 0.0127	    0.0134	  0.0141	0.01481	  0.01526	0.0156
    0.0131	 0.0147	    0.0152	  0.0157	0.0164	  0.0171	0.0180
    0.01462  0.0165	    0.01687	  0.01724	0.01788	  0.01866	0.0199];
						
  Sg=[0	        0	      0	         0	       0	     0	       0
      0	       180	     290	    500	      700	    900	     1047
      0	       200	     300	    500	     1000	   1500	     2500
      0	       200	     500	   1000	     2000	   3000	     4550
      0	       500	    1000	   2000	     3000	   5000	     8061
      0	       950	    1502	   3200	     6000	  10000	    13360
      0	      1074	    2000	   4000	     8000	  15000	    24732
      0	      1300	    3000	   5360	    10000	  20600	    40000];



%Dg=Dg.*10;

%donn�es Thermique
hos=0.0951;                                 %heat_film_transfer_coefficient_for_flange_outside_surface_
hi=0.476;                                   %heat_film_transfer_coefficient_for_flange_inside_surface
hfb=31.96;                                  %heat_film_transfer_coefficient_from_flange_to_bolt
hcb=9.51;                                   %heat_film_transfer_coefficient_from_flange_to_bolt_head
kf=5.87;                                    %flange_thermal_conductivity
ks=5.87;                                    %shell_thermal_conductivity
alfab=7.777e-6;                             %bolt_thermal_expansion_coefficient
alfag=4.77e-6;                               %gasket_thermal_expansion_coefficient
alfaf(1)=6.94e-6;                           %fange_thermal_expansion_coefficient
alfaf(2)=6.94e-6;                           %fange_thermal_expansion_coefficient
alfac(1)=6.94e-6;                           %shell_thermal_expansion_coefficient
alfac(2)=6.94e-6;                           %shell_thermal_expansion_coefficient

%Gasket creep data
N_stress(1)=2;                              %creep_stress_function_type
a_stress(1)=0;%5.617e-4;                       %creep_stress_constant_1
b_stress(1)=0;%-3.764e-3;                      %creep_stress_constant_2
c_stress(1)=1;                              %creep_stress_constant_3
d_stress(1)=0;                              %creep_stress_constant_4

N_time(1)=2;                                %creep_time_function_type
a_time(1)=0;                                %creep_time_constant_1
b_time(1)=0;                                %creep_time_constant_2
c_time(1)=0.001;%-2.3741;                          %creep_time_constant_3
d_time(1)=0;                                %creep_time_constant_4
creep_member(1)=1;


%Bolt creep data
N_stress(2)=6;                              %creep_time_function_type
a_stress(2)=4.8e-37/24;                     %creep_stress_constant_1
b_stress(2)=6.9;                            %creep_stress_constant_2
c_stress(2)=0;                              %creep_stress_constant_3
d_stress(2)=0;                              %creep_stress_constant_4

N_time(2)=6;                                %creep_time_function_type
a_time(2)=1;                                %creep_stress_constant_1
b_time(2)=1;                                %creep_stress_constant_2
c_time(2)=0;                                %creep_stress_constant_3
d_time(2)=0;                                %creep_stress_constant_4
creep_member(2)=0;

%Flange creep data
n_f(1)=1;                                      %flange_creep_constant_n
A_f(1)=7.5e-17/146^5.5;                        %flange_creep_constant_A
m_f(1)=5.5;                                    %flange_creep_constant_m
n_f(2)=1;                                      %flange_creep_constant_n
A_f(2)=7.5e-17/146^5.5;                        %flange_creep_constant_A
m_f(2)=5.5;                                    %flange_creep_constant_m
creep_member(3)=0;

%cylinder creep data
n_c(1)=n_f(1);                                    %shell_creep_constant_n
A_c(1)=A_f(1);                                    %shell_creep_constant_A
m_c(1)=m_f(1);                                    %shell_creep_constant_m
n_c(2)=n_f(2);                                    %shell_creep_constant_n
A_c(2)=A_f(2);                                    %shell_creep_constant_A
m_c(2)=m_f(2);                                    %shell_creep_constant_m
creep_member(4)=0;

%Variable des conditions initiales
Pressure=0;%270;                          %nominal_pressure
Ext_Moment=0;                               %external_bending_moment
Ext_Force=0;                                %external_axial_force
BoltStress=45000;                           %initial_bolt_stress
ti=797;                                     %internal_operating_temperature
to=77;                                      %external_ambient_temperature


unit=0;                                     %unit�
Seating_load=1;
Operating_load=1;
External_load=0;
Thermal_load=0;
Relaxation_load=0;

%-----------------------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------------------

% MAIN

%-----------------------------------------------------------------------------------------------------
% %----------------------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------------------


lb=0.5*1.125*d+tf(1)+tf(2)+tg;
StrArea=n*pi*(d-0.9743/nt)^2/4;
RootArea=n*pi*(d-1.3/nt)^2/4;
kb=StrArea*Eb/lb;
kw=kw*n;
if kw==0
    kw=inf;
end;
kbw=1/kb+1/kw;
kbw=1/kbw;
LoaInBol=BoltStress*RootArea;             %Load in bolt



AreaGas= pi*(OD^2-ID^2)/4;
d_hole=(d+0.125);
if OD>C                                     
   Area_hole=n*(d+0.125)^2*pi/4;
   Cgi=C-d_hole;
   Cgo=C+d_hole;
   AreaGas=AreaGas-Area_hole;
   G_uniform=((OD^3-ID^3)/6-n*d_hole^2*C/4)/0.25/((OD^2-ID^2)-n*d_hole^2);
   if C>G_uniform
      G_max=C;
      G_min=G_uniform;
   else
      G_max=G_uniform;
      G_min=C;
   end;
else
   G_uniform=2*(OD^2+ID^2+OD*ID)/3/(OD+ID);
   Cgi=C;
   Cgo=C;
end

kg=0;
%Eg=0;

%----------------------------------------------------------------------------------------------------
% Seating 
%----------------------------------------------------------------------------------------------------


P_Eq=0;
ad_creep=0;
Pres_Seat=0;
init_TetaF1=0;
init_TetaF2=0;
G_Sgmi=G_uniform;
G_Fgi=G_uniform;
G_old=G_uniform;
rd_Temp=zeros(2,4);
rot_Temp=zeros(2,5);
ad_Temp=zeros(4);
total_ad_Temp=0;
rd_creep=zeros(2,4);
rot_creep=zeros(2,4);
Precision_min=0.0001;  %N/OD/100;
fid = fopen('outfile.txt','wt');
u_time='[ hr ]';
if unit==1
   u_area='[ mm^2 ]';
   u_stress='[ MPa ]';
else
   u_area='[ in^2 ]';
   u_stress='[ psi ]';
end
if Seating_load==1
disp('  INITIAL TIGHTENING');
fprintf(fid,'\n\n                               **********************\n');
fprintf(fid,'******************************** INITIAL TIGHTENING ***************************\n');
fprintf(fid,'                               **********************\n\n');

s=' = ';
str1='        Total bolt area ';
fprintf(fid,'%s%s%s%12.2f\n',str1,u_area,s,RootArea);
str2='    Gasket contact area ';
fprintf(fid,'%s%s%s%12.2f\n\n',str2,u_area,s,AreaGas);

Precision=1;
Seat_flag=1;
K=1;
%kg_old=kg;
while Precision > Precision_min
    AreaPres=pi*G_old^2/4;                        %Pressurazed Area
    NbFlange=1;
    [m1,Y1]=matrices(vc(1),vf(1),Ec(1),Ef(1),B(1),A(1),g1(1),g0(1),h(1),d,C,n,tf(1),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
            FlangeType1,ShellHeadType1,NbFlange,Pres_Seat,P_Eq,N,LoaInBol,0,rd_Temp,rot_Temp,rd_creep,rot_creep);
    NbFlange=2;
    [m2,Y2]=matrices(vc(2),vf(2),Ec(2),Ef(2),B(2),A(2),g1(2),g0(2),h(2),d,C,n,tf(2),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
            FlangeType2,ShellHeadType2,NbFlange,Pres_Seat,P_Eq,N,LoaInBol,0,rd_Temp,rot_Temp,rd_creep,rot_creep);

    m2=flipud(m2);
    m2=fliplr(m2);
    M=[m1,zeros(13,12);zeros(12,13),zeros(12,12)];
    M([13:25],[13:25])=m2;

    Y2=flipud(Y2);
    Y=[Y1;zeros(12,1)];
    Y([14:25],[1:1])=Y2([2:13]);
    M(13,:)=0;
    Y(13)=LoaInBol;
    M(13,13)=1;
    Coef=M^-1*Y;
    init_TetaF1=Coef(12);        %imprime les angle de rotation 1 voir si on multiblie par 2pi pour avoir en deg
    init_TetaF2=Coef(14);        %imprime les angle de rotation 2
    GasketForce=Coef(13);
    if FlangeType1==6
       init_TetaF1=GasketForce/StifPl(1,2,1)-GasketForce/StifPl(1,2,2);
    end;
    if FlangeType2==6
       init_TetaF2=-(GasketForce/StifPl(2,2,1)-GasketForce/StifPl(2,2,2));
    end;
    StrGas=GasketForce/AreaGas;
    if StrGas < 0
        disp('  Error program stops - no stress on gasket');
        fprintf(fid,'  Error program stops - no stress on gasket');
        Seating_load=0;
        Operating_load=0;
        External_load=0;
        Thermal_load=0;
        Relaxation_load=0;
        break, return
    end;
    ugmi=si_ui_initial(StrGas);
    G_Sgmi=sgmi_position(N,OD,ID,Cgi,Cgo,C,d_hole,LoaInBol,kg,AreaGas,GasketForce,...
           n,init_TetaF1,init_TetaF2,ugmi,G_Sgmi,G_Fgi,G_old);
    [G_new,kg_new]=fg_position(N,OD,ID,Cgi,Cgo,C,d_hole,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
           n,init_TetaF1,init_TetaF2,0,0,ugmi,0,G_Sgmi,G_old,G_Fgi);
    G_Fgi=G_old;

    if OD > C                                     
        if G_new<G_max
           G_max=G_old;
           G_new=0.5*(G_max+G_min);
       else
           G_min=G_old;
           G_new=0.5*(G_max+G_min);
       end;
    end;
    Precision=abs((G_new-G_old)/G_old);
    G_old=abs(G_new);

end;
kg=kg_new;
ug_Fg=ugmi-(init_TetaF1-init_TetaF2)*(G_Sgmi-G_Fgi)/2;
G_old=G_Fgi;
final_TetaF1=init_TetaF1;
final_TetaF2=init_TetaF2;
[x_N,y_Sg_r,y_ug_r]=stress_distribution(N,OD,ID,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,GasketForce,...
    init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old);
stress(vc,vf,Ec,Ef,B,A,g1,g0,h,C,tf,Eb,lb,d,n,nt,G_old,FlangeType1,ShellHeadType1,FlangeType2,ShellHeadType2,...
    Pres_Seat,final_TetaF1,final_TetaF2,Coef,GasketForce,StrGas,x_N,y_Sg_r,unit,fid);
fclose(fid);
end; %Seating
 
%----------------------------------------------------------------------------------------------------
% Operating                        
%----------------------------------------------------------------------------------------------------


P_Eq=0;
Pres_Oper=Pressure;

G_max=OD;
G_min=G_old;
if OD > C                                     
   G_old=0.5*(G_max+G_min);
end;
if Operating_load==1
disp('  PRESSURIZATION');
fid = fopen('outfile.txt','at');
fprintf(fid,'\n\n                                 ******************\n');
fprintf(fid,'********************************** PRESSURIZATION *****************************\n');
fprintf(fid,'                                 ******************\n\n');
Seat_flag=0;
Precision=1;
%kg_old=kg;
while Precision > Precision_min 
%    if it > 4; break; end
    AreaPres=pi*G_old^2/4;                        %Pressurazed Area
    NbFlange=1;  
    [m1,Y1]=matrices(vc(1),vf(1),Ec(1),Ef(1),B(1),A(1),g1(1),g0(1),h(1),d,C,n,tf(1),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
            FlangeType1,ShellHeadType1,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,init_TetaF1,rd_Temp,rot_Temp,rd_creep,rot_creep);

    NbFlange=2;
    [m2,Y2]=matrices(vc(2),vf(2),Ec(2),Ef(2),B(2),A(2),g1(2),g0(2),h(2),d,C,n,tf(2),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
            FlangeType2,ShellHeadType2,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,-init_TetaF2,rd_Temp,rot_Temp,rd_creep,rot_creep);

    Y2=flipud(Y2);
    Y=[Y1];
    Y([14:25],[1:1])=Y2([2:13]);

    Y(13)=Y1(13)+Y2(1)+(1/kbw+1/kg)*LoaInBol+AreaPres*Pressure/kg-total_ad_Temp-ad_creep;  %beaucoup de chose a f�rifier ici
        
    m2=flipud(m2);
    m2=fliplr(m2);
    M=[m1,zeros(13,12);zeros(12,13),zeros(12,12)];
    M([13:25],[13:25])=m2;
    M(13,13)=m1(13,13)+m2(1,1)+1/kg+1/kbw;

    Coef=M^-1*Y;
    final_TetaF1=Coef(12);        %imprime les angle de rotation 1 voir si on multiblie par 2pi pour avoir en deg
    final_TetaF2=Coef(14);        %imprime les angle de rotation 2
    GasketForce=Coef(13)-AreaPres*Pressure;
    StrGas=GasketForce/AreaGas;
    if StrGas < 0
        disp('  Error program stops - no stress on gasket');
        fprintf(fid,'  Error program stops - no stress on gasket');
        Seating_load=0;
        Operating_load=0;
        External_load=0;
        Thermal_load=0;
        Relaxation_load=0;
        break, return
    end;
    if FlangeType1==6
       final_TetaF1=GasketForce/StifPl(1,2,1)-Coef(13)/StifPl(1,2,2)+Pressure/StifPl(1,2,3);
    end;
    if FlangeType2==6
       final_TetaF2=-(GasketForce/StifPl(2,2,1)-Coef(13)/StifPl(2,2,2)+Pressure/StifPl(2,2,3));
    end;
    ug_Fg=ug_force(N,OD,ID,Cgi,Cgo,C,d_hole,tg,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
          n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,G_Sgmi,G_old,G_Fgi);
    [G_new,kg_new]=fg_position        (N,OD,ID,Cgi,Cgo,C,d_hole,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
          n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old,G_Fgi);

    if OD > C                                     
       if G_new<G_old
           G_max=G_old;
           G_new=0.5*(G_max+G_min);
       else
           G_min=G_old;
           G_new=0.5*(G_max+G_min);
       end;
    end;
    Precision=abs((G_new-G_old)/G_old);
    G_old=abs(G_new);


end;
%kg=kg_new;
StrBol=Coef(13)/RootArea;
[x_N,y_Sg_r,y_ug_r]=stress_distribution(N,OD,ID,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,GasketForce,...
                    init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old);
stress(vc,vf,Ec,Ef,B,A,g1,g0,h,C,tf,Eb,lb,d,n,nt,G_old,FlangeType1,ShellHeadType1,FlangeType2,ShellHeadType2,...
    Pres_Oper,final_TetaF1,final_TetaF2,Coef,GasketForce,StrGas,x_N,y_Sg_r,unit,fid);
fclose(fid);
end; %Operating

%----------------------------------------------------------------------------------------------------
% External Loads                        
%----------------------------------------------------------------------------------------------------

if External_load==1
disp('  EXTERNAL LOADING');
fid = fopen('outfile.txt','at');
fprintf(fid,'\n\n                                ********************\n');
fprintf(fid,'********************************* EXTERNAL LOADING ****************************\n');
fprintf(fid,'                                ********************\n\n');
P_Eq=0;
Pres_Oper=Pressure;
Bme=(B(1)+B(2)+g0(1)+g0(2))/2;
AreaEnd=pi*G_old^2/4;                        %End Pressure Area

b_fe=(A(1)+A(2)-B(1)-B(2))/4;
R_m=(A(1)+B(1))/4;
t_fe=(tf(1)+tf(2))/2;
J_f=b_fe*t_fe^3/3*(1-192/(pi())^5*t_fe/b_fe*tanh(pi()*b_fe/(2*t_fe)));
I_f=R_m*t_fe^3*log(A(1)/B(1))/12;
BM_factor=1/(1+J_f/(2*I_f*(1+vf(1))));
%kg_old=kg;
L=n/2+1;
if Ext_Moment==0 
   L=1;
end;
for I=L:L
    P_Eq=BM_factor*16*Ext_Moment*sin(pi/2*(4/n*(I-1)-1))/G_old^3/pi;  %Calculation at bolt position
    P_Eq=P_Eq+Ext_Force/(pi*G_old^2/4);

    Seat_flag=0;
    Precision=1;

    while Precision > Precision_min
        AreaPres=pi*G_old^2/4;                        %Pressurazed Area
        NbFlange=1;
        [m1,Y1]=matrices(vc(1),vf(1),Ec(1),Ef(1),B(1),A(1),g1(1),g0(1),h(1),d,C,n,tf(1),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
                FlangeType1,ShellHeadType1,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,init_TetaF1,rd_Temp,rot_Temp,rd_creep,rot_creep);

        NbFlange=2;
        [m2,Y2]=matrices(vc(2),vf(2),Ec(2),Ef(2),B(2),A(2),g1(2),g0(2),h(2),d,C,n,tf(2),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
                FlangeType2,ShellHeadType2,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,-init_TetaF2,rd_Temp,rot_Temp,rd_creep,rot_creep);

        Y2=flipud(Y2);
        Y=[Y1];
        Y([14:25],[1:1])=Y2([2:13]);

        Y(13)=Y1(13)+Y2(1)+(1/kbw+1/kg)*LoaInBol+(AreaPres*Pressure+AreaEnd*P_Eq)/kg-total_ad_Temp-ad_creep;  %beaucoup de chose a f�rifier ici
        
        m2=flipud(m2);
        m2=fliplr(m2);
        M=[m1,zeros(13,12);zeros(12,13),zeros(12,12)];
        M([13:25],[13:25])=m2;
        M(13,13)=m1(13,13)+m2(1,1)+1/kg+1/kbw;

        Coef=M^-1*Y;
        final_TetaF1=Coef(12);        %imprime les angle de rotation 1 voir si on multiblie par 2pi pour avoir en deg
        final_TetaF2=Coef(14);        %imprime les angle de rotation 2
        GasketForce=Coef(13)-AreaPres*Pressure-AreaEnd*P_Eq;
        StrGas=GasketForce/AreaGas
        if StrGas < 0
            disp('  Error program stops - no stress on gasket');
            fprintf(fid,'  Error program stops - no stress on gasket');
            Seating_load=0;
            Operating_load=0;
            External_load=0;
            Thermal_load=0;
            Relaxation_load=0;
            break, return
        end;
        if FlangeType1==6
           final_TetaF1=GasketForce/StifPl(1,2,1)-Coef(13)/StifPl(1,2,2)+Pressure/StifPl(1,2,3);
        end;
        if FlangeType2==6
           final_TetaF2=-(GasketForce/StifPl(2,2,1)-Coef(13)/StifPl(2,2,2)+Pressure/StifPl(2,2,3));
        end;
        ug_Fg=ug_force(N,OD,ID,Cgi,Cgo,C,d_hole,tg,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
              n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,G_Sgmi,G_old,G_Fgi);
        [G_new,kg_new]=fg_position        (N,OD,ID,Cgi,Cgo,C,d_hole,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
            n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old,G_Fgi);

        if OD > C                                     
           if G_new<G_old
               G_max=G_old;
               G_new=0.5*(G_max+G_min);
           else
               G_min=G_old;
               G_new=0.5*(G_max+G_min);
           end;
        end;
        Precision=abs((G_new-G_old)/G_old);
        G_old=abs(G_new);
        
    end;

    x_t(I)=90*(4/n*(I-1)-1);%(I-1)*180/(n/2);
    y_SgBM(I)=Coef(13);
end;
%kg=kg_new;
StrBol=Coef(13)/RootArea;
[x_N,y_Sg_r,y_ug_r]=stress_distribution(N,OD,ID,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,GasketForce,...
                    init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old);
[x_N,y_Sg_r,y_ug_r]=stress_distribution(N,OD,ID,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,GasketForce,...
                    init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old);
stress(vc,vf,Ec,Ef,B,A,g1,g0,h,C,tf,Eb,lb,d,n,nt,G_old,FlangeType1,ShellHeadType1,FlangeType2,ShellHeadType2,...
    Pres_Oper,final_TetaF1,final_TetaF2,Coef,GasketForce,StrGas,x_N,y_Sg_r,unit,fid);

%fprintf(fid,'%1i\n',x_N,y_Sg_r,y_ug_r,x_t,y_SgBM);
fclose(fid);
end; %External_Loads


%----------------------------------------------------------------------------------------------------
% Thermal                        
%----------------------------------------------------------------------------------------------------

if Thermal_load==1;
kb=StrArea*Ebt/lb;
kbw=1/kb+1/kw;
kbw=1/kbw;
disp('  THERMAL EXPANSION EFFECT');
fid = fopen('outfile.txt','at');
fprintf(fid,'\n\n                            ****************************\n');
fprintf(fid,'***************************** THERMAL EXPANSION EFFECT ************************\n');
fprintf(fid,'                            ****************************\n\n');
P_Eq=0;
Pres_Oper=Pressure;
Seat_flag=0;             
Precision=1;

tol1=1e-3;

[rd_Temp,rot_Temp,ad_Temp]=thermal(hos,hi,kf,ks,alfaf,Eft,Ect,alfac,ti,to,tg,alfag,ID,OD,G_old,vf,vc,A,B,...
    g0,g1,tf,h,d,C,lb,n,alfab,hcb,hfb,tol1,FlangeType1,FlangeType2,ShellHeadType1,ShellHeadType2);
%Total axial displacement
total_ad_Temp=ad_Temp(4)-ad_Temp(3)-ad_Temp(1)-ad_Temp(2);             %+(rot_Temp(1,4)+rot_Temp(2,4))*(C-G_old)/2;
%kg_old=kg;

while Precision > Precision_min
    AreaPres=pi*G_old^2/4;                        %Pressurazed Area
    NbFlange=1;
    [m1,Y1]=matrices(vc(1),vf(1),Ect(1),Eft(1),B(1),A(1),g1(1),g0(1),h(1),d,C,n,tf(1),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
            FlangeType1,ShellHeadType1,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,init_TetaF1,rd_Temp,rot_Temp,rd_creep,rot_creep);

    NbFlange=2;
     [m2,Y2]=matrices(vc(2),vf(2),Ect(2),Eft(2),B(2),A(2),g1(2),g0(2),h(2),d,C,n,tf(2),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
            FlangeType2,ShellHeadType2,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,-init_TetaF2,rd_Temp,rot_Temp,rd_creep,rot_creep);

    Y2=flipud(Y2);
    Y=[Y1];
    Y([14:25],[1:1])=Y2([2:13]);

    Y(13)=Y1(13)+Y2(1)+(1/kbw+1/kg)*LoaInBol+AreaPres*Pressure/kg-total_ad_Temp-ad_creep;  %beaucoup de chose a f�rifier ici
        
    m2=flipud(m2);
    m2=fliplr(m2);
    M=[m1,zeros(13,12);zeros(12,13),zeros(12,12)];
    M([13:25],[13:25])=m2;
    M(13,13)=m1(13,13)+m2(1,1)+1/kg+1/kbw;
%    Y(13)=Y1(13)-(C-G_old)/2*(rot_Temp(1,4)+rot_Temp(2,4));
    
    Coef=M^-1*Y;
    GasketForce=Coef(13)-AreaPres*Pressure;
    StrGas=GasketForce/AreaGas;
    if StrGas < 0
       disp('  Error program stops - no stress on gasket');
       fprintf(fid,'  Error program stops - no stress on gasket');
       Seating_load=0;
       Operating_load=0;
       External_load=0;
       Thermal_load=0;
       Relaxation_load=0;
       break, return
    end;
    final_TetaF1=Coef(12)-rot_Temp(1,4);        %imprime les angle de rotation 1 voir si on multiblie par 2pi pour avoir en deg
    final_TetaF2=Coef(14)+rot_Temp(2,4);        %imprime les angle de rotation 2
    if FlangeType1==6
       final_TetaF1=final_TetaF1+GasketForce/StifPl(1,2,1)-Coef(13)/StifPl(1,2,2)+Pressure/StifPl(1,2,3)-rot_Temp(1,5);
    end;
    if FlangeType2==6
       final_TetaF2=final_TetaF2-(GasketForce/StifPl(2,2,1)-Coef(13)/StifPl(2,2,2)+Pressure/StifPl(2,2,3))+rot_Temp(2,5);
    end;
    ug_Fg=ug_force(N,OD,ID,Cgi,Cgo,C,d_hole,tg,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
          n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,G_Sgmi,G_old,G_Fgi);
    [G_new,kg_new]=fg_position        (N,OD,ID,Cgi,Cgo,C,d_hole,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
          n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old,G_Fgi);
    
    if OD > C                                     
        if G_new<G_max
           G_max=G_old;
           G_new=0.5*(G_max+G_min);
       else
           G_min=G_old;
           G_new=0.5*(G_max+G_min);
       end;
    end;
    Precision=abs((G_new-G_old)/G_old);
    G_old=abs(G_new);

end;
%kg=kg_new;
StrBol=Coef(13)/RootArea;
[x_N,y_Sg_r,y_ug_r]=stress_distribution(N,OD,ID,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,GasketForce,...
                    init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old);
final_TetaF1=Coef(12);
final_TetaF2=Coef(14);
stress(vc,vf,Ect,Eft,B,A,g1,g0,h,C,tf,Ebt,lb,d,n,nt,G_old,FlangeType1,ShellHeadType1,FlangeType2,ShellHeadType2,...
    Pres_Oper,final_TetaF1,final_TetaF2,Coef,GasketForce,StrGas,x_N,y_Sg_r,unit,fid);
fclose(fid);
end; %Thermal


%----------------------------------------------------------------------------------------------------
% Relaxation                        
%----------------------------------------------------------------------------------------------------

if Relaxation_load==1
disp('  CREEP RELAXATION');
fid = fopen('outfile.txt','at');
fprintf(fid,'\n\n                                ********************\n');
fprintf(fid,'********************************* CREEP RELAXATION ****************************\n');
fprintf(fid,'                                ********************\n\n');
P_Eq=0;
Pres_Oper=Pressure;
Seat_flag=0;
Precision=1;

ad_creep=0;
ad_creep_gas=0;
ad_creep_bol=0;
ad_creep_fl=0;


t_init=500;                     %start creep after 500 sec.
t_tot=20000;                   %finish creep after after 18000 sec.
t_inc=1;

t_count=t_init;
t_eqGas=t_init+t_inc; 
t_eqBol=t_eqGas;
if t_eqGas>=t_tot
   t_eqGas=t_init;
   t_eqBol=t_init;
end;

StrBol=Coef(13)/RootArea;
%[Fs,Gt]=creep_function(N_stress,N_time,a_stress,b_stress,c_stress,d_stress,a_time,b_time,c_time,d_time);
t=t_init;
StrBolOld=StrBol;
%s=StrGas;
%Ecreep_initGas=eval(Fs(1)*Gt(1));
%s=StrBol;
%Ecreep_initBol=eval(Fs(2)*Gt(2));
if creep_member(1)==1 | creep_member(2)==1
    [Ecreep_or_Time]=creep_function(1,0,0,0,0,t_init,t_init,StrGas,StrBol,N_stress,N_time,a_stress,b_stress,c_stress,d_stress,a_time,b_time,c_time,d_time);
    Ecreep_initGas=Ecreep_or_Time(1);
    Ecreep_initBol=Ecreep_or_Time(2);
end;
jj=1;
x_time(jj)=0;
S_g(jj)=StrGas;
S_b(jj)=StrBol;
P1an=Coef(5);
M1an=Coef(6);
P2an=Coef(21);
M2an=Coef(20);
epsi_t_cr_f1=zeros(21,21);
epsi_t_cr_f2=zeros(21,21);
epsi_r_cr_f1=zeros(21,21);
epsi_r_cr_f2=zeros(21,21);
elastic_TetaF1=final_TetaF1;
elastic_TetaF2=final_TetaF2;
%fid = fopen('stressdisplacementcf.txt','wt');
%kg_old=kg;
while (t_count<t_tot) & (StrGas>Pres_Oper)
    jj=jj+1;    
    t_count=t_count+t_inc;
%   Flange creep
%    [d_rot_creep_f,d_rd_creep_f]=flange_creep(A_f,m_f,n_f,final_TetaF1,final_TetaF2,Ef,A,B,tf,Pressure,t_count,t_inc,Coef(7),Coef(19),fid);
    if creep_member(3)==1
        [d_rot_creep_f,d_rd_creep_f]=ring_creep(A_f,m_f,n_f,final_TetaF1,final_TetaF2,Ef,vf,A,B,tf,Pressure,t_count,t_inc,Coef(7),Coef(19),fid);
        rot_creep(1,4)=rot_creep(1,4)+d_rot_creep_f(1);
        rot_creep(2,4)=rot_creep(2,4)+d_rot_creep_f(2);
        ad_creep_fl=(rot_creep(1,4)+rot_creep(2,4))*(C-G_old)/2;
        rd_creep(1,4)=rd_creep(1,4)+d_rd_creep_f(1);
        rd_creep(2,4)=rd_creep(2,4)+d_rd_creep_f(2);
    end;

%   Cylinder creep
%    [d_rot_creep_c,d_rd_creep_c]=cylinder_creep(A_c,m_c,n_c,Ect,vc,B,g0,Pressure,t_count,t_inc,Coef(5),Coef(6),Coef(20),Coef(21),fid);
%    rot_creep(1,1)=rot_creep(1,1)+d_rot_creep_c(1);
%    rot_creep(2,1)=rot_creep(2,1)+d_rot_creep_c(2);
%    rd_creep(1,1)=rd_creep(1,1)+d_rd_creep_c(1);
%    rd_creep(2,1)=rd_creep(2,1)+d_rd_creep_c(2);
    
    
%    t=t_eqGas;
%    t=t_eqBol;
%    s=StrGas
%    ad_creep_gas=eval(Fs(1)*Gt(1))-Ecreep_initGas;
%    t=t_eqBol;
%    s=StrBol
%    ad_creep_bol=eval(Fs(2)*Gt(2))-Ecreep_initBol;
    if creep_member(1)==1 | creep_member(2)==1
        [Ecreep_or_Time]=creep_function(1,Ecreep_initGas,Ecreep_initBol,ad_creep_gas,ad_creep_bol,t_eqGas,t_eqBol,StrGas,StrBol,N_stress,N_time,a_stress,b_stress,c_stress,d_stress,a_time,b_time,c_time,d_time);
        ad_creep_gas=Ecreep_or_Time(1)-Ecreep_initGas;
        ad_creep_bol=Ecreep_or_Time(2)-Ecreep_initBol;
    end;

    ad_creep=ad_creep_gas+ad_creep_bol*lb+ad_creep_fl;
%    ad_creep=double(ad_creep);
    Precision=1;
    while Precision > Precision_min
        AreaPres=pi*G_old^2/4;                        %Pressurazed Area
        NbFlange=1;
        [m1,Y1]=matrices(vc(1),vf(1),Ect(1),Eft(1),B(1),A(1),g1(1),g0(1),h(1),d,C,n,tf(1),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
                FlangeType1,ShellHeadType1,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,init_TetaF1,rd_Temp,rot_Temp,rd_creep,rot_creep);

        NbFlange=2;
        [m2,Y2]=matrices(vc(2),vf(2),Ect(2),Eft(2),B(2),A(2),g1(2),g0(2),h(2),d,C,n,tf(2),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
                FlangeType2,ShellHeadType2,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,-init_TetaF2,rd_Temp,rot_Temp,rd_creep,rot_creep);

        Y2=flipud(Y2);
        Y=[Y1];
        Y([14:25],[1:1])=Y2([2:13]);

        Y(13)=Y1(13)+Y2(1)+(1/kbw+1/kg)*LoaInBol+AreaPres*Pressure/kg-total_ad_Temp-ad_creep;  %beaucoup de chose a f�rifier ici
        
        m2=flipud(m2);
        m2=fliplr(m2);
        M=[m1,zeros(13,12);zeros(12,13),zeros(12,12)];
        M([13:25],[13:25])=m2;
        M(13,13)=m1(13,13)+m2(1,1)+1/kg+1/kbw;

        Coef=M^-1*Y;
        elastic_TetaF1=Coef(12);        %imprime les angle de rotation 1 voir si on multiblie par 2pi pour avoir en deg
        elastic_TetaF2=Coef(14);        %imprime les angle de rotation 2
        final_TetaF1=elastic_TetaF1+rot_creep(1,4);
        final_TetaF2=elastic_TetaF2-rot_creep(2,4);
        GasketForce=Coef(13)-AreaPres*Pressure;
        StrGas=GasketForce/AreaGas;
        if StrGas < 0
           disp('  Error program stops - no stress on gasket');
           fprintf(fid,'  Error program stops - no stress on gasket');
           Seating_load=0;
           Operating_load=0;
           External_load=0;
           Thermal_load=0;
           Relaxation_load=0;
           break, return
        end;
        StrBol=Coef(13)/RootArea;
        final_TetaF1=Coef(12)-rot_Temp(1,4);        %ajouter 
        final_TetaF2=Coef(14)+rot_Temp(2,4);        %ajouter
        if FlangeType1==6
           final_TetaF1=GasketForce/StifPl(1,2,1)-Coef(13)/StifPl(1,2,2)+Pressure/StifPl(1,2,3);
        end;
        if FlangeType2==6
           final_TetaF2=-(GasketForce/StifPl(2,2,1)-Coef(13)/StifPl(2,2,2)+Pressure/StifPl(2,2,3));
        end;
        ug_Fg=ug_force(N,OD,ID,Cgi,Cgo,C,d_hole,tg,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
              n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,G_Sgmi,G_old,G_Fgi);
        [G_new,kg_new]=fg_position        (N,OD,ID,Cgi,Cgo,C,d_hole,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
              n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old,G_Fgi);
    

        if OD > C                                     
           if G_new<G_old
              G_max=G_old;
              G_new=0.5*(G_max+G_min);
           else
              G_min=G_old;
              G_new=0.5*(G_max+G_min);
           end;
        end;
        Precision=abs((G_new-G_old)/G_old);
        G_old=abs(G_new);
          

    end;

    if t_inc==0 
       t_count=t_tot+1;     %{Protection against t_inc=0}
    end;
%    s=StrGas;
%    t_eqGas=solve(eval(Fs(1))*Gt(1)-ad_creep_gas);
%    s=StrBol;
%    t_eqBol=solve(eval(Fs(2))*Gt(2)-ad_creep_bol);
%    t_eqBol=double(t_eqBol);
    if creep_member(1)==1 | creep_member(2)==1
        [Ecreep_or_Time]=creep_function(2,Ecreep_initGas,Ecreep_initBol,ad_creep_gas,ad_creep_bol,t_eqGas,t_eqBol,StrGas,StrBol,N_stress,N_time,a_stress,b_stress,c_stress,d_stress,a_time,b_time,c_time,d_time);
        t_eqGas=Ecreep_or_Time(1);
        t_eqBol=Ecreep_or_Time(2);
    end;

    x_time(jj)=t_count;
    S_g(jj)=StrGas;
    S_b(jj)=StrBol;

    if t_count>501 
      t_inc=200;
    end;
    if t_count>2001 
      t_inc=500;
    end;
    if t_count>10001 
      t_inc=1000;
    end;
    if creep_member(1)==1 | creep_member(2)==1
        t_eqGas=t_eqGas+t_inc;
        t_eqBol=t_eqBol+t_inc;
    end;
    
end;    
%kg=kg_new;
[x_N,y_Sg_r,y_ug_r]=stress_distribution(N,OD,ID,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,GasketForce,...
               init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old);

stress(vc,vf,Ect,Eft,B,A,g1,g0,h,C,tf,Ebt,lb,d,n,nt,G_old,FlangeType1,ShellHeadType1,FlangeType2,ShellHeadType2,...
    Pres_Oper,final_TetaF1,final_TetaF2,Coef,GasketForce,StrGas,x_N,y_Sg_r,unit,fid);
fprintf(fid,'\n                          * STRESS RELAXATION *\n\n');
fprintf(fid,'            time             Bolt stress          Gasket stress\n');
fprintf(fid,'           %s               %s               %s \n\n',u_time,u_stress,u_stress);
for i=1:jj
    fprintf(fid,'     %12.2f          %12.2f          %12.2f\n',x_time(i),S_b(i),S_g(i));
end;

%fid = fopen('stressdisplacementc.txt','wt');
%fprintf(fid,'%1i\n',x_N,y_Sg_r,y_ug_r,x_time,S_b);
fclose(fid);
end; %Relaxation
