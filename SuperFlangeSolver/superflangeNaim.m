% mcc -W lib:superflangeDLL -T link:lib D:\ETS\BBS\DLG\Solver\SF++20-01-2006\superflange.m matrices.m kelvin_function.m sgmi_position.m fg_position.m stress_distribution.m stress.m si_ui_initial.m ug_force.m Thermal.m creep_function.m ui_uf_sf_k.m a_newton.m a_newton1.m modbessel_function.m ring_creep.m -v

function aaaa = superflangeNaim(FlgType1,...  
    flange_outside_diameter1, height_raised_face1, flange_thickness1, raised_face_diameter1, bolt_circle_diameter1,...
    number_of_bolts1, flange_inside_diameter1, welding_neck_hub_thickness1, shell_thickness1, hub_length1,...
    flange_Young_modulus_amb1, flange_young_modulus_temp1, flange_Poisson_ratio1,...
    flange_allowable_stress_amb1, flange_allowable_stress_temp1, flange_thermal_expansion_coef1, flange_thermal_conductivity1,...
    heat_film_trans_coef_for_flange_out_surface1, heat_film_trans_coef_for_flange_in_surface1, ...
    flange_creep_constant_A1, flange_creep_constant_m1, flange_creep_constant_n1,...
    ShHeadType1, shell_Poisson_ratio1, shell_Young_modulus_amb1, shell_Young_modulus_temp1, shell_creep_constant_n1, shell_creep_constant_A1,...
    shell_creep_constant_m1, shell_thermal_expansion_coef1, shell_thermal_conductivity1,...
    FlgType2,...
    flange_outside_diameter2, height_raised_face2, flange_thickness2, raised_face_diameter2, bolt_circle_diameter2,...
    number_of_bolts2, flange_inside_diameter2, welding_neck_hub_thickness2, shell_thickness2, hub_length2,... 
    flange_Young_modulus_amb2, flange_young_modulus_temp2, flange_Poisson_ratio2, ...
    flange_allowable_stress_amb2, flange_allowable_stress_temp2, flange_thermal_expansion_coef2, flange_thermal_conductivity2,...
    heat_film_trans_coef_for_flange_out_surface2, heat_film_trans_coef_for_flange_in_surface2, ...
    flange_creep_constant_A2, flange_creep_constant_m2, flange_creep_constant_n2,...
    ShHeadType2, shell_Poisson_ratio2, shell_Young_modulus_amb2, shell_Young_modulus_temp2, shell_creep_constant_n2, shell_creep_constant_A2,...
    shell_creep_constant_m2, shell_thermal_expansion_coef2, shell_thermal_conductivity2,... 
    number_of_thread_per_inch, wachers_rigidity, nut_factor, bolt_Young_modulus_amb, bolt_Young_modulus_temp,...
    poisson_ratio, allowable_stress_amb, allowable_stress_temp, bolt_thermal_expansion_coef,...
    heat_film_trans_coef_from_flange_to_bolt, heat_film_trans_coef_from_flange_to_bolt_head,...
    bolt_creep_constant_A_b, bolt_creep_constant_m_b, bolt_creep_constant_n_b, ...
    Bolt_creep_stress_function_type, Bolt_creep_stress_constant_1, ...
    Bolt_creep_stress_constant_2,Bolt_creep_stress_constant_3, Bolt_creep_stress_constant_4, ...
    Bolt_creep_time_function_type, Bolt_creep_time_constant_1,  Bolt_creep_time_constant_2, ...
    Bolt_creep_time_constant_3, Bolt_creep_time_constant_4, ...
    gasket_inside_diameter, gasket_outside_diameter, ring_outside_diameter, centring_ring_diameter, gasket_thickness, ...
    Contact_Width_Gasket, gasket_thermal_expansion_coef, intercept_part_A, slope_gb_line, intercept_part_B,...
    gasket_factor, seating_stress, max_tightness, min_operating_stress, stress_creep_function, time_creep_function, ...                    
    DisplMatrix, StressMatrix, bolt_nominal_diameter, ...
    Gasket_creep_stress_function_type, Gasket_creep_stress_constant_1, Gasket_creep_stress_constant_2, ...
    Gasket_creep_stress_constant_3, Gasket_creep_stress_constant_4, ...
    Gasket_creep_time_function_type, Gasket_creep_time_constant_1, Gasket_creep_time_constant_2, ...
    Gasket_creep_time_constant_3, Gasket_creep_time_constant_4, ...                   
    nominal_pressure, internal_operating_temperature, test_pressure_ratio, initial_bolt_load, external_axial_force,...
    external_bending_moment, external_ambient_temperature, Total_creep_time, initial_bolt_stress, initial_gasket_stress, ...
    applied_bolt_torque, tightness_class, assembly_efficiency, ratio_tpa_tpmin, ...
    strUnit, ...
    Seating_load_check, ...
    Operating_load_check, ...
    External_load_check, ...
    Thermal_load_check, ...
    Relaxation_load_check) % 137 parametres

format long;
global Dg Sg P1an M1an P2an M2an epsi_r_cr_f1 epsi_r_cr_f2 epsi_t_cr_f1 epsi_t_cr_f2 StifPl;

StifPl=zeros(2,2,3);


%Variables qui doivent �tre transf�r�es  DATA
%************************************************************************************************************
%************************************************************************************************************

%{
%Variable g�n�rales
FlangeType1=1;                   %Type de flange: 1=WN, 2=SlipOn, 3=Lapped, 4=Threaded, 5= Socket, 6=Blind, 0=none
FlangeType2=1; 
ShellHeadType1=1;                %Type de shell-head: 1=Cylinder, 2=plate, 3=Sphere, 4=Conical, 5=eliptical, 0=none
ShellHeadType2=1;
%}
FlangeType1=FlgType1;                   
FlangeType2=FlgType2; 
ShellHeadType1=ShHeadType1;                
ShellHeadType2=ShHeadType2;

%*********************************************************************************************
%  flange 52 in HE
%*********************************************************************************************
%{
%Variable pour calculer l'interface cylindre/hub 
vc(1)=0.3;                                %Poison of cylindre
Ec(1)=30000000;                           %Young of cylindre at ambiante
Ect(1)=30000000;                          %Young of cylindre at temperature
B(1)=51;                                  %ID of flange
A(1)=58.375;                              %OD of flange
g1(1)=0.823;                              %thickness of hub big end
g0(1)=0.625;                              %thickness of cylinder
h(1)=1.25;                                %hub lenght
%}
vc(1)=shell_Poisson_ratio1;                                
Ec(1)=shell_Young_modulus_amb1;  
Ect(1)=shell_Young_modulus_temp1; 
B(1)=flange_inside_diameter1;                              
A(1)=flange_outside_diameter1;                               
g1(1)=welding_neck_hub_thickness1;                             
g0(1)=shell_thickness1;                             
h(1)=hub_length1;

%{
%Variable pour calculer l'interface hub/flange
vf(1)=0.3;                                %Poison of flange
Ef(1)=30000000;                           %Young of flange
Eft(1)=30000000;                          %Young of flange at
tf(1)=5.625;                              %thickness of flange
%}

vf(1)=flange_Poisson_ratio1;                               
Ef(1)=flange_Young_modulus_amb1;   
Eft(1)=flange_young_modulus_temp1;
tf(1)=flange_thickness1;   

Sfa(1)= flange_allowable_stress_amb1; % Naim
Sfo(1)= flange_allowable_stress_temp1; % Naim
f(1)=height_raised_face1; % Naim
R(1)=raised_face_diameter1; % Naim

%  flange 2

%{
%Variable pour calculer l'interface cylindre/hub 
vc(2)=0.3;                                %poison of cylindre
Ec(2)=30000000;                           %Young of cylindre at ambiante
Ect(2)=30000000;                          %Young of cylindre at temperature
B(2)=51;                                  %ID of flange
A(2)=58.375;                              %OD of flange
g1(2)=0.823;                              %thickness of hub big end
g0(2)=0.625;                              %thickness of cylinder
h(2)=1.25;                                %hub lenght
%}
vc(2)=shell_Poisson_ratio2;                                
Ec(2)=shell_Young_modulus_amb2; 
Ect(2)=shell_Young_modulus_temp2; 
B(2)=flange_inside_diameter2;                              
A(2)=flange_outside_diameter2;                                
g1(2)=welding_neck_hub_thickness2;                              
g0(2)=shell_thickness2;                              
h(2)=hub_length2;  

%{
%Variable pour calculer l'interface hub/flange
vf(2)=0.3;                                %poison of flange
Ef(2)=30000000;                           %Young of flange at ambiante
Eft(2)=30000000;                          %Young of flange at temperature
tf(2)=5.625;                              %thickness of flange
%}
vf(2)=flange_Poisson_ratio2;                               
Ef(2)=flange_Young_modulus_amb2;  
Eft(2)=flange_young_modulus_temp2;
tf(2)=flange_thickness2;

Sfa(2)= flange_allowable_stress_amb2;   % Naim
Sfo(2)= flange_allowable_stress_temp2;  % Naim
f(2)=height_raised_face2;               % Naim
R(2)= raised_face_diameter2;            % Naim

%  VARIABLE IND�PENDANTES
%{
% Variable pour le gasket
vg=0.4;                                   %Poisson of Gasket
tg=0.063;                                 %Epaisseur du gasket
N=0.5;                                    %Contact Width of gasket
OD=53.125;                                %Gasket OD
ID=52.125;                                %Gasket ID
%}
vg=0.4;
tg=gasket_thickness;                               
N=Contact_Width_Gasket;                                  
OD=gasket_outside_diameter;                                   
ID=gasket_inside_diameter;
IRID = ring_outside_diameter; % Naim
CROD= centring_ring_diameter; % Naim

%{
% Bolt properties       
d=1;                                      %Nominal Diameter of bolt
C=56.25;                                  %Bolt Circular Diameter
n=76;                                     %number of bolt
nt=8;                                     %number of threads per inch
Eb=30000000;                              %Young Modulud of bolts
Ebt=30000000;                             %Young Modulud of bolts at temperature
kw=0;                                     %Belleville_rigidity
%}
d=bolt_nominal_diameter;                                    
C=bolt_circle_diameter1;                                
n=number_of_bolts1;                                  
nt=number_of_thread_per_inch;                                   
Eb=bolt_Young_modulus_amb;
Ebt=bolt_Young_modulus_temp;
kw= wachers_rigidity;

Ub=poisson_ratio;           % Naim
Sa=allowable_stress_amb;    % Naim
So=allowable_stress_temp;   % Naim
C2=bolt_circle_diameter2;   % Naim
n2=number_of_bolts2;        % Naim

%**************************************************************************

% LAMONS CMS t=0.080 in
%{  
  Dg=[ 0	    0	      0	         0	       0	     0	       0
    0.0019	 0.0026	    0.0033	  0.0040	0.0045	  0.0050	0.0054
    0.0055	 0.0075	    0.0082	  0.0088	0.0093	  0.0094	0.0095
    0.007	 0.0084634	0.00953	  0.01039	0.01108	  0.01144	0.0119
    0.0090	 0.0114	    0.0121	  0.0125	0.0128	  0.0134	0.0137
    0.0115	 0.0127	    0.0134	  0.0141	0.01481	  0.01526	0.0156
    0.0131	 0.0147	    0.0152	  0.0157	0.0164	  0.0171	0.0180
    0.01462  0.0165	    0.01687	  0.01724	0.01788	  0.01866	0.0199];
						
  Sg=[0	        0	      0	         0	       0	     0	       0
      0	       180	     290	    500	      700	    900	     1047
      0	       200	     300	    500	     1000	   1500	     2500
      0	       200	     500	   1000	     2000	   3000	     4550
      0	       500	    1000	   2000	     3000	   5000	     8061
      0	       950	    1502	   3200	     6000	  10000	    13360
      0	      1074	    2000	   4000	     8000	  15000	    24732
      0	      1300	    3000	   5360	    10000	  20600	    40000];
%}
%Dg=Dg.*10;
Dg = DisplMatrix;
Sg = StressMatrix;

%{
%donn�es Thermique
hos=0.0951;                                 %heat_film_transfer_coefficient_for_flange_outside_surface_
hi=0.476;                                   %heat_film_transfer_coefficient_for_flange_inside_surface
hfb=31.96;                                  %heat_film_transfer_coefficient_from_flange_to_bolt
hcb=9.51;                                   %heat_film_transfer_coefficient_from_flange_to_bolt_head
kf=5.87;                                    %flange_thermal_conductivity
ks=5.87;                                    %shell_thermal_conductivity
alfab=7.777e-6;                             %bolt_thermal_expansion_coefficient
alfag=4.77e-6;                               %gasket_thermal_expansion_coefficient
alfaf(1)=6.94e-6;                           %fange_thermal_expansion_coefficient
alfaf(2)=6.94e-6;                           %fange_thermal_expansion_coefficient
alfac(1)=6.94e-6;                           %shell_thermal_expansion_coefficient
alfac(2)=6.94e-6;                           %shell_thermal_expansion_coefficient
%}
hos=heat_film_trans_coef_for_flange_out_surface1;                                 
hi=heat_film_trans_coef_for_flange_in_surface1;  
hfb=heat_film_trans_coef_from_flange_to_bolt;                                  
hcb=heat_film_trans_coef_from_flange_to_bolt_head;                                   
kf=flange_thermal_conductivity1;                                    
ks=shell_thermal_conductivity1;                                    
alfab=bolt_thermal_expansion_coef;                             
alfag=gasket_thermal_expansion_coef;                               
alfaf(1)=flange_thermal_expansion_coef1;                           
alfaf(2)=flange_thermal_expansion_coef2;                           
alfac(1)=shell_thermal_expansion_coef1;                           
alfac(2)=shell_thermal_expansion_coef2;

kf2=flange_thermal_conductivity2;      % Naim                              
ks2=shell_thermal_conductivity2;       % Naim
hos2=heat_film_trans_coef_for_flange_out_surface2;   % Naim                               
hi1=heat_film_trans_coef_for_flange_in_surface2;     % Naim   

A_b=bolt_creep_constant_A_b; % Naim  
m_b=bolt_creep_constant_m_b; % Naim       
n_b=bolt_creep_constant_n_b; % Naim 

Gb=intercept_part_A;        % Naim
a=slope_gb_line;            % Naim
Gs=intercept_part_B;        % Naim
m=gasket_factor;            % Naim
y=seating_stress;           % Naim
Tpmax=max_tightness;        % Naim
SL=min_operating_stress;    % Naim
N_S=stress_creep_function;  % Naim
N_t=time_creep_function;    % Naim

%{
%Gasket creep data
N_stress(1)=1;                              %creep_stress_function_type
a_stress(1)=5.617e-4;                       %creep_stress_constant_1
b_stress(1)=-3.764e-3;                      %creep_stress_constant_2
c_stress(1)=0;                              %creep_stress_constant_3
d_stress(1)=0;                              %creep_stress_constant_4

N_time(1)=4;                                %creep_time_function_type
a_time(1)=1;                                %creep_time_constant_1
b_time(1)=0;                                %creep_time_constant_2
c_time(1)=0;%-2.3741;                          %creep_time_constant_3
d_time(1)=0;                                %creep_time_constant_4
%}
N_stress(1)=Gasket_creep_stress_function_type;                          
a_stress(1)=Gasket_creep_stress_constant_1;                   
b_stress(1)=Gasket_creep_stress_constant_2;                 
c_stress(1)=Gasket_creep_stress_constant_3;                          
d_stress(1)=Gasket_creep_stress_constant_4;        

N_time(1)=Gasket_creep_time_function_type;                            
a_time(1)=Gasket_creep_time_constant_1;                            
b_time(1)=Gasket_creep_time_constant_2;                           
c_time(1)=Gasket_creep_time_constant_3;                   
d_time(1)=Gasket_creep_time_constant_4; 

creep_member(1)=0; % � voir avec Hakim

%{
%Bolt creep data
N_stress(2)=6;                              %creep_time_function_type
a_stress(2)=4.8e-37/24;                     %creep_stress_constant_1
b_stress(2)=6.9;                            %creep_stress_constant_2
c_stress(2)=0;                              %creep_stress_constant_3
d_stress(2)=0;                              %creep_stress_constant_4

N_time(2)=6;                                %creep_time_function_type
a_time(2)=1;                                %creep_stress_constant_1
b_time(2)=1;                                %creep_stress_constant_2
c_time(2)=0;                                %creep_stress_constant_3
d_time(2)=0;                                %creep_stress_constant_4
%}
N_stress(2)=Bolt_creep_stress_function_type;                              
a_stress(2)=Bolt_creep_stress_constant_1;                     
b_stress(2)=Bolt_creep_stress_constant_2;                            
c_stress(2)=Bolt_creep_stress_constant_3;                              
d_stress(2)=Bolt_creep_stress_constant_4;                              
N_time(2)=Bolt_creep_time_function_type;                                
a_time(2)=Bolt_creep_time_constant_1;                
b_time(2)=Bolt_creep_time_constant_2;                 
c_time(2)=Bolt_creep_time_constant_3;                                
d_time(2)=Bolt_creep_time_constant_4; 

creep_member(2)=1;
%{
%Flange creep data
n_f=1;                                      %flange_creep_constant_n
A_f=7.5e-17/146^5.5;                        %flange_creep_constant_A
m_f=5.5;                                    %flange_creep_constant_m
%}
n_f=flange_creep_constant_n1;
A_f=flange_creep_constant_A1;        
m_f=flange_creep_constant_m1;

n_f2=flange_creep_constant_n2;  % Naim
A_f2=flange_creep_constant_A2;  % Naim      
m_f2=flange_creep_constant_m2;  % Naim

creep_member(3)=0;
%{
%cylinder creep data
n_c=n_f;                                    %shell_creep_constant_n
A_c=A_f;                                    %shell_creep_constant_A
m_c=m_f;                                    %shell_creep_constant_m
%}
n_c=n_f;
A_c=A_f;  
m_c=m_f;
n_c2=shell_creep_constant_n2; % Naim
A_c2=shell_creep_constant_A2; % Naim 
m_c2=shell_creep_constant_m2; % Naim

creep_member(4)=0;
%{
%Variable des conditions initiales
Pressure=400;%270;                          %nominal_pressure
Ext_Moment=0;                               %external_bending_moment
Ext_Force=0;                                %external_axial_force
BoltStress=45000;                           %initial_bolt_stress
ti=797;                                     %internal_operating_temperature
to=77;                                      %external_ambient_temperature
%}
Pressure=nominal_pressure;    % P                      
Ext_Moment=external_bending_moment;  % Me            
Ext_Force=external_axial_force;    % Fa                  
BoltStress=initial_bolt_stress;    % Si            
ti=internal_operating_temperature;                                
to=external_ambient_temperature;                                
t_tot=Total_creep_time; 

r=test_pressure_ratio;  % Naim
Fb=initial_bolt_load;   % Naim
GasketStress= initial_gasket_stress; % Naim
T=applied_bolt_torque; % Naim
Tc=tightness_class; % Naim
Ae=assembly_efficiency; % Naim
X=ratio_tpa_tpmin;  % Naim


unit=0;                                     %unit�
Seating_load=1;
Operating_load=0;
External_load=0;
Thermal_load=1;
Relaxation_load=0;
%{
unit= strUnit;
Seating_load=Seating_load_check;
Operating_load=Operating_load_check;
External_load=External_load_check;
Thermal_load=Thermal_load_check;
Relaxation_load=Relaxation_load_check;

%}
%--------------------------------------------------------------------------
%-------- printing input data ---------------------------------------------
%--------------------------------------------------------------------------

fid2 = fopen('InputData.log','wt');
fprintf(fid2,'Flange 1:\n --------- \n ');
fprintf(fid2,'Flange Type: %g\n Flange outside diameter : %g\n Height Of Raised Face : %g\n Flange thickness : %g\n Raised Face Diameter : %g\n Bolt circle diameter : %g\n Number of bolts : %g\n Flange inside diameter : %g\n Welding neck hub thickness : %g\n Shell thickness : %g\n Hub length : %g\n Flange Young modulus amb : %g\n Flange Young modulus temp : %g\n Flange Poisson ratio : %g\n Allowable Stress Amb : %g\n Allowable Stress Temp : %g\n Flange thermal expansion coef : %g\n Flange thermal conductivity : %g\n Heat film trans coef for flange out surface : %g\n Heat film trans coef for flange in surface : %g\n Flange creep constant A : %g\n Flange creep constant m : %g\n Flange creep constant n : %g\n Shell Head Type: %g\n Shell Poisson ratio : %g\n Shell Young modulus amb : %g\n Shell Young modulus temp : %g\n Shell creep constant n : %g\n Shell creep constant A: %g\n Shell creep constant m : %g\n Shell thermal expansion coef : %g\n Shell thermal conductivity : %g\n ',...
                FlgType1, flange_outside_diameter1, height_raised_face1, flange_thickness1, raised_face_diameter1, bolt_circle_diameter1,...
                number_of_bolts1, flange_inside_diameter1, welding_neck_hub_thickness1, shell_thickness1, hub_length1,...
                flange_Young_modulus_amb1, flange_young_modulus_temp1, flange_Poisson_ratio1,...
                flange_allowable_stress_amb1, flange_allowable_stress_temp1, flange_thermal_expansion_coef1, flange_thermal_conductivity1,...
                heat_film_trans_coef_for_flange_out_surface1, heat_film_trans_coef_for_flange_in_surface1,...
                flange_creep_constant_A1, flange_creep_constant_m1, flange_creep_constant_n1,...
                ShHeadType1, shell_Poisson_ratio1, shell_Young_modulus_amb1, shell_Young_modulus_temp1, shell_creep_constant_n1, shell_creep_constant_A1,...
                shell_creep_constant_m1, shell_thermal_expansion_coef1, shell_thermal_conductivity1);
            
fprintf(fid2,'\nFlange 2:\n --------- \n ');
fprintf(fid2,'Flange Type: %g\n Flange outside diameter : %g\n Height Of Raised Face : %g\n Flange thickness : %g\n Raised Face Diameter : %g\n Bolt circle diameter : %g\n Number of bolts : %g\n Flange inside diameter : %g\n Welding neck hub thickness : %g\n Shell thickness : %g\n Hub length : %g\n Flange Young modulus amb : %g\n Flange Young modulus temp : %g\n Flange Poisson ratio : %g\n Allowable Stress Amb : %g\n Allowable Stress Temp : %g\n Flange thermal expansion coef : %g\n Flange thermal conductivity : %g\n Heat film trans coef for flange out surface : %g\n Heat film trans coef for flange in surface : %g\n Flange creep constant A : %g\n Flange creep constant m : %g\n Flange creep constant n : %g\n Shell Head Type: %g\n Shell Poisson ratio : %g\n Shell Young modulus amb : %g\n Shell Young modulus temp : %g\n Shell creep constant n : %g\n Shell creep constant A: %g\n Shell creep constant m : %g\n Shell thermal expansion coef : %g\n Shell thermal conductivity : %g\n ',...
                FlgType2, flange_outside_diameter2, height_raised_face2, flange_thickness2, raised_face_diameter2, bolt_circle_diameter2,...
                number_of_bolts2, flange_inside_diameter2, welding_neck_hub_thickness2, shell_thickness2, hub_length2,...
                flange_Young_modulus_amb2, flange_young_modulus_temp2, flange_Poisson_ratio2,...
                flange_allowable_stress_amb2, flange_allowable_stress_temp2, flange_thermal_expansion_coef2, flange_thermal_conductivity2,...
                heat_film_trans_coef_for_flange_out_surface2, heat_film_trans_coef_for_flange_in_surface2, ...
                flange_creep_constant_A2, flange_creep_constant_m2, flange_creep_constant_n2,...
                ShHeadType2, shell_Poisson_ratio2, shell_Young_modulus_amb2, shell_Young_modulus_temp2, shell_creep_constant_n2, shell_creep_constant_A2,...
                shell_creep_constant_m2, shell_thermal_expansion_coef2, shell_thermal_conductivity2);                

% Bolt            
fprintf(fid2,'\nBolt:\n -----\n ');
fprintf(fid2,'Number of thread per inch: %g\n Kw : %g\n K : %g\n Bolt Young modulus amb : %g\n Bolt Young modulus temp : %g\n Ub : %g\n Sa : %g\n So : %g\n Bolt thermal expansion coef : %g\n Heat film trans coef from flange to bolt : %g\n Heat film trans coef from flange to bolt head : %g\n A_b : %g\n m_b : %g\n n_b : %g\n Bolt creep stress function type : %g\n Bolt creep stress constant 1 : %g\n Bolt creep stress constant 2 : %g\n Bolt creep stress constant 3 : %g\n Bolt creep stress constant 4 : %g\n Bolt creep time function type : %g\n Bolt creep time constant 1 : %g\n Bolt creep time constant 2 : %g\n Bolt creep time constant 3 : %g\n Bolt creep time constant 4 : %g\n ',...
                number_of_thread_per_inch, wachers_rigidity, nut_factor, bolt_Young_modulus_amb, bolt_Young_modulus_temp,...
                poisson_ratio, allowable_stress_amb, allowable_stress_temp, bolt_thermal_expansion_coef,...
                heat_film_trans_coef_from_flange_to_bolt, heat_film_trans_coef_from_flange_to_bolt_head,...
                bolt_creep_constant_A_b, bolt_creep_constant_m_b, bolt_creep_constant_n_b, ...
                Bolt_creep_stress_function_type, Bolt_creep_stress_constant_1, ...
                Bolt_creep_stress_constant_2,Bolt_creep_stress_constant_3, Bolt_creep_stress_constant_4, ...
                Bolt_creep_time_function_type, Bolt_creep_time_constant_1,  Bolt_creep_time_constant_2, ...
                Bolt_creep_time_constant_3, Bolt_creep_time_constant_4);           
 
% Gasket                
fprintf(fid2,'\nGasket: \n ------\n ');
fprintf(fid2,'Gasket_inside_diameter: %g\n Gasket_outside_diameter : %g\n IRID : %g\n CROD : %g\n Gasket_thickness : %g\n Contact_Width_Gasket : %g\n Gasket_thermal_expansion_coef : %g\n Gb : %g\n a : %g\n Gs : %g\n m : %g\n y: %g\n Tpmax: %g\n SL : %g\n N_L : %g\n N_t : %g\n ',...
                gasket_inside_diameter, gasket_outside_diameter, ring_outside_diameter, centring_ring_diameter, gasket_thickness, ...
                Contact_Width_Gasket, gasket_thermal_expansion_coef, intercept_part_A, slope_gb_line, intercept_part_B,...
                gasket_factor, seating_stress, max_tightness, min_operating_stress, stress_creep_function, time_creep_function);

 
[mDg,nDg]=size(Dg);
fprintf(fid2,' \nDisplacement matrix:\n --------------------\n');
fprintf(fid2,'m = %d\tn = %d\n\n',mDg,nDg);
for i1=1:mDg
    for j1=1:nDg
        fprintf(fid2,'%g\t',Dg(i1,j1));
    end;
    fprintf(fid2,' \n');
end;   

[mSg,nSg]=size(Sg);
fprintf(fid2,' \nStress matrix:\n --------------\n');
fprintf(fid2,'m = %d\tn = %d\n\n',mSg,nSg);
for i2=1:mSg
    for j2=1:nSg
      fprintf(fid2,'%g\t',Sg(i2,j2)); 
    end;
    fprintf(fid2,' \n');
end;      

fprintf(fid2,'\n Bolt nominal diameter: %g\n Gasket creep stress function type : %g\n Gasket creep stress constant 1 : %g\n Gasket creep stress constant 2 : %g\n Gasket creep stress constant 3 : %g\n Gasket creep stress constant 4 : %g\n Gasket creep time function type : %g\n Gasket creep time constant 1 : %g\n Gasket creep time constant 2 : %g\n Gasket creep time constant 3 : %g\n Gasket creep time constant 4: %g\n ',... 
                bolt_nominal_diameter, ...
                Gasket_creep_stress_function_type, Gasket_creep_stress_constant_1, Gasket_creep_stress_constant_2, ...
                Gasket_creep_stress_constant_3, Gasket_creep_stress_constant_4, ...
                Gasket_creep_time_function_type, Gasket_creep_time_constant_1, Gasket_creep_time_constant_2, ...
                Gasket_creep_time_constant_3, Gasket_creep_time_constant_4); 

% Design conditions
fprintf(fid2,'\n\nDesign conditions: \n -----------------\n');
fprintf(fid2,' Nominal pressure: %g\n Internal operating temperature : %g\n r : %g\n Fb : %g\n External axial force : %g\n External bending moment : %g\n External ambient temperature : %g\n Total creep time : %g\n Initial bolt stress : %g\n Sg : %g\n T: %g\n Tc : %g\n Ae: %g\n X: %g\n ',...
            nominal_pressure, internal_operating_temperature, test_pressure_ratio, initial_bolt_load, external_axial_force,...
            external_bending_moment, external_ambient_temperature, Total_creep_time, initial_bolt_stress, initial_gasket_stress, ...
            applied_bolt_torque, tightness_class, assembly_efficiency, ratio_tpa_tpmin);
                      
fprintf(fid2,'Unit: %s\n ', strUnit);
  
fprintf(fid2,'Seating load: %d\n Operating load: %d\n External load: %d\n Thermal load: %d\n Relaxation load: %d\n ',...
        Seating_load_check, ...
        Operating_load_check, ...
        External_load_check, ...
        Thermal_load_check, ...
        Relaxation_load_check);
        
fclose(fid2);


%-----------------------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------------------

% MAIN

%-----------------------------------------------------------------------------------------------------
% %----------------------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------------------


lb=0.5*1.125*d+tf(1)+tf(2)+tg;
StrArea=n*pi*(d-0.9743/nt)^2/4;
RootArea=n*pi*(d-1.3/nt)^2/4;
kb=StrArea*Eb/lb;

kw=kw*n;
if kw==0
    kw=inf;
end;
kbw=1/kb+1/kw;
kbw=1/kbw;
LoaInBol=BoltStress*RootArea;             %Load in bolt

AreaGas= pi*(OD^2-ID^2)/4;
d_hole=(d+0.125);
if OD>C                                     
   Area_hole=n*(d+0.125)^2*pi/4;
   Cgi=C-d_hole;
   Cgo=C+d_hole;
   AreaGas=AreaGas-Area_hole;
   G_uniform=((OD^3-ID^3)/6-n*d_hole^2*C/4)/0.25/((OD^2-ID^2)-n*d_hole^2);
   if C>G_uniform
      G_max=C;
      G_min=G_uniform;
   else
      G_max=G_uniform;
      G_min=C;
   end;
else
   G_uniform=2*(OD^2+ID^2+OD*ID)/3/(OD+ID);
   Cgi=C;
   Cgo=C;
end

kg=0;
Eg=0;

%----------------------------------------------------------------------------------------------------
% Seating 
%----------------------------------------------------------------------------------------------------


P_Eq=0;
ad_creep=0;
Pres_Seat=0;
init_TetaF1=0;
init_TetaF2=0;
G_Sgmi=G_uniform;
G_Fgi=G_uniform;
G_old=G_uniform;
rd_Temp=zeros(2,4);
rot_Temp=zeros(2,5);
ad_Temp=zeros(4);
total_ad_Temp=0;
rd_creep=zeros(2,4);
rot_creep=zeros(2,4);
Precision_min=0.0001;  %N/OD/100;
fid = fopen('outfile.txt','wt');
u_time='[ hr ]';
%{
if unit==1
   u_area='[ mm^2 ]';
   u_stress='[ MPa ]';
else
   u_area='[ in^2 ]';
   u_stress='[ psi ]';
end
%}
if unit=='I'
   u_area='[ in^2 ]';
   u_stress='[ psi ]';
else
   u_area='[ mm^2 ]';
   u_stress='[ MPa ]';
end


if Seating_load==1
disp('  INITIAL TIGHTENING');
fprintf(fid,'\n\n                               **********************\n');
fprintf(fid,'******************************** INITIAL TIGHTENING ***************************\n');
fprintf(fid,'                               **********************\n\n');

s=' = ';
str1='        Total bolt area ';
fprintf(fid,'%s%s%s%12.2f\n',str1,u_area,s,RootArea);
str2='    Gasket contact area ';
fprintf(fid,'%s%s%s%12.2f\n\n',str2,u_area,s,AreaGas);

Precision=1;
Seat_flag=1;
%K=1;
K = nut_factor;
%kg_old=kg;
while Precision > Precision_min
    AreaPres=pi*G_old^2/4;                        %Pressurazed Area
    NbFlange=1;

    [m1,Y1]=matrices(vc(1),vf(1),Ec(1),Ef(1),Eg,B(1),A(1),g1(1),g0(1),h(1),d,C,n,tf(1),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
            FlangeType1,ShellHeadType1,NbFlange,Pres_Seat,P_Eq,N,LoaInBol,0,rd_Temp,rot_Temp,rd_creep,rot_creep);   
         
    NbFlange=2;
    [m2,Y2]=matrices(vc(2),vf(2),Ec(2),Ef(2),Eg,B(2),A(2),g1(2),g0(2),h(2),d,C,n,tf(2),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
            FlangeType2,ShellHeadType2,NbFlange,Pres_Seat,P_Eq,N,LoaInBol,0,rd_Temp,rot_Temp,rd_creep,rot_creep);

    m2=flipud(m2);
    m2=fliplr(m2);
    M=[m1,zeros(13,12);zeros(12,13),zeros(12,12)];
    M([13:25],[13:25])=m2;

    Y2=flipud(Y2);
    Y=[Y1;zeros(12,1)];
    Y([14:25],[1:1])=Y2([2:13]);
    M(13,:)=0;
    Y(13)=LoaInBol;
    M(13,13)=1;
    Coef=M^-1*Y;
    init_TetaF1=Coef(12);        %imprime les angle de rotation 1 voir si on multiblie par 2pi pour avoir en deg
    init_TetaF2=Coef(14);        %imprime les angle de rotation 2
    GasketForce=Coef(13);
    if FlangeType1==6
       init_TetaF1=GasketForce/StifPl(1,2,1)-GasketForce/StifPl(1,2,2);
    end;
    if FlangeType2==6
       init_TetaF2=-(GasketForce/StifPl(2,2,1)-GasketForce/StifPl(2,2,2));
    end;
    StrGas=GasketForce/AreaGas;
    if StrGas < 0
        disp('  Error program stops - no stress on gasket');
        fprintf(fid,'  Error program stops - no stress on gasket');
        Seating_load=0;
        Operating_load=0;
        External_load=0;
        Thermal_load=0;
        Relaxation_load=0;
        break, return
    end;
    ugmi=si_ui_initial(StrGas);
    G_Sgmi=sgmi_position(N,OD,ID,Cgi,Cgo,C,d_hole,LoaInBol,kg,AreaGas,GasketForce,...
           n,init_TetaF1,init_TetaF2,ugmi,G_Sgmi,G_Fgi,G_old);
    [G_new,kg_new]=fg_position(N,OD,ID,Cgi,Cgo,C,d_hole,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
           n,init_TetaF1,init_TetaF2,0,0,ugmi,0,G_Sgmi,G_old,G_Fgi);
    G_Fgi=G_old;

    if OD > C                                     
        if G_new<G_max
           G_max=G_old;
           G_new=0.5*(G_max+G_min);
       else
           G_min=G_old;
           G_new=0.5*(G_max+G_min);
       end;
    end;
    Precision=abs((G_new-G_old)/G_old);
    G_old=abs(G_new);

end;
kg=kg_new;
ug_Fg=ugmi-(init_TetaF1-init_TetaF2)*(G_Sgmi-G_Fgi)/2;
G_old=G_Fgi;
final_TetaF1=init_TetaF1;
final_TetaF2=init_TetaF2;
[x_N,y_Sg_r,y_ug_r]=stress_distribution(N,OD,ID,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,GasketForce,...
    init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old);
stress(vc,vf,Ec,Ef,B,A,g1,g0,h,C,tf,Eb,lb,d,n,nt,G_old,FlangeType1,ShellHeadType1,FlangeType2,ShellHeadType2,...
    Pres_Seat,final_TetaF1,final_TetaF2,Coef,GasketForce,StrGas,x_N,y_Sg_r,unit,fid);
fclose(fid);
end; %Seating
 
%----------------------------------------------------------------------------------------------------
% Operating                        
%----------------------------------------------------------------------------------------------------


P_Eq=0;
Pres_Oper=Pressure;

G_max=OD;
G_min=G_old;
if OD > C                                     
   G_old=0.5*(G_max+G_min);
end;
if Operating_load==1
disp('  PRESSURIZATION');
fid = fopen('outfile.txt','at');
fprintf(fid,'\n\n                                 ******************\n');
fprintf(fid,'********************************** PRESSURIZATION *****************************\n');
fprintf(fid,'                                 ******************\n\n');
Seat_flag=0;
Precision=1;
%kg_old=kg;
while Precision > Precision_min 
%    if it > 4; break; end
    AreaPres=pi*G_old^2/4;                        %Pressurazed Area
    NbFlange=1;  
    [m1,Y1]=matrices(vc(1),vf(1),Ec(1),Ef(1),Eg,B(1),A(1),g1(1),g0(1),h(1),d,C,n,tf(1),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
            FlangeType1,ShellHeadType1,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,init_TetaF1,rd_Temp,rot_Temp,rd_creep,rot_creep);

    NbFlange=2;
    [m2,Y2]=matrices(vc(2),vf(2),Ec(2),Ef(2),Eg,B(2),A(2),g1(2),g0(2),h(2),d,C,n,tf(2),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
            FlangeType2,ShellHeadType2,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,-init_TetaF2,rd_Temp,rot_Temp,rd_creep,rot_creep);

    Y2=flipud(Y2);
    Y=[Y1];
    Y([14:25],[1:1])=Y2([2:13]);

    Y(13)=Y1(13)+Y2(1)+(1/kbw+1/kg)*LoaInBol+AreaPres*Pressure/kg-total_ad_Temp-ad_creep;  %beaucoup de chose a f�rifier ici
        
    m2=flipud(m2);
    m2=fliplr(m2);
    M=[m1,zeros(13,12);zeros(12,13),zeros(12,12)];
    M([13:25],[13:25])=m2;
    M(13,13)=m1(13,13)+m2(1,1)+1/kg+1/kbw;

    Coef=M^-1*Y;
    final_TetaF1=Coef(12);        %imprime les angle de rotation 1 voir si on multiblie par 2pi pour avoir en deg
    final_TetaF2=Coef(14);        %imprime les angle de rotation 2
    GasketForce=Coef(13)-AreaPres*Pressure;
    StrGas=GasketForce/AreaGas;
    if StrGas < 0
        disp('  Error program stops - no stress on gasket');
        fprintf(fid,'  Error program stops - no stress on gasket');
        Seating_load=0;
        Operating_load=0;
        External_load=0;
        Thermal_load=0;
        Relaxation_load=0;
        break, return
    end;
    if FlangeType1==6
       final_TetaF1=GasketForce/StifPl(1,2,1)-Coef(13)/StifPl(1,2,2)+Pressure/StifPl(1,2,3);
    end;
    if FlangeType2==6
       final_TetaF2=-(GasketForce/StifPl(2,2,1)-Coef(13)/StifPl(2,2,2)+Pressure/StifPl(2,2,3));
    end;
    ug_Fg=ug_force(N,OD,ID,Cgi,Cgo,C,d_hole,tg,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
          n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,G_Sgmi,G_old,G_Fgi);
    [G_new,kg_new]=fg_position        (N,OD,ID,Cgi,Cgo,C,d_hole,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
          n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old,G_Fgi);

    if OD > C                                     
       if G_new<G_old
           G_max=G_old;
           G_new=0.5*(G_max+G_min);
       else
           G_min=G_old;
           G_new=0.5*(G_max+G_min);
       end;
    end;
    Precision=abs((G_new-G_old)/G_old);
    G_old=abs(G_new);


end;
%kg=kg_new;
StrBol=Coef(13)/RootArea;
[x_N,y_Sg_r,y_ug_r]=stress_distribution(N,OD,ID,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,GasketForce,...
                    init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old);
stress(vc,vf,Ec,Ef,B,A,g1,g0,h,C,tf,Eb,lb,d,n,nt,G_old,FlangeType1,ShellHeadType1,FlangeType2,ShellHeadType2,...
    Pres_Oper,final_TetaF1,final_TetaF2,Coef,GasketForce,StrGas,x_N,y_Sg_r,unit,fid);
fclose(fid);
end; %Operating

%----------------------------------------------------------------------------------------------------
% External Loads                        
%----------------------------------------------------------------------------------------------------

if External_load==1
disp('  EXTERNAL LOADING');
fid = fopen('outfile.txt','at');
fprintf(fid,'\n\n                                ********************\n');
fprintf(fid,'********************************* EXTERNAL LOADING ****************************\n');
fprintf(fid,'                                ********************\n\n');
P_Eq=0;
Pres_Oper=Pressure;
Bme=(B(1)+B(2)+g0(1)+g0(2))/2;
AreaEnd=pi*Bme^2/4;                        %End Pressure Area

b_fe=(A(1)+A(2)-B(1)-B(2))/4;
R_m=(A(1)+B(1))/4;
t_fe=(tf(1)+tf(2))/2;
J_f=b_fe*t_fe^3/3*(1-192/(pi())^5*t_fe/b_fe*tanh(pi()*b_fe/(2*t_fe)));
I_f=R_m*t_fe^3*log(A(1)/B(1))/12;
BM_factor=1/(1+J_f/(2*I_f*(1+vf(1))));
%kg_old=kg;
L=n/2+1;
if Ext_Moment==0 
   L=1;
end;
for I=L:L
    P_Eq=BM_factor*16*Ext_Moment*sin(pi/2*(4/n*(I-1)-1))/Bme^3/pi;  %Calculation at bolt position
    P_Eq=P_Eq+Ext_Force/(pi*Bme^2/4);

    Seat_flag=0;
    Precision=1;

    while Precision > Precision_min
        AreaPres=pi*G_old^2/4;                        %Pressurazed Area
        NbFlange=1;
        [m1,Y1]=matrices(vc(1),vf(1),Ec(1),Ef(1),Eg,B(1),A(1),g1(1),g0(1),h(1),d,C,n,tf(1),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
                FlangeType1,ShellHeadType1,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,init_TetaF1,rd_Temp,rot_Temp,rd_creep,rot_creep);

        NbFlange=2;
        [m2,Y2]=matrices(vc(2),vf(2),Ec(2),Ef(2),Eg,B(2),A(2),g1(2),g0(2),h(2),d,C,n,tf(2),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
                FlangeType2,ShellHeadType2,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,-init_TetaF2,rd_Temp,rot_Temp,rd_creep,rot_creep);

        Y2=flipud(Y2);
        Y=[Y1];
        Y([14:25],[1:1])=Y2([2:13]);

        Y(13)=Y1(13)+Y2(1)+(1/kbw+1/kg)*LoaInBol+(AreaPres*Pressure+AreaEnd*P_Eq)/kg-total_ad_Temp-ad_creep;  %beaucoup de chose a f�rifier ici
        
        m2=flipud(m2);
        m2=fliplr(m2);
        M=[m1,zeros(13,12);zeros(12,13),zeros(12,12)];
        M([13:25],[13:25])=m2;
        M(13,13)=m1(13,13)+m2(1,1)+1/kg+1/kbw;

        Coef=M^-1*Y;
        final_TetaF1=Coef(12);        %imprime les angle de rotation 1 voir si on multiblie par 2pi pour avoir en deg
        final_TetaF2=Coef(14);        %imprime les angle de rotation 2
        GasketForce=Coef(13)-AreaPres*Pressure-AreaEnd*P_Eq;
        StrGas=GasketForce/AreaGas
        if StrGas < 0
            disp('  Error program stops - no stress on gasket');
            fprintf(fid,'  Error program stops - no stress on gasket');
            Seating_load=0;
            Operating_load=0;
            External_load=0;
            Thermal_load=0;
            Relaxation_load=0;
            break, return
        end;
        if FlangeType1==6
           final_TetaF1=GasketForce/StifPl(1,2,1)-Coef(13)/StifPl(1,2,2)+Pressure/StifPl(1,2,3);
        end;
        if FlangeType2==6
           final_TetaF2=-(GasketForce/StifPl(2,2,1)-Coef(13)/StifPl(2,2,2)+Pressure/StifPl(2,2,3));
        end;
        ug_Fg=ug_force(N,OD,ID,Cgi,Cgo,C,d_hole,tg,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
              n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,G_Sgmi,G_old,G_Fgi);
        [G_new,kg_new]=fg_position        (N,OD,ID,Cgi,Cgo,C,d_hole,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
            n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old,G_Fgi);

        if OD > C                                     
           if G_new<G_old
               G_max=G_old;
               G_new=0.5*(G_max+G_min);
           else
               G_min=G_old;
               G_new=0.5*(G_max+G_min);
           end;
        end;
        Precision=abs((G_new-G_old)/G_old);
        G_old=abs(G_new);
        
    end;

    x_t(I)=90*(4/n*(I-1)-1);%(I-1)*180/(n/2);
    y_SgBM(I)=Coef(13);
end;
%kg=kg_new;
StrBol=Coef(13)/RootArea;
[x_N,y_Sg_r,y_ug_r]=stress_distribution(N,OD,ID,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,GasketForce,...
                    init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old);
[x_N,y_Sg_r,y_ug_r]=stress_distribution(N,OD,ID,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,GasketForce,...
                    init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old);
stress(vc,vf,Ec,Ef,B,A,g1,g0,h,C,tf,Eb,lb,d,n,nt,G_old,FlangeType1,ShellHeadType1,FlangeType2,ShellHeadType2,...
    Pres_Oper,final_TetaF1,final_TetaF2,Coef,GasketForce,StrGas,x_N,y_Sg_r,unit,fid);

%fprintf(fid,'%1i\n',x_N,y_Sg_r,y_ug_r,x_t,y_SgBM);
fclose(fid);
end; %External_Loads


%----------------------------------------------------------------------------------------------------
% Thermal                        
%----------------------------------------------------------------------------------------------------

if Thermal_load==1;
kb=StrArea*Ebt/lb;
kbw=1/kb+1/kw;
kbw=1/kbw;
disp('  THERMAL EXPANSION EFFECT');
fid = fopen('outfile.txt','at');
fprintf(fid,'\n\n                            ****************************\n');
fprintf(fid,'***************************** THERMAL EXPANSION EFFECT ************************\n');
fprintf(fid,'                            ****************************\n\n');
P_Eq=0;
Pres_Oper=Pressure;
Seat_flag=0;             
Precision=1;

tol1=1e-3;

[rd_Temp,rot_Temp,ad_Temp]=thermal(hos,hi,kf,ks,alfaf,Eft,Ect,alfac,ti,to,tg,alfag,ID,OD,G_old,vf,vc,A,B,...
    g0,g1,tf,h,d,C,lb,n,alfab,hcb,hfb,tol1,FlangeType1,FlangeType2,ShellHeadType1,ShellHeadType2);
%Total axial displacement
total_ad_Temp=ad_Temp(4)-ad_Temp(3)-ad_Temp(1)-ad_Temp(2);             %+(rot_Temp(1,4)+rot_Temp(2,4))*(C-G_old)/2;
%kg_old=kg;

while Precision > Precision_min
    AreaPres=pi*G_old^2/4;                        %Pressurazed Area
    NbFlange=1;
    [m1,Y1]=matrices(vc(1),vf(1),Ect(1),Eft(1),Eg,B(1),A(1),g1(1),g0(1),h(1),d,C,n,tf(1),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
            FlangeType1,ShellHeadType1,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,init_TetaF1,rd_Temp,rot_Temp,rd_creep,rot_creep);

    NbFlange=2;
     [m2,Y2]=matrices(vc(2),vf(2),Ect(2),Eft(2),Eg,B(2),A(2),g1(2),g0(2),h(2),d,C,n,tf(2),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
            FlangeType2,ShellHeadType2,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,-init_TetaF2,rd_Temp,rot_Temp,rd_creep,rot_creep);

    Y2=flipud(Y2);
    Y=[Y1];
    Y([14:25],[1:1])=Y2([2:13]);

    Y(13)=Y1(13)+Y2(1)+(1/kbw+1/kg)*LoaInBol+AreaPres*Pressure/kg-total_ad_Temp-ad_creep;  %beaucoup de chose a f�rifier ici
        
    m2=flipud(m2);
    m2=fliplr(m2);
    M=[m1,zeros(13,12);zeros(12,13),zeros(12,12)];
    M([13:25],[13:25])=m2;
    M(13,13)=m1(13,13)+m2(1,1)+1/kg+1/kbw;
%    Y(13)=Y1(13)-(C-G_old)/2*(rot_Temp(1,4)+rot_Temp(2,4));
    
    Coef=M^-1*Y;
    GasketForce=Coef(13)-AreaPres*Pressure;
    StrGas=GasketForce/AreaGas;
    if StrGas < 0
       disp('  Error program stops - no stress on gasket');
       fprintf(fid,'  Error program stops - no stress on gasket');
       Seating_load=0;
       Operating_load=0;
       External_load=0;
       Thermal_load=0;
       Relaxation_load=0;
       break, return
    end;
    final_TetaF1=Coef(12)-rot_Temp(1,4);        %imprime les angle de rotation 1 voir si on multiblie par 2pi pour avoir en deg
    final_TetaF2=Coef(14)+rot_Temp(2,4);        %imprime les angle de rotation 2
    if FlangeType1==6
       final_TetaF1=final_TetaF1+GasketForce/StifPl(1,2,1)-Coef(13)/StifPl(1,2,2)+Pressure/StifPl(1,2,3)-rot_Temp(1,5);
    end;
    if FlangeType2==6
       final_TetaF2=final_TetaF2-(GasketForce/StifPl(2,2,1)-Coef(13)/StifPl(2,2,2)+Pressure/StifPl(2,2,3))+rot_Temp(2,5);
    end;
    ug_Fg=ug_force(N,OD,ID,Cgi,Cgo,C,d_hole,tg,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
          n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,G_Sgmi,G_old,G_Fgi);
    [G_new,kg_new]=fg_position        (N,OD,ID,Cgi,Cgo,C,d_hole,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
          n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old,G_Fgi);
    
    if OD > C                                     
        if G_new<G_max
           G_max=G_old;
           G_new=0.5*(G_max+G_min);
       else
           G_min=G_old;
           G_new=0.5*(G_max+G_min);
       end;
    end;
    Precision=abs((G_new-G_old)/G_old);
    G_old=abs(G_new);

end;
%kg=kg_new;
StrBol=Coef(13)/RootArea;
[x_N,y_Sg_r,y_ug_r]=stress_distribution(N,OD,ID,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,GasketForce,...
                    init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old);
final_TetaF1=Coef(12);
final_TetaF2=Coef(14);
stress(vc,vf,Ect,Eft,B,A,g1,g0,h,C,tf,Ebt,lb,d,n,nt,G_old,FlangeType1,ShellHeadType1,FlangeType2,ShellHeadType2,...
    Pres_Oper,final_TetaF1,final_TetaF2,Coef,GasketForce,StrGas,x_N,y_Sg_r,unit,fid);
fclose(fid);
end; %Thermal


%----------------------------------------------------------------------------------------------------
% Relaxation                        
%----------------------------------------------------------------------------------------------------

if Relaxation_load==1
disp('  CREEP RELAXATION');
fid = fopen('outfile.txt','at');
fprintf(fid,'\n\n                                ********************\n');
fprintf(fid,'********************************* CREEP RELAXATION ****************************\n');
fprintf(fid,'                                ********************\n\n');
P_Eq=0;
Pres_Oper=Pressure;
Seat_flag=0;
Precision=1;

ad_creep=0;
ad_creep_gas=0;
ad_creep_bol=0;
ad_creep_fl=0;


t_init=2;                     %start creep after 500 sec.
t_tot=1000;                   %finish creep after after 18000 sec.
t_inc=10;

t_count=t_init;
t_eqGas=t_init+t_inc; 
t_eqBol=t_eqGas;
if t_eqGas>=t_tot
   t_eqGas=t_init;
   t_eqBol=t_init;
end;

StrBol=Coef(13)/RootArea;
%[Fs,Gt]=creep_function(N_stress,N_time,a_stress,b_stress,c_stress,d_stress,a_time,b_time,c_time,d_time);
t=t_init;
StrBolOld=StrBol;
%s=StrGas;
%Ecreep_initGas=eval(Fs(1)*Gt(1));
%s=StrBol;
%Ecreep_initBol=eval(Fs(2)*Gt(2));
if creep_member(1)==1 | creep_member(2)==1
    [Ecreep_or_Time]=creep_function(1,0,0,0,0,t_init,t_init,StrGas,StrBol,N_stress,N_time,a_stress,b_stress,c_stress,d_stress,a_time,b_time,c_time,d_time);
    Ecreep_initGas=Ecreep_or_Time(1);
    Ecreep_initBol=Ecreep_or_Time(2);
end;
jj=1;
x_time(jj)=0;
S_g(jj)=StrGas;
S_b(jj)=StrBol;
P1an=Coef(5);
M1an=Coef(6);
P2an=Coef(21);
M2an=Coef(20);
epsi_t_cr_f1=zeros(21,21);
epsi_t_cr_f2=zeros(21,21);
epsi_r_cr_f1=zeros(21,21);
epsi_r_cr_f2=zeros(21,21);
elastic_TetaF1=final_TetaF1;
elastic_TetaF2=final_TetaF2;
%fid = fopen('stressdisplacementcf.txt','wt');
%kg_old=kg;
while (t_count<t_tot) & (StrGas>Pres_Oper)
    jj=jj+1;    
    t_count=t_count+t_inc;
%   Flange creep
%    [d_rot_creep_f,d_rd_creep_f]=flange_creep(A_f,m_f,n_f,final_TetaF1,final_TetaF2,Ef,A,B,tf,Pressure,t_count,t_inc,Coef(7),Coef(19),fid);
    if creep_member_index(3)==1
        [d_rot_creep_f,d_rd_creep_f]=ring_creep(A_f,m_f,n_f,final_TetaF1,final_TetaF2,Ef,vf,A,B,tf,Pressure,t_count,t_inc,Coef(7),Coef(19),fid);
        rot_creep(1,4)=rot_creep(1,4)+d_rot_creep_f(1);
        rot_creep(2,4)=rot_creep(2,4)+d_rot_creep_f(2);
        ad_creep_fl=(rot_creep(1,4)+rot_creep(2,4))*(C-G_old)/2;
        rd_creep(1,4)=rd_creep(1,4)+d_rd_creep_f(1);
        rd_creep(2,4)=rd_creep(2,4)+d_rd_creep_f(2);
    end;

%   Cylinder creep
%    [d_rot_creep_c,d_rd_creep_c]=cylinder_creep(A_c,m_c,n_c,Ect,vc,B,g0,Pressure,t_count,t_inc,Coef(5),Coef(6),Coef(20),Coef(21),fid);
%    rot_creep(1,1)=rot_creep(1,1)+d_rot_creep_c(1);
%    rot_creep(2,1)=rot_creep(2,1)+d_rot_creep_c(2);
%    rd_creep(1,1)=rd_creep(1,1)+d_rd_creep_c(1);
%    rd_creep(2,1)=rd_creep(2,1)+d_rd_creep_c(2);
    
    
%    t=t_eqGas;
%    t=t_eqBol;
%    s=StrGas
%    ad_creep_gas=eval(Fs(1)*Gt(1))-Ecreep_initGas;
%    t=t_eqBol;
%    s=StrBol
%    ad_creep_bol=eval(Fs(2)*Gt(2))-Ecreep_initBol;
    if creep_member(1)==1 | creep_member(2)==1
        [Ecreep_or_Time]=creep_function(1,Ecreep_initGas,Ecreep_initBol,ad_creep_gas,ad_creep_bol,t_eqGas,t_eqBol,StrGas,StrBol,N_stress,N_time,a_stress,b_stress,c_stress,d_stress,a_time,b_time,c_time,d_time);
        ad_creep_gas=Ecreep_or_Time(1)-Ecreep_initGas;
        ad_creep_bol=Ecreep_or_Time(2)-Ecreep_initBol;
    end;

    ad_creep=ad_creep_gas+ad_creep_bol*lb+ad_creep_fl;
%    ad_creep=double(ad_creep);
    Precision=1;
    while Precision > Precision_min
        AreaPres=pi*G_old^2/4;                        %Pressurazed Area
        NbFlange=1;
        [m1,Y1]=matrices(vc(1),vf(1),Ect(1),Eft(1),Eg,B(1),A(1),g1(1),g0(1),h(1),d,C,n,tf(1),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
                FlangeType1,ShellHeadType1,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,init_TetaF1,rd_Temp,rot_Temp,rd_creep,rot_creep);

        NbFlange=2;
        [m2,Y2]=matrices(vc(2),vf(2),Ect(2),Eft(2),Eg,B(2),A(2),g1(2),g0(2),h(2),d,C,n,tf(2),OD,ID,G_old,vg,tg,AreaPres,AreaGas,...
                FlangeType2,ShellHeadType2,NbFlange,Pres_Oper,P_Eq,N,LoaInBol,-init_TetaF2,rd_Temp,rot_Temp,rd_creep,rot_creep);

        Y2=flipud(Y2);
        Y=[Y1];
        Y([14:25],[1:1])=Y2([2:13]);

        Y(13)=Y1(13)+Y2(1)+(1/kbw+1/kg)*LoaInBol+AreaPres*Pressure/kg-total_ad_Temp-ad_creep;  %beaucoup de chose a f�rifier ici
        
        m2=flipud(m2);
        m2=fliplr(m2);
        M=[m1,zeros(13,12);zeros(12,13),zeros(12,12)];
        M([13:25],[13:25])=m2;
        M(13,13)=m1(13,13)+m2(1,1)+1/kg+1/kbw;

        Coef=M^-1*Y;
        elastic_TetaF1=Coef(12);        %imprime les angle de rotation 1 voir si on multiblie par 2pi pour avoir en deg
        elastic_TetaF2=Coef(14);        %imprime les angle de rotation 2
        final_TetaF1=elastic_TetaF1+rot_creep(1,4);
        final_TetaF2=elastic_TetaF2-rot_creep(2,4);
        GasketForce=Coef(13)-AreaPres*Pressure;
        StrGas=GasketForce/AreaGas;
        if StrGas < 0
           disp('  Error program stops - no stress on gasket');
           fprintf(fid,'  Error program stops - no stress on gasket');
           Seating_load=0;
           Operating_load=0;
           External_load=0;
           Thermal_load=0;
           Relaxation_load=0;
           break, return
        end;
        StrBol=Coef(13)/RootArea;
        final_TetaF1=Coef(12)-rot_Temp(1,4);        %ajouter 
        final_TetaF2=Coef(14)+rot_Temp(2,4);        %ajouter
        if FlangeType1==6
           final_TetaF1=GasketForce/StifPl(1,2,1)-Coef(13)/StifPl(1,2,2)+Pressure/StifPl(1,2,3);
        end;
        if FlangeType2==6
           final_TetaF2=-(GasketForce/StifPl(2,2,1)-Coef(13)/StifPl(2,2,2)+Pressure/StifPl(2,2,3));
        end;
        ug_Fg=ug_force(N,OD,ID,Cgi,Cgo,C,d_hole,tg,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
              n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,G_Sgmi,G_old,G_Fgi);
        [G_new,kg_new]=fg_position        (N,OD,ID,Cgi,Cgo,C,d_hole,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
              n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old,G_Fgi);
    

        if OD > C                                     
           if G_new<G_old
              G_max=G_old;
              G_new=0.5*(G_max+G_min);
           else
              G_min=G_old;
              G_new=0.5*(G_max+G_min);
           end;
        end;
        Precision=abs((G_new-G_old)/G_old);
        G_old=abs(G_new);
          

    end;

    if t_inc==0 
       t_count=t_tot+1;     %{Protection against t_inc=0}
    end;
%    s=StrGas;
%    t_eqGas=solve(eval(Fs(1))*Gt(1)-ad_creep_gas);
%    s=StrBol;
%    t_eqBol=solve(eval(Fs(2))*Gt(2)-ad_creep_bol);
%    t_eqBol=double(t_eqBol);
    if creep_member(1)==1 | creep_member(2)==1
        [Ecreep_or_Time]=creep_function(2,Ecreep_initGas,Ecreep_initBol,ad_creep_gas,ad_creep_bol,t_eqGas,t_eqBol,StrGas,StrBol,N_stress,N_time,a_stress,b_stress,c_stress,d_stress,a_time,b_time,c_time,d_time);
        t_eqGas=Ecreep_or_Time(1);
        t_eqBol=Ecreep_or_Time(2);
    end;

    x_time(jj)=t_count;
    S_g(jj)=StrGas;
    S_b(jj)=StrBol;

    if t_count>39 
      t_inc=20;
    end;
    if t_count>99 
      t_inc=50;
    end;
    if t_count>499 
      t_inc=100;
    end;
    if creep_member(1)==1 | creep_member(2)==1
        t_eqGas=t_eqGas+t_inc;
        t_eqBol=t_eqBol+t_inc;
    end;
    
end;    
%kg=kg_new;
[x_N,y_Sg_r,y_ug_r]=stress_distribution(N,OD,ID,Cgi,Cgo,Seat_flag,LoaInBol,AreaGas,GasketForce,...
               init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,ug_Fg,G_Sgmi,G_old);

stress(vc,vf,Ect,Eft,B,A,g1,g0,h,C,tf,Ebt,lb,d,n,nt,G_old,FlangeType1,ShellHeadType1,FlangeType2,ShellHeadType2,...
    Pres_Oper,final_TetaF1,final_TetaF2,Coef,GasketForce,StrGas,x_N,y_Sg_r,unit,fid);
fprintf(fid,'\n                          * STRESS RELAXATION *\n\n');
fprintf(fid,'            time             Bolt stress          Gasket stress\n');
fprintf(fid,'           %s               %s               %s \n\n',u_time,u_stress,u_stress);
for i=1:jj
    fprintf(fid,'     %12.2f          %12.2f          %12.2f\n',x_time(i),S_b(i),S_g(i));
end;

%fid = fopen('stressdisplacementc.txt','wt');
%fprintf(fid,'%1i\n',x_N,y_Sg_r,y_ug_r,x_time,S_b);
fclose(fid);
end; %Relaxation


%juste pour tester si le programme est arriv� � ce point,
aaaa = 9999;
