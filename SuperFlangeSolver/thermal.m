function [rd_Temp,rot_Temp,ad_Temp]=thermal(hos,hi,kf,ks,alfaf,Ef,Ec,alfac,ti,to,tg,alfag,ID,OD,G,vf,vc,A,B,g0,g1,tf,h,d,C,...
    lb,nb,alfab,hcb,hfb,tol1,FlangeType1,FlangeType2,ShellHeadType1,ShellHeadType2);

ri=zeros(2);
re=zeros(2);
ls=zeros(2);

ta=to;
if FlangeType1~=6
   ri(1)=B(1)/2;
   re(1)=ri(1)+g1(1);
   ls(1)=sqrt(g0(1)*ri(1));
else
   ri(1)=B(2)/2;
   re(1)=ri(2)+g1(2);
end
if FlangeType2~=6
   ri(2)=B(2)/2;
   re(2)=ri(2)+g1(2);
   ls(2)=sqrt(g0(2)*ri(2));
else
   ri(2)=B(1)/2;
   re(2)=ri(1)+g1(2);
end

if (FlangeType1==6) & (FlangeType2==6)
   ri(2)=ID/2;
   re(2)=OD/2;
end

ro=A/2;
ho=hos/kf;
te=tf(1)+tf(2)-tg;
rim=(ri(1)+ri(2))/2;
rom=(ro(1)+ro(2))/2;
rem=(re(1)+re(2))/2;
alfam=(alfaf(1)+alfaf(2))/2;
nuf=(vf(1)+vf(2))/2;

%--------------------------------

per=pi/te;
i=1;
critere=1e-6;
tol=1e-3;
%--------------------------------
per1=per/2;
epsi=0.01*per1;
if ho<per1-epsi
   xi=per1-epsi/100;
else
   xi=per1;
end
k=0;

n=10 ;% nombre de solution pour phi

phin(1:n)=0;
 
%-------------------------------------------------------------------
%   debut programme de calcul des phin par la methode de NEWTON
%-------------------------------------------------------------------
for m=1:n
    per1=per/2;

    if ho<per1
   
        phin(m)=a_newton(xi,te,ho,tol1,m);
    else
        phin(m)=a_newton1(xi,te,ho,tol1,m);
    end
    xi=xi+per;
end

somme=0;
som2=0;

for m=1:n
  
    R1=rom*phin(m);
    [I0,I1,K0,K1]=modbessel_function(R1);
    Ioro = I0;
    Koro = K0;
    I1ro = I1;
    K1ro = K1;

    R2=rem*phin(m);
    [I0,I1,K0,K1]=modbessel_function(R2);
    Iore = I0;
    Kore = K0;
    I1re = I1;
    K1re = K1;
  
      
    
    phirenp=phin(m)*(I1re*(phin(m)*K1ro-ho*Koro)-K1re*(phin(m)*I1ro+ho*Ioro));
    phiren=Iore*(phin(m)*K1ro-ho*Koro)+Kore*(phin(m)*I1ro+ho*Ioro);
    inphirero=((rom/phin(m))*I1ro-(rem/phin(m))*I1re)*(phin(m)*K1ro-ho*Koro)-((rom/phin(m))*K1ro-(rem/phin(m))*K1re)*(phin(m)*I1ro+ho*Ioro);

    res=(phin(m)*cos(phin(m)*te/2)+ho*sin(phin(m)*te/2))/phin(m)*(ho*cos(phin(m)*te)-ho-phin(m)*sin(phin(m)*te))*phirenp;
    res=res/((phin(m)^2+ho^2)*te+2*ho)/phiren;
    somme=somme+res;
    betanew=2*kf*somme;

    som2=som2+(((phin(m)*cos(phin(m)*te/2)+ho*sin(phin(m)*te/2))/phin(m)*(ho*cos(phin(m)*te)-ho-phin(m)*sin(phin(m)*te)))*inphirero)/(((phin(m)^2+ho^2)*te+2*ho)*phiren);

end

%---------------------------------
 beta=betanew;
%---------------------------------
%RESISTANCE INITIALIZATION
%------------------------
   Rsi=[inf,inf];
   Rs=[inf,inf];
   Rso=[inf,inf];
   Rhi=[inf,inf];
   Rha=[inf,inf];
   Rho=[inf,inf];
   Rhr=[inf,inf];
   Rpf=[inf,inf];
   Rpfo=[inf,inf];
   Rfh=inf;
   Rfi=inf;
   Rfo=inf;

%------------------------
%RESISTANCES OF BOLT
%------------------------

ln=d;
l1=d;
phib=d;
phind=1.5*d;
tfm=(tf(1)+tf(2))/2;
%hfb=1/(2/hos+0.06);               %attention conversion en imperial
Rab=1/(hcb*pi/4*(phind^2-phib^2));
Rbb=1/(hfb*pi*phib*2*tfm);
Rcb=1/(hos*pi*phib*(d+l1));
Rdb=1/(hos*pi*(phind*2*ln+2/4*phib^2));
 
%------------------------
%RESISTANCES OF CYLINDERS
%------------------------
if ShellHeadType1==1
   ras(1)=1/(2*pi*ri(1)*hi);    
   rbs(1)=(1/(2*pi*ks))*log((ri(1)+g0(1))/ri(1));
   rcs(1)=1/(2*pi*(ri(1)+g0(1))*hos);
   Rsi(1)=ras(1)/ls(1);
   Rs(1)=rbs(1)/ls(1);
   Rso(1)=rcs(1)/ls(1);
end
if ShellHeadType2==1
   ras(2)=1/(2*pi*ri(2)*hi);    
   rbs(2)=(1/(2*pi*ks))*log((ri(2)+g0(2))/ri(2));
   rcs(2)=1/(2*pi*(ri(2)+g0(2))*hos);
   Rsi(2)=ras(2)/ls(2);
   Rs(2)=rbs(2)/ls(2);
   Rso(2)=rcs(2)/ls(2);
end

%------------------------
%RESISTANCE OF FLANGE RING 1 AND 2
%------------------------
raf=1/(2*pi*rim*hi);
rbf=log(rem/rim)/(2*pi*kf);
rcf=1/(2*pi*rem*beta);
Rfh=raf/te;
Rfi=rbf/te;
Rfo=rcf/te;

%------------------------
%RESISTANCES HUB
%------------------------
if FlangeType1==1
   rah(1)=1/(2*pi*ri(1)*hi);    
   rbh(1)=(1/(2*pi*kf))*log((re(1))/ri(1));
   rch(1)=1/(2*pi*(re(1))*hos);
   Rhi(1)=rah(1)/h(1);
   Rha(1)=h(1)/(2*pi*kf*((((ri(1)+g1(1))^2-ri(1)^2)+((ri(1)+g0(1))^2-ri(1)^2))/2));
   Rho(1)=rch(1)/h(1);
   Rhr(1)=rbh(1)/h(1);
end
if FlangeType2==1
   rah(2)=1/(2*pi*ri(2)*hi);    
   rbh(2)=(1/(2*pi*kf))*log((re(2))/ri(2));
   rch(2)=1/(2*pi*(re(2))*hos);
   Rhi(2)=rah(2)/h(2);
   Rha(2)=h(2)/(2*pi*kf*((((ri(2)+g1(2))^2-ri(2)^2)+((ri(2)+g0(2))^2-ri(2)^2))/2));
   Rho(2)=rch(2)/h(2);
   Rhr(2)=rbh(2)/h(2);
end

%--------------------------------------
% RESISTANCE blind-cover  
%--------------------------------------
if FlangeType1==6
   Rpf(1)=tf(1)/(pi*(re(1)^2-(2*ri(1)-re(1))^2)*kf);
   Rpfo(1)=1/(pi*(re(1)^2-(2*ri(1)-re(1))^2)*hos);  
end
if FlangeType2==6
   Rpf(2)=tf(2)/(pi*(re(2)^2-(2*ri(2)-re(2))^2)*kf);
   Rpfo(2)=1/(pi*(re(2)^2-(2*ri(2)-re(2))^2)*hos);
end

%---------------------------------
% Calcul des temperatures 
%---------------------------------

MD(1,1)=1/Rsi(1)+1/Rhi(1)+1/(Rs(1)+Rso(1))+1/Rha(1);
MD(1,2)=-1/Rha(1);
MD(1,3)=0;
MD(2,1)=-1/Rha(1);
MD(2,2)=1/Rfh(1)+1/(Rfi+Rfo)+1/Rha(1)+1/Rha(2)+1/(Rhr(1)+Rho(1))+1/(Rhr(2)+Rho(2))+1/(Rpf(1)+Rpfo(1))+1/(Rpf(2)+Rpfo(2));   %+nb/Rdb
MD(2,3)=-1/Rha(2);
MD(3,1)=0;
MD(3,2)=-1/Rha(2);
MD(3,3)=1/Rsi(2)+1/Rhi(2)+1/(Rs(2)+Rso(2))+1/Rha(2);
MB(1,1)=ti/Rsi(1)+ti/Rhi(1)+to/(Rs(1)+Rso(1));
MB(2,1)=ti/Rfh(1)+to/(Rfi+Rfo)+to/(Rhr(1)+Rho(1))+to/(Rhr(2)+Rho(2))+to/(Rpf(1)+Rpfo(1))+to/(Rpf(2)+Rpfo(2));   %+nb/Rdb
MB(3,1)=ti/Rsi(2)+ti/Rhi(2)+to/(Rs(2)+Rso(2));

if FlangeType1==6
   MD(1,1)=1;
end
if FlangeType2==6
   MD(3,3)=1;
end


T=inv(MD)*MB;

% Flange ring
Tfh=to+((T(2)-to)*rcf/(rbf+rcf));
Tfi=T(2);

%---------------------------------------


for r=0:(rim-0)/30:rim;

% temperature au milieu de la plaque

%tfr=Tpo+(Tpi-Tpo)*(1-r^2/rim^2)-(Tpi-Tpo)*1/2;

%tfr=Tfi+(Tpi(1)-Tfi)*(1-r^3/rim^3);

%fprintf('%5.5d      %5.5f        \n',r,tfr)

%hold on
%plot(r,tfr,'b*')

 end
%-------------------------------------------
 
%fprintf('\n\nr                   tfrnew      \n')

rad(1)=rim;
rad(2)=G/2;
rad(3)=C/2;
rad(4)=rom;

%for r=rim:(rom-rim)/30:rom;

intrire=1/2*rem^2*(Tfh-ta+(Tfi-Tfh)*(log(rem/rem)+1/2)/log(rem/rim))-1/2*rim^2*(Tfh-ta+(Tfi-Tfh)*(log(rem/rim)+1/2)/log(rem/rim));
intrero=1/2*(to-ta)*(rom^2-rem^2)-2*(Tfh-to)*som2;

%for i=0:10
%    rad(i+1)=rim+i*(rom-rim)/10
%end

for i=1:4
   r=rad(i); 

 
   som1=0;
   som3=0;

   if r<=rem
      intrir=1/2*r^2*(Tfh-ta+(Tfi-Tfh)*(log(rem/r)+1/2)/log(rem/rim))-(1/2)*(rim^2)*(Tfh-ta+(Tfi-Tfh)*(log(rem/rim)+1/2)/log(rem/rim));
      tfr=Tfh+(Tfi-Tfh)*(log(rem/r)/log(rem/rim));
      ufr=(alfam/r)*(((1+nuf)*intrir)+((((1-nuf)*(r^2))+(1+nuf)*rim^2)/(rom^2-rim^2))*(intrire+intrero));
      sigmafr1=(alfaf(1)*Ef(1))*((-1/r^2*intrir)+(r^2-rim^2)/(r^2*(rom^2-rim^2))*(intrire+intrero));
      sigmaft1=(alfaf(1)*Ef(1))*((-(tfr-ta)+1/r^2*intrir)+(r^2+rim^2)/(r^2*(rom^2-rim^2))*(intrire+intrero));
      sigmafr2=(alfaf(2)*Ef(2))*((-1/r^2*intrir)+(r^2-rim^2)/(r^2*(rom^2-rim^2))*(intrire+intrero));
      sigmaft2=(alfaf(2)*Ef(2))*((-(tfr-ta)+1/r^2*intrir)+(r^2+rim^2)/(r^2*(rom^2-rim^2))*(intrire+intrero));
   else

      for m=1:n
   
        R1=r*phin(m);
        [I0,I1,K0,K1]=modbessel_function(R1);
        Ior = I0;
        Kor = K0;
        I1r = I1;
        K1r = K1;

        R2=rom*phin(m);
        [I0,I1,K0,K1]=modbessel_function(R2);
        Ioro = I0;
        Koro = K0;
        I1ro = I1;
        K1ro = K1;

        R3=rem*phin(m);
        [I0,I1,K0,K1]=modbessel_function(R3);
        Iore = I0;
        Kore = K0;
        I1re = I1;
        K1re = K1;


        phirn=Ior*(phin(m)*K1ro-ho*Koro)+Kor*(phin(m)*I1ro+ho*Ioro);
        inphirer=((r/phin(m))*I1r-(rem/phin(m))*I1re)*(phin(m)*K1ro-ho*Koro)-((r/phin(m))*K1r-(rem/phin(m))*K1re)*(phin(m)*I1ro+ho*Ioro);

        phiren=Iore*(phin(m)*K1ro-ho*Koro)+Kore*(phin(m)*I1ro+ho*Ioro);
        
        som1=som1+(((phin(m)*cos(phin(m)*te/2)+ho*sin(phin(m)*te/2))/phin(m)*(ho*cos(phin(m)*te)-ho-phin(m)*sin(phin(m)*te)))*phirn)/(((phin(m)^2+ho^2)*te+2*ho)*phiren);
        som3=som3+(((phin(m)*cos(phin(m)*te/2)+ho*sin(phin(m)*te/2))/phin(m)*(ho*cos(phin(m)*te)-ho-phin(m)*sin(phin(m)*te)))*inphirer)/(((phin(m)^2+ho^2)*te+2*ho)*phiren);
    
    end
    
        tfrin=((1/2)*(to-ta)*(r^2-rem^2))-2*(Tfh-to)*som3;
        tfr=to-2*(Tfh-to)*som1;
        ufr=(alfam/r)*((1+nuf)*(intrire+tfrin)+(((1-nuf)*r^2+(1+nuf)*rim^2)/(rom^2-rim^2))*(intrire+intrero));
        sigmafr1=(alfaf(1)*Ef(1))*(-1/r^2*(intrire+tfrin)+(r^2-rim^2)/(r^2*(rom^2-rim^2))*(intrire+intrero));
        sigmaft1=(alfaf(1)*Ef(1))*(-(tfr-ta)+1/r^2*(intrire+tfrin)+(r^2+rim^2)/(r^2*(rom^2-rim^2))*(intrire+intrero));
        sigmafr2=(alfaf(2)*Ef(2))*(-1/r^2*(intrire+tfrin)+(r^2-rim^2)/(r^2*(rom^2-rim^2))*(intrire+intrero));
        sigmaft2=(alfaf(2)*Ef(2))*(-(tfr-ta)+1/r^2*(intrire+tfrin)+(r^2+rim^2)/(r^2*(rom^2-rim^2))*(intrire+intrero));
 
   end

Tfhgb(i)=tfr;
ufhgb(i)=ufr;
sigmafr(1,i)=sigmafr1;
sigmaft(1,i)=sigmaft1;
sigmafr(2,i)=sigmafr2;
sigmaft(2,i)=sigmaft2;

end

%fid = fopen('stressdisplacementtd.txt','wt');
%fprintf(fid,'%1i\n',rad,Tfhgb,ufhgb);
%fclose(fid);


%-------------------------------
% Temperature
% ------------------------------
% Shells
if ShellHeadType1==1
   Tsi(1)=T(1);
   Tso(1)=to+(T(1)-to)*(rcs(1))/(rbs(1)+rcs(1));
   Tsif(1)=to+(ti-to)*(rbs(1)+rcs(1))/(ras(1)+rbs(1)+rcs(1));
   Tsof(1)=to+(Tsif(1)-to)*rcs(1)/(rbs(1)+rcs(1));
   deltaTs=-(Tsof(1)-Tsif(1)+Tso(1)-Tsi(1))/2;
   Bs(1)=(3*(1-vc(1)^2)/ri(1)^2/g0(1)^2)^0.25;
   rd_Temp(1,1)=(1+vc(1))*alfac(1)*deltaTs/(2*Bs(1)^2*g0(1))+alfac(1)*ri(1)*(Tso(1)-ta);
   rot_Temp(1,1)=-(1+vc(1))*alfac(1)*deltaTs/(Bs(1)*g0(1))+alfac(1)*ri(1)*(Tsof(1)-Tso(1))/ls(1);
end
if ShellHeadType2==1
   Tsi(2)=T(3);
   Tso(2)=to+(T(3)-to)*(rcs(2))/(rbs(2)+rcs(2));
   Tsif(2)=to+(ti-to)*(rbs(2)+rcs(2))/(ras(2)+rbs(2)+rcs(2));
   Tsof(2)=to+(Tsif(2)-to)*rcs(2)/(rbs(2)+rcs(2));
   deltaTs=-(Tsof(2)-Tsif(2)+Tso(2)-Tsi(2))/2;
   Bs(2)=(3*(1-vc(2)^2)/ri(2)^2/g0(2)^2)^0.25;
   rd_Temp(2,1)=(1+vc(2))*alfac(2)*deltaTs/(2*Bs(2)^2*g0(2))+alfac(2)*ri(2)*(Tso(2)-ta);
   rot_Temp(2,1)=-(1+vc(2))*alfac(2)*deltaTs/(Bs(2)*g0(2))+alfac(2)*ri(2)*(Tsof(2)-Tso(2))/ls(2);
end

% Hubs
if FlangeType1==1
   deltaTh=Tsi(1)-Tso(1);
   rh(1)=(ri(1)+re(1))/2;
   Bh(1)=(3*(1-vf(1)^2)/ri(1)^2/(g0(1))^2)^0.25;
   rd_Temp(1,2)=(1+vf(1))*alfaf(1)*deltaTh*exp(-Bh(1)*h(1))*(cos(Bh(1)*h(1))-sin(Bh(1)*h(1)))/(2*Bh(1)^2*g0(1))+alfaf(1)*ri(1)*(Tso(1)-ta);
%   rd_Temp(1,2)=(1+vf(1))*alfaf(1)*deltaTh/(2*Bh(1)^2*g0(1))+alfaf(1)*ri(1)*(Tso(1)-ta);
   rot_Temp(1,2)=-(1+vf(1))*alfaf(1)*deltaTh*exp(-Bh(1)*h(1))*cos(Bh(1)*h(1))/(Bh(1)*g0(1))+alfaf(1)*ri(1)*(Tso(1)-Tfh)/h(1);
%   rot_Temp(1,2)=(1+vf(1))*alfaf(1)*deltaTh/(Bh(1)*g0(1))+alfaf(1)*ri(1)*(Tso(1)-Tfh)/h(1);
   deltaTh=Tfi-Tfh;
   rd_Temp(1,3)=(1+vf(1))*alfaf(1)*deltaTh/(2*Bh(1)^2*g1(1))+alfaf(1)*rh(1)*(Tfh-ta);
   rot_Temp(1,3)=-(1+vf(1))*alfaf(1)*deltaTh/(Bh(1)*g1(1))+alfaf(1)*rh(1)*(Tso(1)-Tfh)/h(1);
end
if FlangeType2==1
   deltaTh=Tsi(2)-Tso(2);
   rh(2)=(ri(1)+re(1))/2;
   Bh(2)=(3*(1-vf(2)^2)/ri(1)^2/(g0(2))^2)^0.25;
   rd_Temp(2,2)=(1+vf(2))*alfaf(2)*deltaTh*exp(-Bh(2)*h(2))*(cos(Bh(2)*h(2))-sin(Bh(2)*h(2)))/(2*Bh(2)^2*g0(2))+alfaf(2)*ri(2)*(Tso(2)-ta);
%   rd_Temp(2,2)=(1+vf(2))*alfaf(2)*deltaTh/(2*Bh(2)^2*g0(2))+alfaf(2)*ri(2)*(Tso(2)-ta);
   rot_Temp(2,2)=-(1+vf(2))*alfaf(2)*deltaTh*exp(-Bh(2)*h(2))*cos(Bh(2)*h(2))/(Bh(2)*g0(2))+alfaf(2)*ri(2)*(Tso(2)-Tfh)/h(2);
%   rot_Temp(2,2)=(1+vf(2))*alfaf(2)*deltaTh/(Bh(2)*g1(2))+alfaf(2)*ri(2)*(Tso(2)-Tfh)/h(2);
   deltaTh=Tfi-Tfh;
   rd_Temp(2,3)=(1+vf(2))*alfaf(2)*deltaTh/(2*Bh(2)^2*g1(2))+alfaf(2)*rh(2)*(Tfh-ta);
   rot_Temp(2,3)=-(1+vf(2))*alfaf(2)*deltaTh/(Bh(2)*g1(2))+alfaf(2)*rh(2)*(Tso(2)-Tfh)/h(2);
end

%Flange or blind cover axial displacement difference between gasket and bolt location and resulting rotation
rd_Temp(1,4)=ufhgb(1);
rd_Temp(2,4)=ufhgb(1);
logrr=1/(2*log(rom/rim))-log(rom/(C/2))/log(rom/rim)-rim^2/(rom^2-rim^2);
%logrr=0;
adfb1=alfaf(1)*tf(1)*(Tfhgb(3)-ta)-alfaf(1)*vf(1)*tf(1)*(Tfhgb(1)-Tfhgb(4))/(1-vf(1))*logrr;
adfb2=alfaf(2)*tf(2)*(Tfhgb(3)-ta)-alfaf(2)*vf(2)*tf(2)*(Tfhgb(1)-Tfhgb(4))/(1-vf(2))*logrr;
logrr=1/(2*log(rom/rim))-log(rom/(G/2))/log(rom/rim)-rim^2/(rom^2-rim^2);
%logrr=0;
ad_Temp(1)=alfaf(1)*tf(1)*(Tfhgb(2)-ta)-alfaf(1)*vf(1)*tf(1)*(Tfhgb(1)-Tfhgb(4))/(1-vf(1))*logrr;
ad_Temp(2)=alfaf(2)*tf(2)*(Tfhgb(2)-ta)-alfaf(2)*vf(2)*tf(2)*(Tfhgb(1)-Tfhgb(4))/(1-vf(2))*logrr;
rot_Temp(1,4)=(ad_Temp(1)-adfb1)/(C-G);
rot_Temp(2,4)=(ad_Temp(2)-adfb2)/(C-G);

% additional blind cover axial displacement and rotation due to axial thermal gradient (spherical curvature)
if FlangeType1==6
   raf(1)=1/(pi*rim^2*hi);
   rbf(1)=tf(1)/(pi*rim^2*kf);
   rcf(1)=1/(pi*rim^2*hos);
   Tpi(1)=ti-(ti-to)*(raf(1))/(raf(1)+rbf(1)+rcf(1));
   Tpo(1)=to+(ti-to)*(rcf(1))/(raf(1)+rbf(1)+rcf(1));
   ad_Temp(1)=ad_Temp(1)+alfaf(1)*(Tpi(1)-Tpo(1))*(C^2-G^2)/(8*tf(1));
   rot_Temp(1,5)=alfaf(1)*(Tpi(1)-Tpo(1))*C/(4*tf(1));
end
if FlangeType2==6
   raf(2)=1/(pi*rim^2*hi);
   rbf(2)=tf(2)/(pi*rim^2*kf);
   rcf(2)=1/(pi*rim^2*hos);
   Tpi(2)=ti-(ti-to)*(raf(2))/(raf(2)+rbf(2)+rcf(2));
   Tpo(2)=to+(ti-to)*(rcf(2))/(raf(2)+rbf(2)+rcf(2));
   ad_Temp(2)=ad_Temp(2)+alfaf(2)*(Tpi(2)-Tpo(2))*(C^2-G^2)/(8*tf(2));
   rot_Temp(2,5)=alfaf(2)*(Tpi(2)-Tpo(2))*C/(4*tf(2));
end

%gasket
ad_Temp(3)=alfag*(Tfhgb(2)-ta)*tg;

% Bolt
Tb=((1/Rab+1/Rbb)*Tfhgb(3)+(1/Rcb+1/Rdb)*to)/(1/Rab+1/Rbb+1/Rcb+1/Rdb);
%ad_Temp(4)=alfab*(Tb-ta)*(tf(1)+tf(2)+tg);
ad_Temp(4)=alfab*(Tb-ta)*lb;
return
