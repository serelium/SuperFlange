function ug_Fg=ug_force(ContWidthGas,Ag,Bg,Cgi,Cgo,C,d_hole,tg,Seat_flag,LoaInBol,kg,AreaGas,GasketForce,...
    n,init_TetaF1,init_TetaF2,final_TetaF1,final_TetaF2,ugmi,G_Sgmi,G_old,G_Fgi)

% Calculate the location of the gasket mean stress}

N=250;                                 %Le nombre d'intervals pour le calcul de la position de Sgm}
Fg=0;
init_total_Rotation=init_TetaF1-init_TetaF2;
final_total_Rotation=final_TetaF1-final_TetaF2;
ug_Fgi=ugmi+init_total_Rotation*(G_Fgi-G_Sgmi)/2;
ug_Fg=ug_Fgi-(LoaInBol-GasketForce)/kg;
ug_Fg_max=2*ug_Fg;
ug_Fg_min=0;
K=0;
while abs(Fg-GasketForce) > abs(0.005*GasketForce)
  K=K+1
  Fg=0;
  Fg_hin=0;
  Fg_hout=0;
  dG_in=(G_old-Bg)/N;
  for I=0:N
      G_r(I+1)=Bg+I*dG_in;
  end    
  dG_out=(Ag-G_old)/N;
  for I=N+1:2*N
      G_r(I+1)=G_old+(I-N)*dG_out;
  end
  ug_ri=ugmi+init_total_Rotation*(G_r-G_Sgmi)/2;
  ug_r=ug_Fg+final_total_Rotation*(G_r-G_old)/2;
  [kf,Sg_r]=ui_uf_sf_k(ug_ri,ug_r);

  
  for I=1:2:N-1
      Fg=Fg+pi*(Sg_r(I)*G_r(I)+4*Sg_r(I+1)*G_r(I+1)+Sg_r(I+2)*G_r(I+2))*dG_in/6;
      if (G_r(I)>Cgi) & (G_r(I)<Cgo) & (G_r(I+1)<Cgo) & (G_r(I+2)<Cgo)
         Fg_hin=Fg_hin+(Sg_r(I)*(d_hole^2-(G_r(I)-C)^2)^.5+4*Sg_r(I+1)*(d_hole^2-(G_r(I+1)-C)^2)^.5+Sg_r(I+2)*(d_hole^2-(G_r(I+2)-C)^2)^.5)*dG_in/6;
      end   
  end
  
  %  Fg=trapz(pi*G_r.^2/4,Sg_r);
  
  for I=N+1:2:2*N-1
      Fg=Fg+pi*(Sg_r(I)*G_r(I)+4*Sg_r(I+1)*G_r(I+1)+Sg_r(I+2)*G_r(I+2))*dG_out/6;
      if (G_r(I)>Cgi) & (G_r(I)<Cgo) & (G_r(I+1)<Cgo) & (G_r(I+2)<Cgo)
         Fg_hout=Fg_hout+(Sg_r(I)*(d_hole^2-(G_r(I)-C)^2)^.5+4*Sg_r(I+1)*(d_hole^2-(G_r(I+1)-C)^2)^.5+Sg_r(I+2)*(d_hole^2-(G_r(I+2)-C)^2)^.5)*dG_out/6;
      end
  end
    
  %  Fg=Fg+trapz(pi*G_r.^2/4,Sg_r);

  Fg=Fg-n*(Fg_hin+Fg_hout);
  
  if  abs(Fg-GasketForce) > abs(0.005*GasketForce)
     if  Fg > GasketForce
         ug_Fg_max=ug_Fg;
         ug_Fg=0.5*(ug_Fg_min+ug_Fg_max);
     else
         ug_Fg_min=ug_Fg;
         ug_Fg=0.5*(ug_Fg_min+ug_Fg_max);
     end
  end

  if  K==20
      K=0;
      N=2*N;
  end
end
