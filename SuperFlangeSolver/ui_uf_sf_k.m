function [kf,S_f]=ui_uf_sf_k(u_i,u_f)


% fonction qui prend comme entr�e les d�placements initiaux et finaux et retourne la 
% contrainte finale S_f et la raideur du joint kf = dS/du en utilisant les
% �quations d'interpolation du krigeage
%
% �quation param�trique du krigeage d'une coordonn�e :
% XX(skrig,tkrig,rkrig) = SP * KA^-1 * coefX * KB^-1 * TP
% 
% param�tres d'entr�e
% vecteur des d�placements initiaux du joint:   u_i
% vecteur des d�placements finaux du joint:     u_f
% affichage des graphiques:                     graphe (si graphe <> 1, pas de graphiques)
%
% param�tres retourn�s
% vecteur des contraintes actuelles du joint:   S_f
% vecteur des raideurs actuelles du joint:      kf
%
% syntaxe: [kf,S_f=ui_uf_Sf_k(u_i,u_f)
% exemples: 
%   ui=[0.0110 0.01050 0.0100]; uf=[0.0300 0.0250 0.0200]; [kf,Sf]=ui_uf_Sf_k(ui,uf) % uf >> ui
%   ui=[0.0110 0.01050 0.0100]; uf=[0.0125 0.0120 0.0115]; [kf,Sf]=ui_uf_Sf_k(ui,uf) % uf > ui
%   ui=[0.0110 0.01050 0.0100]; uf=[0.0110 0.0105 0.0100]; [kf,Sf]=ui_uf_Sf_k(ui,uf) % uf = ui
%   ui=[0.0110 0.01050 0.0100]; uf=[0.0105 0.0100 0.0095]; [kf,Sf]=ui_uf_Sf_k(ui,uf) % uf < ui
%   ui=[0.0110 0.01050 0.0100]; uf=[0.0085 0.0080 0.0075]; [kf,Sf]=ui_uf_Sf_k(ui,uf) % uf << ui
%   ui=[0.0110 0.01050 0.0100]; uf=[0.0050 0.0045 0.0040]; [kf,Sf]=ui_uf_Sf_k(ui,uf) % uf <<< ui
%   ui=[0.0110 0.01050 0.0100]; uf=[0.0000 0.0000 0.0000]; [kf,Sf]=ui_uf_Sf_k(ui,uf) % uf = 0
%   ui=[0.00250 0.002450 0.00240 0.00230]; uf=[0.00210 0.00205 0.00200 0.00190]; [kf,Sf]=ui_uf_Sf_k(ui,uf) % ui et uf <<<<< 1

global Dg Sg u_map S_map umax Smax sref tref derivA covA angleRA derivB covB angleRB IP JP KA KB coefX coefY

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1 - choix pour le calcul des d�riv�es: param�trique ou diff�rences finies
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
df=1;                   % d�riv�e par diff�rences finies
%df=0;                  % d�riv�e param�trique
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2 - affichage des graphiques: 1 ou 0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
graphe=0;               % affichage graphique si graphe = 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3 - caract�ristiques du joint: X = d�placements, Y = contraintes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[u_map,S_map]=u_S;      % renvoie u et S (chargement et d�chargement)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4 - D�terminer le type des d�rives et des covariances pour chaque profil A ou B
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mean_cov;               % renvoie les choix pour les param�tres de krigeage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5 - % Calcul des matrices de krigeage KA et KB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rappel: �quation param�trique : XX(skrig,tkrig,rkrig) = SP * KA^-1 * coefX * KB^-1 * TP
% O� 	SP : matrice des coefficients param�triques li� � la covariance et � la d�rive du profil A
%		KA : matrice de krigeage du profil A
%	coefX : matrice form�e � partir des coordonn�es en X des points krig�s
%		KB : matrice de krigeage du profil B
%		TP : matrice des coefficients param�triques li� � la covariance et � la d�rive du profil B
%
% NB: Pour obtenir les coordonn�es en Y et en Z il suffit de remplacer XX et
%     coefX par la lettre corresponndante.
% Calcul des matrices de krigeage KA et KB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
KA_X_KB(u_map,S_map)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6 - graphe du comportement du joint krig� si graphe = 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if graphe == 1
%    figure(1), hold on
%    graphe_u_S(u_map,S_map)
%end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7 - d�placements: u_i d�placement au serrage initial
%                   u_f d�placement en service
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ni=length(u_i);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8 - Calcul de skrig les coordonn�es param�triques correspondantes
%     au d�placement initial, permettant ainsi de calculer la contrainte associ�e
%     sk_i  % vecteur des skrig, skrig est la valeur cherch�e de s pour chaque valeur de d�pl. donn�e 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
umax=max(max(u_map));
Smax=max(max(S_map));
tk_i=ones(1,ni);        % le point cherch� se trouve sur la courbe de chargement
for i=1:ni
    % le d�placement initial est sup�rieur au plus grand d�placement sur la courbe de chargement
    % ==> le d�placement assign� est le plus grand d�placement sur la courbe de chargement
    % ==> le param�tre normalis� s est mis �gal � 1
    if u_i(i)>=umax
        u_i(i)=umax;
        S_i(i)=Smax;
        sk_i(i)=1;
    elseif u_i(i)<0
        u_i(i)=0;
        S_i(i)=0;
        sk_i(i)=0;
    else
        % le d�placement initial est � l'int�rieur des limites de la courbe
        [sk_i(i),S_i(i)]=seek_s(u_i(i));    % calcul de s sur la courbe de chargement (t=1)
    end
end
S_i_graphe=S_i;
% NB: les valeurs de s pour chaque entr�e de u_i sont � pr�sent connues
%     ainsi que la valeur correspondante de la contrainte (remarque: t=1
%     pour tous les d�placements initiaux <==> courbe de chargement)
%     Les valeurs de s permettent de situer sur quelles courbes de
%     d�chargement se trouvent les d�placements finaux 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9 - Calcul de skrig et tkrig, les coordonn�es param�triques correspondantes
%     au d�placement final, permettant ainsi de calculer la contrainte associ�e
%     sk_f  % vecteur des skrig, skrig est la valeur cherch�e de s pour chaque valeur de d�pl. donn�e 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:ni
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 8.1 - Calcul de skrig, les coordonn�es param�triques correspondantes au d�placement final
    %       NB: si u_f > u_i, u_f et S_f sont sur la courbe de chargement ==> t(u_i(i)) = t(u_f(i)) = 1
    %           sinon s(u_f)=s(u_i)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % assurance que les valeurs se trouvent sur la plage de donn�es
    % le d�placement est n�gatif ==> la contrainte est nulle, et les
    % coordonn�es param�triques sont mises � z�ro
    if u_f(i)<=0
        u_f(i)=0;
        S_f(i)=0;
        sk_f(i)=0;
        tk_f(i)=0;
    % le d�placement final est sup�rieur au plus grand d�placement sur la courbe de chargement
    % la contrainte assign�e est la plus grande contrainte sur la courbe de
    % chargement, et les coordonn�es param�triques sont mises �gales � l'unit�
    elseif u_f(i)>=umax
        u_f(i)=umax;
        S_f(i)=Smax;
        sk_f(i)=1;
        tk_f(i)=1;
    elseif u_f(i)>=u_i(i)
        % le d�placement final est sur la courbe de chargement
        [sk_f(i),S_f(i)]=seek_s(u_f(i));
        tk_f(i)=1;
    else
        % v�rifier si le d�placement final est sur la courbe de d�chargement
        skrig=sk_i(i);       % courbe de d�charge
        % Cr�ation de la matrice param�trique du profil A, SP.
        SP=matRP(skrig,IP,covA,derivA,angleRA,sref);
        % Cr�ation de la matrice param�trique du profil B, TP, pour t=0
        if JP >= 2, TP=matRP(0,JP,covB,derivB,angleRB,tref)'; else, TP = 1; end
        % Calcul de u et sigma correspondants � skrig et tkrig
        uminf=[SP*KA^-1*coefX*KB^-1*TP];
        if u_f(i)<uminf
            % le d�placement n'est pas sur la courbe de d�chargement ==> S_f = 0
            sk_f(i)=0;
            tk_f(i)=0;
            S_f(i)=0;
        else    
            sk_f(i)=sk_i(i);
            [tk_f(i),S_f(i)]=seek_t(u_f(i),skrig);
        end % if
    end % else
end % for inc=1:ni
S_f_graphe=S_f;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 10 - montre les points initiaux et finaux sur les courbes krig�es
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if graphe==1
    graphe_if(u_i,u_f,S_i,S_f)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 11 - % calcul des ki (rigidit� au d�chargement)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dx=[];
dy=[];
% calcul des ki (rigidit� au d�chargement)
% KA, KB, coefX et coefY restent inchang�es
for k=1:ni
    if df==1
        % calcul par diff�rences finies
        % Cr�ation de la matrice param�trique du profil A, SP.
        SP=matRP(sk_f(k),IP,covA,derivA,angleRA,sref);
        % Cr�ation de la matrice param�trique du profil B, TP, pour t=0
        if JP >= 2, TP1=matRP(tk_f(k),JP,covB,derivB,angleRB,tref)'; else, TP1 = 1; end
        % Calcul de u et sigma correspondants � s et t
        %u1=[SP*KA^-1*coefX*KB^-1*TP1];
        %S1=[SP*KA^-1*coefY*KB^-1*TP1];
            dt=1/10000; % variation pour le calcul de la d�riv�e par diff�rences finies
        if (tk_f(k)+dt)<1&(tk_f(k)+dt)>=0
            t=tk_f(k)+dt;
        else
            t=tk_f(k)-dt;
            dt=-dt;
        end            
        % Cr�ation de la matrice param�trique du profil B, TP, pour t=0
        if JP >= 2, TP2=matRP(t,JP,covB,derivB,angleRB,tref)'; else, TP2 = 1; end
        % Calcul de u et sigma correspondants � s et t
        %u2=[SP*KA^-1*coefX*KB^-1*TP];
        %S2=[SP*KA^-1*coefY*KB^-1*TP];
        dTPdf=(TP1-TP2)/dt;
        dTP=dTPdf; % d�riv�e par diff�rences finies
    else
        % calcul de dTP par d�riv�e param�trique
        covar=covB;
        drift=derivB;
        angle=angleRB;
        I=JP;
        nb_pts=1;
        % Vecteurs contenant la position param�trique des points connus.
        vref = tref;

        % Vecteurs contenant la position param�trique des points apr�s krigeage.
        vkrig = tk_f(k);

        MPp = [];
        % Calcul des matrices des coefficients param�triques li�es � la covariance
        % et � la d�rive de chaque profil. Cr�ation de la matrice param�trique, MP.
        % Calcul de la partie de la covariance de la matrice des coefficients
        % param�triques. Cette partie de la matrice a les dimensions suivantes :
        % [nb_pts, I].
        for i = 1:I
            H = sqrt((vkrig - vref(i)).^2);
            if covar == 1 % Covariance lin�aire.
                MPp = [MPp, sign(vkrig - vref(i))];
            elseif covar == 2 % Covariance cubique.
                MPp = [MPp, 3 * H.^2 .* sign(vkrig - vref(i))];
            elseif covar == 3 % Covariance logarithmique.
                MPj = [];
                for j = 1:length(H)
                    if H(j) == 0
                        MPj = [MPj; 0];
                    else
                        MPj = [MPj; 2 * (vkrig(j) - vref(i)) * log(abs(vkrig(j) - vref(i))) + vkrig(j) - vref(i)];
                    end;
                end;
                MPp = [MPp, MPj];
            elseif covar == 4 % Covariance exponentielle.
                MPp = [MPp, - 2 * (vkrig - vref(i)) .* exp(-H.^2)];
            elseif covar == 5 % Covariance sinuso�dale.
                MPp = [MPp, angle * sign(vkrig - vref(i)) .* cos(angle * H)]; 
            end
        end
        % Ajout et calcul de la partie de la d�rive de la matrice des
        % coefficients param�triques. Cette partie de la matrice a les
        % dimensions suivantes : [nb_pts, drift + 1]. Cette matrice est remplie
        % du vecteur des coordonn�es param�triques, positionn� en colonne, o�
        % chaque �l�ment est �lev� de z�ro jusqu'� la puissance de la d�rive.
        % C'est pourquoi la premi�re colonne de la matrice est compos�e de uns.
        MPp = [MPp, zeros(nb_pts, 1)]; % Premi�re colonne de la matrice, seulement des z�ros.
        if drift ~= 5 % D�rive polynomiale.
            % Boucle pour remplir de la 2i�me � la derni�re colonne en variant la puissance
            % d'�l�vation des �l�ments du vecteur colonne.
            for i = 1:(drift - 1)
                MPp = [MPp, i * vkrig.^(i - 1)];
            end
        else % La d�rive est sinuso�dale.
            MPp = [MPp, angle * cos(angle * vkrig), -angle * sin(angle * vkrig)];   
        end
        dTP=MPp';  % d�riv�e param�trique
    end
    % calcul de dy/dx
    dx=[dx,SP*KA^-1*coefX*KB^-1*dTP];
    if tk_f(k)==0
        dy=[dy,0];
    else
        dy=[dy,SP*KA^-1*coefY*KB^-1*dTP];
    end
end                                     % for k=1:length(tk)
kf=dy./dx;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 12 - graphe_k
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if graphe == 1
    graphe_k(u_f,S_f,dx,umax,kf)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sous-programmes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3 - [u_map,S_map]=u_S;                        % renvoie u et S (chargement et d�chargement)
% 4 - mean_cov;                                 % renvoie les choix pour les param�tres de krigeage
% 5 - KA_X_KB(X,Y)                              % calcul des matrices de krigeage KA KB coefX et coefY
% 6 - graphe_u_S(X,Y)                           % affiche le courbes de krigeage
%   6.1 - [RP]=matRP(r,I,cov,deriv,angleR,rref) % calcul des matrices RP
%   8.1.1 - [sif,Sif]=seek_s(uif);              % recherche de s pour t = 1
%   8.2.1 - [tif,Sif]=seek_t(uif,s);            % recherche de t pour s donn�
% 10 - graphe_if(ui,uf,S_i,S_f)                 % montre les points initiaux et finaux sur les courbes krig�es
% 12 - graphe_k(uf,Sf,dx,umx,k)                 % trace les pentes aux points de d�chargement
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
function [X,Y]=u_S;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3 - caract�ristiques du joint: u = d�placements, S = contraintes
global Dg Sg
%  GALOCK 8748 t=1/16 in

   X=Dg;
   
   Y=Sg;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function mean_cov;
%function [derivA,covA,derivB,covB,angleRB,IP,JP,sref,tref]=mean_cov;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4 - D�terminer le type des d�rives et des covariances pour chaque profil A ou B
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global u_map S_map covA angleRA derivB covB angleRB IP JP sref tref
derivA = 1; % lin�aire = 2
covA = 3;   % cubique = 3
%covA = 1;   % cubique = 3
angleRA = 30;   
derivB = 1;
%covB = 3;
covB = 1;
angleRB = 30;
[IP,JP]=size(u_map);
sref=linspace(0,1,IP); % Vecteur r�f�rence des positions param�triques des points le long du profil A
tref=linspace(0,1,JP); % Vecteur r�f�rence des positions param�triques des points le long du profil B
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function KA_X_KB(X,Y)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5 - % Calcul des matrices de krigeage KA et KB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rappel: �quation param�trique : XX(skrig,tkrig,rkrig) = SP * KA^-1 * coefX * KB^-1 * TP
% O� 	SP : matrice des coefficients param�triques li� � la covariance et � la d�rive du profil A
%		KA : matrice de krigeage du profil A
%	coefX : matrice form�e � partir des coordonn�es en X des points krig�s
%		KB : matrice de krigeage du profil B
%		TP : matrice des coefficients param�triques li� � la covariance et � la d�rive du profil B
%
% NB: Pour obtenir les coordonn�es en Y et en Z il suffit de remplacer XX et
%     coefX par la lettre corresponndante.
% Calcul des matrices de krigeage KA et KB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global sref tref derivA covA angleRA derivB covB angleRB IP JP KA KB coefX coefY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.1 - Calcul du quadran sup�rieur gauche de la matrice KA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ce quadran est une matrice carr�e [IP,IP] diagonale, c'est-�-dire que M(i,j)=M(j,i)
% Cette matrice est remplie par les valeurs absolues de la diff�rence des coordonn�es
% param�triques le long du profil en question �lev�e � la puissance de la covariance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:IP-1
    for j=2:IP
        if covA < 4 % Covariances polynomiales
            KAabs(i,j)=abs(sref(i)-sref(j))^covA; % Partie sup�rieure de la diagonale de z�ro
            KAabs(j,i)=abs(sref(i)-sref(j))^covA; % Partie inf�rieure de la diagonale de z�ro   
        elseif covA == 4 % Covariance logarithmique
            KAabs(i,j)=abs(sref(i)-sref(j))^2*log(abs(sref(i)-sref(j))); % Partie sup�rieure de la diagonale de z�ro
            KAabs(j,i)=abs(sref(i)-sref(j))^2*log(abs(sref(i)-sref(j))); % Partie inf�rieure de la diagonale de z�ro
        elseif covA == 5 % Covariance sinuso�dale
            KAabs(i,j)=sin(angleRA*abs(sref(i)-sref(j))*pi/180); % Partie sup�rieure de la diagonale de z�ro
            KAabs(j,i)=sin(angleRA*abs(sref(i)-sref(j))*pi/180); % Partie inf�rieure de la diagonale de z�ro   
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.2 - Calcul du quadran sup�rieur gauche de la matrice KB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calcul du quadran sup�rieur gauche de la matrice KB
% Ce quadran est une matrice carr�e (JP X JP) diagonale, c'est-�-dire que M(i,j)=M(j,i)
% Cette matrice est remplie par les valeurs absolues de la diff�rence des coordonn�es
% param�triques le long du profil en question �lev�e � la puissance de la covariance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if JP >= 2
    for i=1:JP-1
        for j=2:JP
            if covB < 4 % Covariances polynomiales
                KBabs(i,j)=abs(tref(i)-tref(j))^covB; % Partie sup�rieure de la diagonale de z�ro
                KBabs(j,i)=abs(tref(i)-tref(j))^covB; % Partie inf�rieure de la diagonale de z�ro   
            elseif covB == 4 % Covariance logarithmique
                KBabs(i,j)=abs(tref(i)-tref(j))^2*log(abs(tref(i)-tref(j))); % Partie sup�rieure de la diagonale de z�ro
                KBabs(j,i)=abs(tref(i)-tref(j))^2*log(abs(tref(i)-tref(j))); % Partie inf�rieure de la diagonale de z�ro
            elseif covB == 5 % Covariance sinuso�dale
                KBabs(i,j)=sin(angleRB*abs(tref(i)-tref(j))*pi/180); % Partie sup�rieure de la diagonale de z�ro
                KBabs(j,i)=sin(angleRB*abs(tref(i)-tref(j))*pi/180); % Partie inf�rieure de la diagonale de z�ro   
            end
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.3 - Calcul du quadran sup�rieur droit de la matrice KA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ce quadran est une matrice [IP,derivA+1], cette matrice est remplie du vecteur des
% coordonn�es param�triques le long du profil, positionn� en colonne, o� chaque �l�ment
% est �lev� de z�ro jusqu'� la puissance de la d�rive du profil.  C'est pourquoi la
% premi�re colonne de la matrice est compos�e de uns.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
KAs=[ones(IP,1)]; % Premi�re colonne de la matrice, seulement des uns
if derivA ~= 5
    for i=1:derivA-1 % Boucle pour remplir de la 2i�me � la derni�re colonne
        KAs=[KAs,sref.^i']; % en variant la puissance d'�l�vation des �l�ment du vecteur colonne
    end
else % La d�rive est sinusoidale
    KAs=[KAs,sin(angleRA*pi/180*sref'),cos(angleRA*pi/180*sref')];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.4 - Calcul du quadran sup�rieur droit de la matrice KB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ce quadran est une matrice [JP,derivB+1], cette matrice est remplie du vecteur des
% coordonn�es param�triques le long du profil, positionn� en colonnes, o� chaque �l�ment
% est �lev� de z�ro jusqu'� la puissance de la d�rive du profil.  C'est pourquoi la
% premi�re colonne de la matrice est compos�e de uns.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
KBt=[ones(JP,1)]; % Premi�re colonne de la matrice, seulement des uns
if JP >= 2
    if derivB ~= 5
        for i=1:derivB-1 % Boucle pour remplir de la 2i�me � la derni�re colonne
            KBt=[KBt,tref.^i']; % en variant la puissance d'�l�vation des �l�ment du vecteur colonne
        end
    else % La d�rive est sinusoidale
        KBt=[KBt,sin(angleRB*pi/180*tref'),cos(angleRB*pi/180*tref')];
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.5 - Rassemblement de la matrice de krigeage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% La partie sup�rieure est l'union des deux matrices calcul�es pr�c�demment.
% La partie inf�rieure de la matrice de krigeage est compos�e de deux autres matrices.
% La premi�re se trouve � �tre la transpos�e de la matrice composant la partie sup�rieure
% droite et la seconde matrice est une matrice carr�e pleine de z�ro de la dimension de la
% d�rive plus 1. (Exemple : Si la d�rive est lin�aire, la matrice est 2X2.)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if derivA ~= 5
    KA=[KAabs,KAs;KAs',zeros(derivA)]; % Rassemblement de la matrice de krigeage KA
else % La d�rive est sinusoidale
    KA=[KAabs,KAs;KAs',zeros(3)]; % Rassemblement de la matrice de krigeage KA
end
if JP >= 2
    if derivB ~= 5
        KB=[KBabs,KBt;KBt',zeros(derivB)]; % Rassemblement de la matrice de krigeage KB
    else % La d�rive est sinusoidale
        KB=[KBabs,KBt;KBt',zeros(3)]; % Rassemblement de la matrice de krigeage KB
    end
else
    KB = 1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.6 - Calcul des matrices form�es � partir des coordonn�es des points � kriger
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matrices form�es � partir des coordonn�es des points � kriger pour la partie sup�rieure gauche [IP,JP,nr]
% Les trois autres parties sont des matrices z�ros : la matrice de la partie sup�rieure droite
% de dimension [IP,detB,nr], les deux parties inf�rieures ont respectivement [detA,JP,nr]
% et [detA,detB,nr]
% detA est �gale � derivA si la d�rive est polymomiale et � 3 si la d�rive est sinusoidale
% detB est �gale � derivB si la d�rive est polymomiale et � 3 si la d�rive est sinusoidale
%
% D�terminer les d�terminants de chaque profil selon le type de la d�rive
% Rappel: �quation param�trique : XX(skrig,tkrig,rkrig) = SP * KA^-1 * coefX * KB^-1 * TP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if derivA ~= 5
    detA=derivA;
else % La d�rive est sinusoidale
    detA=3;
end
if derivB ~= 5
    detB=derivB;
else % La d�rive est sinusoidale
    detB=3;
end
% Assemblage des matrices de coefficients
coefX=[X,zeros(IP,detB);zeros(detA,JP),zeros(detA,detB)]; % Pour les coordonn�es en x
coefY=[Y,zeros(IP,detB);zeros(detA,JP),zeros(detA,detB)]; % Pour les coordonn�es en y
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function graphe_u_S(X,Y);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6 - graphe du comportement du joint krig� si graphe = 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global sref tref derivA covA angleRA derivB covB angleRB IP JP KA KB coefX coefY
ns=205; nt=205;
%[ns,nt]=size(X);
s=linspace(0,1,ns)';
t=linspace(0,1,nt)';
SP=matRP(s,IP,covA,derivA,angleRA,sref);
if JP >=2, TP=matRP(t,JP,covB,derivB,angleRB,tref)'; else, TP = ones(1,nt); end
% Calcul des matrices des �quations param�triques
% En rempla�ant par les matrices calcul�es pr�c�demment
% Cette �quation a �t� expliqu�e plus haut
u=[SP*KA^-1*coefX*KB^-1*TP];
sigma=[SP*KA^-1*coefY*KB^-1*TP];
%plot(u,sigma,'-c',u',sigma','-r')
%hold on
%plot(X,Y,'.k',X,Y,':',X',Y','.-')
%surf(u,sigma,ones(size(u,1),size(sigma,2)))
%pause
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [RP]=matRP(r,I,cov,deriv,angleR,rref);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6.1 - calcul des matrices RP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RP=[];
nr=length(r);
for i=1:I
    if cov < 4 % Covariances polynomiales
        RP=[RP,abs(r-rref(i)).^cov];
    elseif cov == 4 % Covariance logarithmique
        RP=[RP,abs(r-rref(i)).^2.*log(abs(r-rref(i)))];
    elseif cov == 5 % Covariance sinuso�dale
        RP=[RP,sin(angleR*abs(r-rref(i))*pi/180)];
    end
end
% Ajout et calcul de la partie de la d�rive de la matrice des coefficients param�triques du profil A
% Cette partie de la matrice a les dimensions suivantes : [ns,derivA+1]
% Cette matrice est remplie du vecteur des coordonn�es param�triques le long du profil, 
% positionn� en colonne, o� chaque �l�ment est �lev� de z�ro jusqu'� la puissance de la 
% d�rive du profil.  C'est pourquoi la premi�re colonne de la matrice est compos�e de uns.
RP=[RP,ones(nr,1)]; % Premi�re colonne de la matrice, seulement des uns
if deriv~=5
    for i=1:deriv-1 % Boucle pour remplir de la 2i�me � la derni�re colonne
        RP=[RP,r.^i]; % en variant la puissance d'�l�vation des �l�ments du vecteur colonne
    end
else % La d�rive est sinuso�dale
    RP=[RP,sin(angleR*pi/180*r),cos(angleR*pi/180*r)];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [sif,Sif]=seek_s(uif);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.1.1 - recherche de s pour t = 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global u_map S_map umax Smax sref tref derivA covA angleRA derivB covB angleRB IP JP KA KB coefX coefY
for i=1:length(uif)
    skrig=.5;            % param�tre de d�part
    tkrig=1;             % courbe de chargement
    u=umax;              % valeur de d�part pour entrer dans la boucle
    ds=0.6;			     % valeur de l'incr�mentation de d�part
    iter_s=0;            % nombre d'it�rations pour trouver s
    eps_s=1e-9;          % crit�re d'arr�t (pr�cision)
    sigma=0;
%    disp(['u=',num2str(u),', u(i) = ',num2str(uif(i)),', i = ',int2str(i)])
    while abs(abs(u)-abs(uif(i)))/umax>eps_s
        % cette boucle permet de d�terminer la valeur de skrig (avec skrig = 1)   
        % Cr�ation de la matrice param�trique du profil A, SP.
        SP=matRP(skrig,IP,covA,derivA,angleRA,sref);
        % Cr�ation de la matrice param�trique du profil B, TP.
        if JP >= 2, TP=matRP(tkrig,JP,covB,derivB,angleRB,tref)'; else, TP = 1; end
        % Calcul de u et sigma correspondants � skrig et tkrig
        u=[SP*KA^-1*coefX*KB^-1*TP];
        sigma=[SP*KA^-1*coefY*KB^-1*TP];
        if u<uif(i)
            skrig=skrig+ds/2;
        else
            skrig=skrig-ds/2;
        end
        if skrig<0
            skrig=0;
        elseif skrig>1
            skrig=1;
        end
        ds=ds/2;
        iter_s=iter_s+1;
    end
    sif(i)=skrig;
    Sif(i)=sigma;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [tif,Sif]=seek_t(uif,s);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.2.1 - recherche de t pour s donn�
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global u_map S_map umax Smax sref tref derivA covA angleRA derivB covB angleRB IP JP KA KB coefX coefY

for i=1:length(uif)
    dt=1; % valeur de l'incr�mentation de d�part (d�part depuis la courbe de chargement)
    u=10*uif(i);% valeur de d�part pour entrer dans la boucle
    iter_t=0;
%    nombreiter=0;
    tkrig=0; % param�tre de d�part
    eps_t=1e-6; % critere de pr�cision
    sigma=0;
    while abs(abs(u)-abs(uif(i)))/umax>eps_t % & nombreiter<=100
        % cette boucle permet de d�terminer la valeur de tkrig et la contrainte recherch�e   
        %nombreiter=nombreiter+1; 
        % Cr�ation de la matrice param�trique du profil A, SP.
        SP=matRP(s(i),IP,covA,derivA,angleRA,sref);
        % Cr�ation de la matrice param�trique du profil B, TP.
        if JP >= 2, TP=matRP(tkrig,JP,covB,derivB,angleRB,tref)'; else, TP = 1; end
        % Calcul de u et sigma correspondants � skrig et tkrig
        u=[SP*KA^-1*coefX*KB^-1*TP];
        sigma=[SP*KA^-1*coefY*KB^-1*TP];
%             if tkrig==0
%                 depl_min(i)=u;
%                 cont_min(i)=sigma;
%             end
%             if depl_fin(i)<depl_min(i)
%                 cont_fin(i)=0;
%                 break
%             end
        if u<uif(i)
            tkrig=tkrig+dt;
        else
            tkrig=tkrig-dt;
        end
        if tkrig<0
            tkrig=0;
        elseif tkrig>1
            tkrig=1;
        end
        dt=dt/2;
        iter_t=iter_t+1;
    end % while
    Sif(i)=sigma;
    tif(i)=tkrig;
end % for
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function graphe_if(ui,uf,Si,Sf)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 10 - montre les points initiaux et finaux sur les courbes krig�es
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plot(ui,Si,'ko')
%plot(uf,Sf,'k*')
for i=1:length(ui)
    text(ui(i),Si(i),[' ',int2str(i),'i'])
    text(uf(i),Sf(i),[' ',int2str(i),'f'])
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function graphe_k(uf,Sf,dx,umx,k)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 12 - trace les pentes aux points de d�chargement
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
deltax=umx/10;
ydx=Sf-deltax*k;
%figure(1), hold on
%for i=1:length(uf)
%    plot([uf(i) uf(i)-deltax],[Sf(i) ydx(i)],'-b')
%end
%hold off
%pause
%close
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
